package chunk2json

import (
	"encoding/json"
	"errors"
	"fmt"
	"github.com/rs/zerolog/log"
	"github.com/spf13/cobra"
	"orienteermap-go-app/pkg/mc_formats/anvil/region"
	"orienteermap-go-app/pkg/mc_formats/shared"
	"orienteermap-go-app/pkg/types/basic"
	"os"
	"sync"
)

type combinedJsonFormat struct {
	Level  *shared.Level
	Chunks map[basic.CoordinatePair]*shared.ChunkBundle
}

func doChunk2Json(rootPath string, mcaPath string, chunks []basic.CoordinatePair) {
	reader, err := region.NewReader(rootPath, mcaPath)
	if err != nil {
		log.Fatal().Err(err).Msg("failed to open region reader")
	}

	var chunksSelected = chunks
	if len(chunksSelected) == 0 {
		chunksSelected, err = reader.GetListOfChunks()
		if err != nil {
			log.Fatal().Err(err).Msg("failed to get list of chunks")
		}
	}

	totalChunks := len(chunksSelected)
	chunksDone := 0

	var jsonOutputStruct combinedJsonFormat
	jsonOutputStruct.Level, err = reader.GetLevel()
	jsonOutputStruct.Chunks = make(map[basic.CoordinatePair]*shared.ChunkBundle)
	if err != nil {
		log.Fatal().Err(err).Msg("failed to get list of chunks")
	}

	// Use synchronization to ensure that no conflicts occur
	var mutex sync.Mutex
	err = reader.GetChunks(chunksSelected, func(pair basic.CoordinatePair, chunk *shared.ChunkBundle) {
		mutex.Lock()
		defer mutex.Unlock()

		jsonOutputStruct.Chunks[pair] = chunk

		chunksDone += 1

		log.Info().Int("chunksDone", chunksDone).Int("totalChunks", totalChunks).Msg("chunks parsed")
	}, true)

	if err != nil {
		log.Fatal().Err(err).Msg("failed to retrieve chunks")
		os.Exit(1)
	}

	log.Info().Msg("collation complete, starting JSON encoding")

	jsonEncoder := json.NewEncoder(os.Stdout)

	err = jsonEncoder.Encode(&jsonOutputStruct)
	if err != nil {
		_, _ = fmt.Fprintf(os.Stderr, "failed to convert into JSON: %v", err)
		os.Exit(1)
	}
}

func PrepareChunk2JsonCommand() *cobra.Command {
	var alternateMcaPath *string

	var cmdChunk2Json = &cobra.Command{
		Use:   "chunk2json [input file folder] <chunk> <chunk> ...",
		Short: "convert individual chunks to single combined JSON",
		Long:  "level2json is for converting individual chunks to a JSON format emitted to standard output, as OrienteerMap sees it. Requires a MC Anvil format save folder",
		Args: func(cmd *cobra.Command, args []string) error {
			if len(args) < 1 {
				return errors.New("at least 1 arguments required, an input folder and optional set of chunks")
			}
			for _, arg := range args {
				if len(arg) < 1 {
					return errors.New("no argument must be empty")
				}
			}
			return nil
		},
		Run: func(_ *cobra.Command, args []string) {
			var chunks []basic.CoordinatePair

			for _, rawArg := range args[1:] {
				var coordinate basic.CoordinatePair
				err := coordinate.UnmarshalText([]byte(rawArg))

				if err != nil {
					log.Fatal().Err(err).Str("rawArg", rawArg).Msg("failed to convert coordinate, exiting")
				}

				chunks = append(chunks, coordinate)
			}

			doChunk2Json(args[0], *alternateMcaPath, chunks)
		},
	}

	alternateMcaPath = cmdChunk2Json.Flags().StringP("directory", "d", "", "alternative subfolder for MCA (e.g. DIM1, DIM-1)")

	return cmdChunk2Json
}
