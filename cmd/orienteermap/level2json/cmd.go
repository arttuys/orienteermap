package level2json

import (
	"compress/gzip"
	"encoding/json"
	"errors"
	"github.com/rs/zerolog/log"
	"github.com/spf13/cobra"
	"orienteermap-go-app/pkg/mc_formats/nbt"
	"orienteermap-go-app/pkg/mc_formats/shared"
	"os"
)

func doLevel2Json(input string) {
	file, err := os.Open(input)
	if err != nil {
		log.Fatal().Err(err).Msg("failed to retrieve chunks")
	}

	gzipReader, err := gzip.NewReader(file)
	if err != nil {
		log.Fatal().Err(err).Msg("failed to uncompress with gzip")
	}

	nbtReader, err := nbt.New(gzipReader)
	if err != nil {
		log.Fatal().Err(err).Msg("failed to initialize NBT reader")
	}

	var level shared.Level
	_, err = nbtReader.DeserializeInto(&level)
	if err != nil {
		log.Fatal().Err(err).Msg("failed to deserialize into OrienteerMap level.dat structure")
	}

	jsonEncoder := json.NewEncoder(os.Stdout)

	err = jsonEncoder.Encode(&level)
	if err != nil {
		log.Fatal().Err(err).Msg("failed to convert into JSON")
	}
}

func PrepareLevel2JsonCommand() *cobra.Command {
	var cmdLevel2Json = &cobra.Command{
		Use:   "level2json [input file]",
		Short: "convert 'level.dat' to JSON",
		Long:  "level2json is for converting a level.dat file to a JSON format emitted to standard output, as OrienteerMap sees it. Useful for debugging purposes",
		Args: func(cmd *cobra.Command, args []string) error {
			if len(args) != 1 {
				return errors.New("exactly 1 argument required, the input file")
			}
			if len(args[0]) < 1 {
				return errors.New("input file name must not be empty")
			}
			return nil
		},
		Run: func(_ *cobra.Command, args []string) {
			doLevel2Json(args[0])
		},
	}

	return cmdLevel2Json
}
