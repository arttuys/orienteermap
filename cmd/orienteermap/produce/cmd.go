package produce

import (
	"errors"
	"github.com/spf13/cobra"
	"orienteermap-go-app/internal/runner/config_gen"
)

func PrepareProduceCommand() *cobra.Command {
	var cmdPrepare = &cobra.Command{
		Use:   "produce [configuration file]",
		Short: "produce a map",
		Long:  `produce is the principal command, producing a map per given configuration either in an one-shot (render all blocks and quit) or streaming (listen for changes and update live) form. It may also also run a web server (for map viewing) or a remote connection (to a MC server) depending on configuration`,
		Args: func(cmd *cobra.Command, args []string) error {
			if len(args) != 1 {
				return errors.New("exactly 1 argument required, the configuration file")
			}
			if len(args[0]) < 1 {
				return errors.New("input file name must not be empty")
			}
			return nil
		},
		Run: func(cmd *cobra.Command, args []string) {
			config_gen.RunByConfigFile(args[0])
		},
	}

	return cmdPrepare
}
