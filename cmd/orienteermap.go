package main

import (
	"github.com/rs/zerolog"
	"github.com/rs/zerolog/log"
	"github.com/spf13/cobra"
	"orienteermap-go-app/cmd/orienteermap/chunk2json"
	"orienteermap-go-app/cmd/orienteermap/level2json"
	"orienteermap-go-app/cmd/orienteermap/produce"
	"os"
)

var PrettyConsoleOutput bool
var EnableDebugLogging bool
var EnableTraceLogging bool

func prepareRootCommand() *cobra.Command {
	var rootCmd = &cobra.Command{
		Use:   "orienteermap",
		Short: "A map rendering tool for Minecraft Java Edition",
		Long:  `OrienteerMap is a map generator and rendering tool designed to create scalable, fast terrain maps describing Minecraft Java Edition worlds in utmost detail`,
		PersistentPreRun: func(cmd *cobra.Command, args []string) {
			if PrettyConsoleOutput {
				log.Logger = log.Output(zerolog.ConsoleWriter{Out: os.Stderr})
			}
			if EnableDebugLogging {
				zerolog.SetGlobalLevel(zerolog.TraceLevel)
			} else if EnableDebugLogging {
				zerolog.SetGlobalLevel(zerolog.DebugLevel)
			} else {
				zerolog.SetGlobalLevel(zerolog.InfoLevel)
			}
		},
	}

	rootCmd.AddCommand(level2json.PrepareLevel2JsonCommand())
	rootCmd.AddCommand(produce.PrepareProduceCommand())
	rootCmd.AddCommand(chunk2json.PrepareChunk2JsonCommand())

	rootCmd.PersistentFlags().BoolVar(&PrettyConsoleOutput, "pretty-console", true, "pretty-printed console output")
	rootCmd.PersistentFlags().BoolVar(&EnableDebugLogging, "debug", false, "enable debug logging")
	rootCmd.PersistentFlags().BoolVar(&EnableTraceLogging, "trace", false, "enable trace logging (very verbose!)")
	return rootCmd
}

func main() {
	rootCmd := prepareRootCommand()

	_ = rootCmd.Execute()
}
