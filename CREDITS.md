# Credits

Following software have been used as reference material for implementing this tool

- [Go-MC](https://github.com/Tnze/go-mc) by Tnze, in particular for its NBT implementation. 
- CESU8 support adapted from [SAP SE's GitHub](https://github.com/SAP/go-hdb/blob/v0.104.1/internal/unicode/cesu8/cesu8.go)
