package interfaces

import (
	"orienteermap-go-app/pkg/mc_formats/shared"
	"orienteermap-go-app/pkg/types/basic"
)

type BackchannelMessageKind uint8

const (
	InvalidMessage BackchannelMessageKind = iota
	// ChunkShouldUpdate indicates that there have occurred updates that probably affect what a map displays on given chunks. The rendering logic should enqueue them somewhere in the future
	ChunkShouldUpdate
	// Quiesce indicates that the world provider requests the runner to stop processing at its earliest opportunity. This can indicate larger changes, which render known pending data useless
	Quiesce
	// FreeformNotification signifies a logging message of some other nature that might be of interest to logging at map generation side. It doesn't necessarily indicate an error
	FreeformNotification
)

type BackchannelMessage struct {
	Type                     BackchannelMessageKind
	AssociatedChunkLocations []basic.CoordinatePair
	Note                     string
}

// WorldSource provides access to some "world", more exactly a single dimension of blocks, plus associated metadata
type WorldSource interface {
	ChunkMetadataQueryable
	// GetLevel gets basic level information
	GetLevel() (*shared.Level, error)
	// GetChunks retrieves chunk data for given chunks. Unlike other calls, this will take a closure which will be called for each chunk read. The calls will be done synchronously - GetChunks is guaranteed not to return until all possible calls have been made
	// A nil error will be returned if everything completed successfully, or non-nil if something went wrong. Boolean sets if this is a strict call - non-existence is an error
	GetChunks([]basic.CoordinatePair, func(basic.CoordinatePair, *shared.ChunkBundle), bool) error
	// GetBackchannel returns a channel which will be provided with backchannel messages if this provider supports them. If nil, no support is available
	// This can be originated from a remote server, but could also be hooked with a suitable WorldSource wrapper, for e.g. to allow web-server based control
	GetBackchannel() chan BackchannelMessage
	// Close releases all resources still allocated
	Close() error
}
