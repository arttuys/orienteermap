package interfaces

import (
	"orienteermap-go-app/pkg/types/basic"
	"orienteermap-go-app/pkg/types/map_abstract"
	"orienteermap-go-app/pkg/types/map_global_meta"
	"time"
)

// MiniDatabaseProvider is meant for the processing code generating SVGs. It doesn't expose anything else but two functions to retrieve and store map abstracts
// This might or might not be the same as a full provider, which should be considered an implementation detail
type MiniDatabaseProvider interface {
	// TryGetMapAbstracts tries to get map abstracts for given chunks. If some abstracts do not exist, they are omitted - it is not considered an error to request non-existing abstracts
	TryGetMapAbstracts([]basic.CoordinatePair) (map[basic.CoordinatePair]*map_abstract.MapAbstract, error)
	// StoreMapAbstract stores a single map abstract
	StoreMapAbstract(basic.CoordinatePair, *map_abstract.MapAbstract) error
}

// DatabaseProvider provides an interface to store interim data related to a given world. Usually it should be selected in conjunction of the output provider - this is meant to standardize the used interfaces
// to enable using a single runner implementation. All database providers are also chunk-metadata-queryable, but instead of actual chunk data, they store chunk map abstracts - map abstracts are essentially derived from raw chunk
// data and are used as an interim step before rendering, which may require data from adjacent chunks as well
//
// Before rendering, it should be ensured that all available abstracts have been generated - at the present time, no support for waiting for particular abstracts to be generated has been implemented
type DatabaseProvider interface {
	ChunkMetadataQueryable
	// StoreMapAbstract stores a single map abstract
	StoreMapAbstract(basic.CoordinatePair, *map_abstract.MapAbstract) error
	// GetMapAbstracts tries to get several map abstracts; if one or more are missing, it is considered an error
	GetMapAbstracts([]basic.CoordinatePair) (map[basic.CoordinatePair]*map_abstract.MapAbstract, error)
	// DeleteMapAbstract deletes one map abstract entirely. This should not fail even if the given abstract doesn't exist
	DeleteMapAbstract(basic.CoordinatePair) error
	// GetMiniProviderForRenderer returns a mini provider for rendering purposes. This should always succeed in all but the most exotic providers
	// Some providers might return themselves here as well - it is an implementation detail though and should not be assumed
	GetMiniProviderForRenderer() MiniDatabaseProvider
	// GetMapGlobalMetadata retrieves last saved instance of map global metadata, or nil if none exists
	GetMapGlobalMetadata() (*map_global_meta.MapGlobalMetadata, error)
	// GetMapGlobalMetadataLastUpdate retrieves when previous change to the map global metadata was done; if none is known, nil is returned
	GetMapGlobalMetadataLastUpdate() (*time.Time, error)
	// UpdateMapGlobalMetadata serializes and saves map global metadata, if any exists
	UpdateMapGlobalMetadata(meta *map_global_meta.MapGlobalMetadata) error
	// Close releases all resources still allocated
	Close() error
}
