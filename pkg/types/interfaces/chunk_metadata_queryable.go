package interfaces

import (
	"orienteermap-go-app/pkg/types/basic"
	"time"
)

// ChunkMetadataQueryable indicates that the given object has possession of chunk data in some form, allowing it to list chunks that are known to it, check if data for a given chunk exists, and return a modification time for an existing chunk
// This does not necessarily require that actual chunk data is stored - only that they have some characteristics derived from chunks
type ChunkMetadataQueryable interface {
	// GetListOfChunks returns list of all chunks that are known. This can be more expensive than a simple exists-check, but ideally should be relatively cheap
	GetListOfChunks() ([]basic.CoordinatePair, error)
	// ChunkExists checks if a given chunk exists. This should be a cheap and fast operation, at slowest equivalent to getting modification times.
	ChunkExists(pair basic.CoordinatePair) (bool, error)
	// GetChunkModificationTimes returns a map of modification times for associated chunks. This should be significantly cheaper than retrieving a full chunk, but can be more expensive than a simple exists-check
	// Boolean indicates if this is a strict query - namely, all given chunks must exist or it is an error
	GetChunkModificationTimes(coords []basic.CoordinatePair, strict bool) (map[basic.CoordinatePair]time.Time, error)
}
