package interfaces

import "orienteermap-go-app/pkg/types/basic"

// SVGOutputCollector collects SVG data, and saves it an implementation-dependent way. It also offers a way to check the last modification of a given chunk, considered usually a file
type SVGOutputCollector interface {
	ChunkMetadataQueryable
	// SaveChunkRender saves given SVG data to a given chunk location.
	SaveChunkRender(basic.CoordinatePair, []byte) error
	// DeleteChunk deletes a given chunk render entirely
	DeleteChunk(pair basic.CoordinatePair) error
	// Close terminates a collector, releasing resources
	Close() error
}
