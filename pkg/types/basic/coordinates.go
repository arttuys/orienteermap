package basic

import (
	"fmt"
	"strconv"
	"strings"
)

type CoordinatePair [2]int32
type CoordinateTriplet [3]int32

func (coord CoordinatePair) MarshalText() ([]byte, error) {
	return []byte(fmt.Sprintf("%d:%d", coord[0], coord[1])), nil
}

func (coord *CoordinatePair) UnmarshalText(text []byte) error {
	s := strings.Split(string(text), ":")

	if l := len(s); l != 2 {
		return fmt.Errorf("invalid length, expected 2, got %d", l)
	}

	for i := 0; i < 2; i++ {
		v, err := strconv.ParseInt(s[i], 10, 32)
		if err != nil {
			return err
		}
		coord[i] = int32(v)
	}

	return nil
}

func (coord CoordinateTriplet) MarshalText() ([]byte, error) {
	return []byte(fmt.Sprintf("%d:%d:%d", coord[0], coord[1], coord[2])), nil
}

func (coord *CoordinateTriplet) UnmarshalText(text []byte) error {
	s := strings.Split(string(text), ":")

	if l := len(s); l != 3 {
		return fmt.Errorf("invalid length, expected 3, got %d", l)
	}

	for i := 0; i < 3; i++ {
		v, err := strconv.ParseInt(s[i], 10, 32)
		if err != nil {
			return err
		}
		coord[i] = int32(v)
	}

	return nil
}
