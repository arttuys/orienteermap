package map_global_meta

import "orienteermap-go-app/pkg/types/basic"

type MapGlobalMetadata struct {
	ChunkRngSeeds map[basic.CoordinatePair]int64
}
