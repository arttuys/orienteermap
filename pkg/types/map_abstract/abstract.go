package map_abstract

type ChunkFlags = uint8
type TerrainType = uint8
type TerrainContext = uint8

type TerrainDef = struct {
	T TerrainType
	C TerrainContext
	// H indicates the main height
	H int32
	// BH indicates at what height built-up indications were found
	BH int32
}
type ChunkTerrainGrid = [16][16]TerrainDef
type ExtendedChunkTerrainGrid = [18][18]TerrainDef

type LineSegmentCoord struct {
	X int32
	Y int32
}
type LineSegment struct {
	A LineSegmentCoord
	B LineSegmentCoord
	// HeightString indicates what height (or other value) should be indicated at the center of this segment on rendering
	HeightString string
	// InverseDir indicates if the height descends from top to bottom, or left to right
	InverseDir bool
}

const (
	// Dangerous indicates there is a grave danger in this chunk (e.g. elder guardian)
	Dangerous = 1 << iota
	// ResourceMobs indicates there are useful resource-like mobs in this chunk (e.g. sheep, horses)
	ResourceMobs
	// Loot indicates there is something treasure-like to be found here (a treasure chest, perhaps?)
	Loot
	// Rest indicates that there is a sheltered location to sleep (a bed, reasonably close to ground level)
	Rest
	// Villagers indicates that there are villagers around
	Villagers
	// Portal indicates that there is an active portal somewhere in this chunk (reasonably close to ground level)
	Portal
)

const (
	// Unknown indicates the terrain type is not known
	Unknown TerrainType = iota
	// Peer indicates that the terrain type should be what the majority of its surroundings have; this is for scenarios where several choices are valid
	Peer
	// InlandWater indicates the primary terrain at a given point is water. Unless the water body is a part of ocean, it is considered inland water (this also includes coasts)
	InlandWater
	// Ocean indicates that the primary terrain here is water, substantially deeper than a lake / has other characteristics to indicate it is an ocean
	Ocean
	// Grassland indicates that the terrain is primarily grass or some other equivalent, biome-appropriate type. Ideally green colored
	Grassland
	// DirtLike indicates a terrain that cannot be considered grass, but is still composed of organic material. Ideally brown-ish color depending on biome
	DirtLike
	// Farmed indicates terrain that is clearly in use for farming - a tended field of some sort
	Farmed
	// Pathway indicates terrain that is clearly a path of some nature
	Pathway
	// Stone indicates plain stone or equivalent terrain - very little or no vegetation whatsoever
	Stone
	// Obsidian is a special case, considering it can be a part of a portal, or naturally generated - and in any case, is of interest in of itself
	Obsidian
	// Sand as it is named, is sand - either a desert, or a coastline
	Sand
	// Snow indicates a snow coverage
	Snow
	// Lava indicates that the topmost terrain is lava, rendering it inherently dangerous
	Lava
)

const (
	// Swamp indicates that the biome is swamp-y, and should be rendered as such
	Swamp TerrainContext = 1 << iota
	// ForestCoverage indicates the area has a forest coverage, and as such, should be rendered in that way. This is assessed by leaves
	ForestCoverage
	// BuiltUp indicates that there is some construction that obscures the terrain; this is considered to be cobblestone and other certainly crafted materials
	BuiltUp
)

// MapAbstract is the core entity for storing an abstract of a chunk, only containing data required. It is built up in two steps, first by individual parts having no access to each other, and
// then building certain contour curves with access to adjunct data
type MapAbstract struct {
	// BaseTerrain indicates the basic nature of terrain around; this is used to render the bottommost image layer
	BaseTerrain ChunkTerrainGrid
	// ChunkNatureFlags indicates if there are any particular interesting properties in this chunk. It is up to the renderer to show them as fit
	ChunkNatureFlags ChunkFlags
	// SecondPhaseComplete indicates if the second part of abstract generation has been completed (namely by global metadata update step, in which one has access to other abstracts)
	SecondPhaseComplete bool
	// ChunkContourSegments stores line segments and auxiliary information for drawing contour lines for height. This doesn't exist unless second phase is complete
	ChunkContourSegments []LineSegment
}
