package map_abstract

func HeightToHeightStep(h int32) int32 {
	stepCount := int32(0)
	sign := int32(1)
	stepDiff := int32(2)

	groundPlaneDiff := h - 64

	if groundPlaneDiff < 0 {
		sign = -1
		groundPlaneDiff *= -1
	}

	for groundPlaneDiff > stepDiff {
		groundPlaneDiff -= stepDiff

		if stepCount == 0 {
			stepDiff *= 2
		} else {
			stepDiff += 1
		}

		stepCount += sign
	}

	return stepCount
}
