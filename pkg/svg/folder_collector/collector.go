package folder_collector

import (
	"encoding/json"
	"fmt"
	"io/fs"
	"orienteermap-go-app/pkg/mc_formats/shared"
	"orienteermap-go-app/pkg/types/basic"
	interfaces2 "orienteermap-go-app/pkg/types/interfaces"
	"os"
	"path/filepath"
	"strconv"
	"strings"
	"sync"
	"time"
)

// SVGFolderCollector is a simple collector saving data in an SVG folder structure. It can work alongside other folder structures in the same root folder - it only concerns itself with SVG naming and
// file-based checks
type SVGFolderCollector struct {
	// rootFolder signifies where to store data. Data will be stored in an subfolder "svg"/regionX/regionZ/map.chunkX.chunkZ.svg
	rootFolder string
	// singleActionMutex ensures there's a happens-before relationship between any and all actions; as this storage method doesn't have its own internal caching, this is required
	singleActionMutex sync.RWMutex
}

type chunkLstEl struct {
	X    int32  `json:"x"`
	Z    int32  `json:"z"`
	Path string `json:"path"`
}

func ParseSVGFileName(name string) (chunkX int32, chunkZ int32, isValid bool) {
	isValid = false

	splitStr := strings.Split(name, ".")

	if len(splitStr) != 4 || splitStr[0] != "map" || splitStr[3] != "svg" {
		// Not acceptable, not a valid region file name
		return
	}

	if i, err := strconv.ParseInt(splitStr[1], 10, 32); err != nil {
		return
	} else {
		chunkX = int32(i)
	}

	if i, err := strconv.ParseInt(splitStr[2], 10, 32); err != nil {
		return
	} else {
		chunkZ = int32(i)
	}

	isValid = true

	return
}

func ChunkCoordinateToRelativeFilePath(pair basic.CoordinatePair) string {
	return filepath.Join("svg", strconv.FormatInt(int64(shared.ChunkToRegionCoord(pair[0])), 10), strconv.FormatInt(int64(shared.ChunkToRegionCoord(pair[1])), 10), fmt.Sprintf("map.%d.%d.svg", pair[0], pair[1]))
}

func (S *SVGFolderCollector) ensureFolderExistsForChunk(pair basic.CoordinatePair) error {
	targetPath := filepath.Join(S.rootFolder, filepath.Dir(ChunkCoordinateToRelativeFilePath(pair)))
	return os.MkdirAll(targetPath, os.ModePerm)
}

func (S *SVGFolderCollector) DeleteChunk(pair basic.CoordinatePair) error {
	S.singleActionMutex.Lock()
	defer S.singleActionMutex.Unlock()

	return os.Remove(filepath.Join(S.rootFolder, ChunkCoordinateToRelativeFilePath(pair)))
}

func (S *SVGFolderCollector) GetListOfChunks() ([]basic.CoordinatePair, error) {
	S.singleActionMutex.RLock()
	defer S.singleActionMutex.RUnlock()

	chunkLst := make([]basic.CoordinatePair, 0)

	err := filepath.Walk(filepath.Join(S.rootFolder, "svg"), func(path string, info fs.FileInfo, err error) error {
		if err != nil {
			if os.IsNotExist(err) {
				return nil
			} else {
				return err
			}
		}
		if info.IsDir() {
			// Ignore directories
			return nil
		}

		// Try to parse as a map file
		chunkX, chunkZ, valid := ParseSVGFileName(filepath.Base(path))
		if !valid {
			return nil
		}

		// This could be a valid chunk. Check that two path levels above also match
		expectedRegionZBase := filepath.Dir(path)
		expectedRegionZ := filepath.Base(expectedRegionZBase)
		expectedRegionX := filepath.Base(filepath.Dir(expectedRegionZBase))

		if strconv.FormatInt(int64(shared.ChunkToRegionCoord(chunkX)), 10) != expectedRegionX || strconv.FormatInt(int64(shared.ChunkToRegionCoord(chunkZ)), 10) != expectedRegionZ {
			return nil
		}

		// OK, this seems like a valid map file. Add the pair
		chunkLst = append(chunkLst, basic.CoordinatePair{chunkX, chunkZ})

		return nil
	})

	return chunkLst, err
}

func (S *SVGFolderCollector) ChunkExists(pair basic.CoordinatePair) (bool, error) {
	S.singleActionMutex.RLock()
	defer S.singleActionMutex.RUnlock()

	_, err := os.Stat(filepath.Join(S.rootFolder, ChunkCoordinateToRelativeFilePath(pair)))

	if err == nil {
		return true, nil
	}
	if os.IsNotExist(err) {
		return false, nil
	}

	return false, err
}

func (S *SVGFolderCollector) GetChunkModificationTimes(pairs []basic.CoordinatePair, strict bool) (map[basic.CoordinatePair]time.Time, error) {
	S.singleActionMutex.RLock()
	defer S.singleActionMutex.RUnlock()

	dataMap := make(map[basic.CoordinatePair]time.Time)

	for _, pair := range pairs {
		stat, err := os.Stat(filepath.Join(S.rootFolder, ChunkCoordinateToRelativeFilePath(pair)))
		if err != nil {
			if os.IsNotExist(err) && !strict {
				continue
			}
			return nil, err
		}

		dataMap[pair] = stat.ModTime()
	}

	return dataMap, nil
}

func (S *SVGFolderCollector) SaveChunkRender(pair basic.CoordinatePair, bytes []byte) error {
	S.singleActionMutex.Lock()
	defer S.singleActionMutex.Unlock()

	err := S.ensureFolderExistsForChunk(pair)
	if err != nil {
		return err
	}

	fullPath := filepath.Join(S.rootFolder, ChunkCoordinateToRelativeFilePath(pair))

	return os.WriteFile(fullPath, bytes, os.ModePerm)
}

func (S *SVGFolderCollector) Close() error {
	chunks, err := S.GetListOfChunks()
	if err != nil {
		return err
	}

	chunkIndexWriteErr := S.writeJSONIndex(chunks)
	chunkHTMLWriteErr := S.GenerateHTMLIndex(chunks)

	if chunkIndexWriteErr != nil || chunkHTMLWriteErr != nil {
		return fmt.Errorf("one or more errors received: index write %w, html write %w", chunkIndexWriteErr, chunkHTMLWriteErr)
	}

	return nil
}

func (S *SVGFolderCollector) writeJSONIndex(chunks []basic.CoordinatePair) error {
	// Generate a chunk list structure
	var chunkList []chunkLstEl = make([]chunkLstEl, 0)

	for _, chunk := range chunks {
		chunkList = append(chunkList, chunkLstEl{
			X:    chunk[0],
			Z:    chunk[1],
			Path: filepath.ToSlash(ChunkCoordinateToRelativeFilePath(chunk)),
		})
	}

	// Ensure path exists
	err := os.MkdirAll(filepath.Join(S.rootFolder, "svg"), os.ModePerm)
	if err != nil {
		return err
	}

	jsonFile, err := os.Create(filepath.Join(S.rootFolder, "svg_index.json"))
	encoder := json.NewEncoder(jsonFile)

	if err := encoder.Encode(&chunkList); err != nil {
		return err
	}

	return jsonFile.Close()
}

func NewCollector(rootFolder string) (interfaces2.SVGOutputCollector, error) {
	absFldr, err := filepath.Abs(rootFolder)

	if err != nil {
		return nil, err
	}

	v := &SVGFolderCollector{
		rootFolder: absFldr,
	}

	return v, nil
}
