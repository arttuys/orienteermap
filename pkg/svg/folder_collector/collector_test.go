package folder_collector

import (
	"io/ioutil"
	"orienteermap-go-app/pkg/types/basic"
	"orienteermap-go-app/pkg/utils/test_helpers"
	"os"
	"testing"
)

func createTempDir() (string, error) {
	return ioutil.TempDir(os.TempDir(), "orienteermap-svgfoldercollector-*")
}

func TestSVGFolderCollectorRoundtrip(t *testing.T) {
	tempPath, err := createTempDir()
	if err != nil {
		t.Error(err)
		return
	}

	// Create a collector
	collector, err := NewCollector(tempPath)
	if err != nil {
		t.Error(err)
		return
	}

	// No chunks should exist
	test_helpers.CheckNChunksExist(t, collector, 0)

	// Set initial first chunk location
	chunk := basic.CoordinatePair{102, -3400}
	test_helpers.CheckChunkDoesNotExist(t, collector, chunk)

	// Create a chunk
	err = collector.SaveChunkRender(chunk, []byte{1, 2, 3, 4})
	if err != nil {
		t.Error(err)
		return
	}

	// Ensure this chunk exists now
	test_helpers.CheckChunkExists(t, collector, chunk)

	// Create second chunk
	chunk2 := basic.CoordinatePair{1021, -340}
	err = collector.SaveChunkRender(chunk2, []byte{1, 2, 3, 4})
	if err != nil {
		t.Error(err)
		return
	}

	// Check that both chunks are found in listing
	test_helpers.CheckChunksFoundInListingExact(t, collector, chunk, chunk2)

	test_helpers.VerifyModificationTimesCloseToNow(t, collector, chunk, chunk2)

	// Expect deletion to work
	err1 := collector.DeleteChunk(chunk)
	err2 := collector.DeleteChunk(chunk2)

	if err1 != nil || err2 != nil {
		t.Errorf("deletion test failed: %v, %v", err1, err2)
		return
	}

	// No chunks should exist
	test_helpers.CheckNChunksExist(t, collector, 0)

	// Finally, test closing
	if err := collector.Close(); err != nil {
		t.Errorf("closing test failed: %v", err)
		return
	}
}
