package folder_collector

import (
	"fmt"
	"orienteermap-go-app/pkg/types/basic"
	"os"
	"path/filepath"
)

const prelude = `
<!DOCTYPE html>
<html>
<head><title>Map</title><meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">

<style>
body { background-color: black; }
.root { font-size: 0; white-space: nowrap; }
.row { display: block; font-size: 0; white-space: nowrap; }
.column { display: inline-block; width: 64px; height: 64px; font-size: 0; white-space: nowrap; }
</style>

</head>
<body>

<div class="root">
`

const epilogue = `
</div>
</body>
</html>
`

func (S *SVGFolderCollector) GenerateHTMLIndex(chunkLst []basic.CoordinatePair) error {
	var minX, maxX, minZ, maxZ int32

	hasFileMap := make(map[basic.CoordinatePair]bool)

	// First, enumerate boundaries and mark present chunks
	for _, chunk := range chunkLst {
		hasFileMap[chunk] = true
		if chunk[0] < minX {
			minX = chunk[0]
		}
		if chunk[0] > maxX {
			maxX = chunk[0]
		}

		if chunk[1] < minZ {
			minZ = chunk[1]
		}
		if chunk[1] > maxZ {
			maxZ = chunk[1]
		}
	}

	// Ensure path exists
	err := os.MkdirAll(filepath.Join(S.rootFolder, "svg"), os.ModePerm)
	if err != nil {
		return err
	}

	outFile, err := os.Create(filepath.Join(S.rootFolder, "svg_view.html"))
	if err != nil {
		return err
	}

	if _, err := outFile.WriteString(prelude); err != nil {
		return err
	}

	for y := minZ; y <= maxZ; y++ {
		if _, err := outFile.WriteString(fmt.Sprintf(`<div class="row"> <!-- Z=%d -->`, y)); err != nil {
			return err
		}
		for x := minX; x <= maxX; x++ {
			if _, err := outFile.WriteString(fmt.Sprintf(`<div class="column"> <!-- X,Z=%d,%d -->`, x, y)); err != nil {
				return err
			}

			pair := basic.CoordinatePair{x, y}

			if hasFileMap[pair] {
				if _, err := outFile.WriteString(fmt.Sprintf(`<img width="64" height="64" src="%s" alt="Chunk %d:%d">`, filepath.ToSlash(ChunkCoordinateToRelativeFilePath(pair)), x, y)); err != nil {
					return err
				}
			}

			if _, err := outFile.WriteString(`</div>`); err != nil {
				return err
			}
		}
		if _, err := outFile.WriteString(`</div>`); err != nil {
			return err
		}
	}

	if _, err := outFile.WriteString(epilogue); err != nil {
		return err
	}

	return outFile.Close()
}
