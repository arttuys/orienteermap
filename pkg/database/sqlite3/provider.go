package sqlite3

import (
	"bytes"
	"compress/gzip"
	"database/sql"
	"encoding/gob"
	"fmt"
	_ "github.com/mattn/go-sqlite3"
	"orienteermap-go-app/pkg/types/basic"
	"orienteermap-go-app/pkg/types/interfaces"
	"orienteermap-go-app/pkg/types/map_abstract"
	"orienteermap-go-app/pkg/types/map_global_meta"
	"os"
	"path/filepath"
	"sync"
	"time"
)

// Provider provides a directory-based provider intended to be a natural companion to the standard SVG provider; same root directory can be given to both without conflict
type Provider struct {
	connection   *sql.DB
	dbSubdir     string
	metaSyncLock sync.RWMutex
	// dbSyncLock guarantees that only one thread at a time accesses SQLite3; it cannot be 100% guaranteed otherwise in a platform-independent way
	dbSyncLock sync.Mutex
}

func (P *Provider) GetMapGlobalMetadataLastUpdate() (*time.Time, error) {
	P.metaSyncLock.RLock()
	defer P.metaSyncLock.RUnlock()

	fname := P.getMetadataFilePath()

	stat, err := os.Stat(fname)

	if err != nil {
		if os.IsNotExist(err) {
			return nil, nil
		}
		return nil, err
	}

	t := stat.ModTime()

	return &t, nil
}

func (P *Provider) getMetadataFilePath() string {
	return filepath.Join(P.dbSubdir, "metadata.dat.gz")
}

//goland:noinspection SqlResolve
func (P *Provider) StoreMapAbstract(pair basic.CoordinatePair, abstract *map_abstract.MapAbstract) error {
	P.dbSyncLock.Lock()
	defer P.dbSyncLock.Unlock()

	// First, do encoding
	buf := bytes.Buffer{}
	gzWriter := gzip.NewWriter(&buf)

	encoder := gob.NewEncoder(gzWriter)
	if err := encoder.Encode(abstract); err != nil {
		return err
	}
	if err := gzWriter.Close(); err != nil {
		return err
	}

	// Everything is complete. Now do a query
	_, err := P.connection.Exec("INSERT OR REPLACE INTO map_abstract VALUES(?,?,?,?)", pair[0], pair[1], buf.Bytes(), time.Now().Unix())

	return err
}

func (P *Provider) GetMapGlobalMetadata() (*map_global_meta.MapGlobalMetadata, error) {
	P.metaSyncLock.RLock()
	defer P.metaSyncLock.RUnlock()

	fname := P.getMetadataFilePath()

	fileReader, err := os.Open(fname)
	if err != nil {
		if os.IsNotExist(err) {
			// This is OK and not an error
			return nil, nil
		}
		return nil, err
	}

	defer func() {
		_ = fileReader.Close()
	}()

	// Create a gzip reader
	gzReader, err := gzip.NewReader(fileReader)
	if err != nil {
		_ = fileReader.Close()
		return nil, err
	}

	defer func() {
		_ = gzReader.Close()
	}()

	// Try to deserialize from a gob
	gobDecoder := gob.NewDecoder(gzReader)
	var mapMeta map_global_meta.MapGlobalMetadata

	err = gobDecoder.Decode(&mapMeta)
	return &mapMeta, err
}

func (P *Provider) UpdateMapGlobalMetadata(meta *map_global_meta.MapGlobalMetadata) error {
	P.metaSyncLock.Lock()
	defer P.metaSyncLock.Unlock()

	fname := P.getMetadataFilePath()

	if meta == nil {
		// Deletion requested
		err := os.Remove(fname)

		if os.IsNotExist(err) {
			// This is OK and not an error
			return nil
		}
		return err
	} else {
		// Open a file writer
		fileWriter, err := os.Create(fname)
		if err != nil {
			return err
		}

		gzWriter := gzip.NewWriter(fileWriter)

		// Try to write as a gob
		gobEncoder := gob.NewEncoder(gzWriter)
		if err := gobEncoder.Encode(meta); err != nil {
			return err
		}

		// Ensure buffers are flushed
		if err := gzWriter.Close(); err != nil {
			return err
		}
		if err := fileWriter.Close(); err != nil {
			return err
		}

		return nil
	}
}

//goland:noinspection SqlResolve
func (P *Provider) GetListOfChunks() ([]basic.CoordinatePair, error) {
	P.dbSyncLock.Lock()
	defer P.dbSyncLock.Unlock()

	res := make([]basic.CoordinatePair, 0)

	// Retrieve rows
	rows, err := P.connection.Query("SELECT x, z FROM map_abstract")
	if err != nil {
		return nil, err
	}

	// Defer row closure
	defer func(rows *sql.Rows) {
		err := rows.Close()
		if err != nil {
			_, _ = fmt.Fprintf(os.Stderr, "failed to close DB rows: %v", err)
		}
	}(rows)

	for rows.Next() {
		var x int32
		var z int32

		err := rows.Scan(&x, &z)

		if err != nil {
			return nil, err
		}

		res = append(res, basic.CoordinatePair{x, z})
	}

	if err := rows.Err(); err != nil {
		return nil, err
	}

	return res, nil
}

func (P *Provider) ChunkExists(pair basic.CoordinatePair) (bool, error) {
	P.dbSyncLock.Lock()
	defer P.dbSyncLock.Unlock()

	where, params := generateXzWhere(pair)

	row := P.connection.QueryRow(fmt.Sprintf("SELECT COUNT(*) > 0 FROM map_abstract %s", where), params...)

	var res bool
	err := row.Scan(&res)

	return res, err
}

func (P *Provider) GetChunkModificationTimes(pairs []basic.CoordinatePair, strict bool) (map[basic.CoordinatePair]time.Time, error) {
	P.dbSyncLock.Lock()
	defer P.dbSyncLock.Unlock()

	resMap := make(map[basic.CoordinatePair]time.Time)

	if len(pairs) == 0 {
		return resMap, nil
	}

	where, args := generateXzWhere(pairs...)

	countRow := P.connection.QueryRow(fmt.Sprintf("SELECT COUNT(*) FROM map_abstract %s", where), args...)
	var count int64

	err := countRow.Scan(&count)
	if err != nil {
		return nil, err
	}

	if count != int64(len(pairs)) && strict {
		return nil, fmt.Errorf("not all chunks are available: expected %d, but found %d", count, len(pairs))
	}

	// Retrieve actual map abstracts
	rows, err := P.connection.Query(fmt.Sprintf("SELECT x,z,modification_time FROM map_abstract %s", where), args...)
	if err != nil {
		return nil, err
	}

	// Defer row closure
	defer func(rows *sql.Rows) {
		err := rows.Close()
		if err != nil {
			_, _ = fmt.Fprintf(os.Stderr, "failed to close DB rows: %v", err)
		}
	}(rows)

	// Scan contents
	for rows.Next() {
		var x int32
		var z int32
		var modificationTime int64

		err := rows.Scan(&x, &z, &modificationTime)
		if err != nil {
			return nil, err
		}

		// All good, we now have all we need
		resMap[basic.CoordinatePair{x, z}] = time.Unix(modificationTime, 0)

	}

	if err := rows.Err(); err != nil {
		return nil, err
	}

	return resMap, nil
}

func (P *Provider) GetMapAbstracts(pairs []basic.CoordinatePair) (map[basic.CoordinatePair]*map_abstract.MapAbstract, error) {
	P.dbSyncLock.Lock()
	defer P.dbSyncLock.Unlock()

	return P.getMapAbstractsInner(pairs, true)
}

func (P *Provider) TryGetMapAbstracts(pairs []basic.CoordinatePair) (map[basic.CoordinatePair]*map_abstract.MapAbstract, error) {
	P.dbSyncLock.Lock()
	defer P.dbSyncLock.Unlock()

	return P.getMapAbstractsInner(pairs, false)
}

func (P *Provider) DeleteMapAbstract(pair basic.CoordinatePair) error {
	P.dbSyncLock.Lock()
	defer P.dbSyncLock.Unlock()

	where, args := generateXzWhere(pair)

	_, err := P.connection.Exec(fmt.Sprintf("DELETE FROM map_abstract %s", where), args...)
	return err
}

func (P *Provider) GetMiniProviderForRenderer() interfaces.MiniDatabaseProvider {
	P.dbSyncLock.Lock()
	defer P.dbSyncLock.Unlock()

	return P
}

func (P *Provider) Close() error {
	P.dbSyncLock.Lock()
	defer P.dbSyncLock.Unlock()

	return P.connection.Close()
}

func NewProvider(directory string) (interfaces.DatabaseProvider, error) {
	absPath, err := filepath.Abs(directory)
	if err != nil {
		return nil, err
	}

	// Try to ensure a DB subdirectory exists
	dbSubdir := filepath.Join(absPath, "db")
	err = os.MkdirAll(dbSubdir, os.ModePerm)
	if err != nil {
		return nil, err
	}

	conn, err := sql.Open("sqlite3", filepath.Join(dbSubdir, "abstracts.sqlite3"))
	if err != nil {
		return nil, err
	}

	// Create provider
	provider := &Provider{connection: conn, dbSubdir: dbSubdir, metaSyncLock: sync.RWMutex{}, dbSyncLock: sync.Mutex{}}

	// Initialize schema
	err = provider.initSqlSchema()
	if err != nil {
		return nil, err
	}

	return provider, nil
}
