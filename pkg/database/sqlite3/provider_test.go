package sqlite3

import (
	"fmt"
	"io/ioutil"
	"math"
	"orienteermap-go-app/pkg/types/basic"
	"orienteermap-go-app/pkg/types/map_abstract"
	"orienteermap-go-app/pkg/types/map_global_meta"
	"orienteermap-go-app/pkg/utils/test_helpers"
	"os"
	"reflect"
	"testing"
	"time"
)

func createTempDir() (string, error) {
	return ioutil.TempDir(os.TempDir(), "orienteermap-sqlite-db-*")
}

func TestGlobalMetadata(t *testing.T) {
	tempPath, err := createTempDir()
	if err != nil {
		t.Error(err)
		return
	}

	provider, err := NewProvider(tempPath)
	if err != nil {
		t.Error(err)
		return
	}

	// First call should return nil
	res, err := provider.GetMapGlobalMetadata()

	if err != nil || res != nil {
		if err == nil {
			err = fmt.Errorf("expected nil result, did not get nil")
		}
		t.Fatal(err)
	}

	// Test that no modification time exists
	modTime, err := provider.GetMapGlobalMetadataLastUpdate()
	if err != nil || modTime != nil {
		if err == nil {
			err = fmt.Errorf("got nonempty modification time for nonexistent meta: %v", modTime)
		}

		t.Fatal(err)
	}

	// Create an instance
	globalMeta := map_global_meta.MapGlobalMetadata{}

	err = provider.UpdateMapGlobalMetadata(&globalMeta)
	if err != nil {
		t.Error(err)
		return
	}

	// Secondary should return not-nil
	res, err = provider.GetMapGlobalMetadata()

	if err != nil || res == nil {
		if err == nil {
			err = fmt.Errorf("expected non-nil result, did get nil")
		}
		t.Fatal(err)
	}

	// Test that modification exists
	modTime, err = provider.GetMapGlobalMetadataLastUpdate()
	if err != nil || modTime == nil {
		if err == nil {
			err = fmt.Errorf("got no modification time where expected")
		}

		t.Fatal(err)
	}

	diff := time.Now().Sub(*modTime)
	if math.Abs(diff.Minutes()) > 3 {
		t.Fatalf("too large difference in current time and given modification time: %v", diff)
	}

	// Erase it
	err = provider.UpdateMapGlobalMetadata(nil)
	if err != nil {
		t.Error(err)
		return
	}

	// Nil should again be returned
	res, err = provider.GetMapGlobalMetadata()

	if err != nil || res != nil {
		if err == nil {
			err = fmt.Errorf("expected nil result, did not get nil")
		}
		t.Fatal(err)
	}

	// Test that no modification time exists again
	modTime, err = provider.GetMapGlobalMetadataLastUpdate()
	if err != nil || modTime != nil {
		if err == nil {
			err = fmt.Errorf("got nonempty modification time for nonexistent meta: %v", modTime)
		}

		t.Fatal(err)
	}

	err = provider.Close()
	if err != nil {
		t.Error(err)
		return
	}
}

func TestMiniProviderRoundtrip(t *testing.T) {
	tempPath, err := createTempDir()
	if err != nil {
		t.Error(err)
		return
	}

	provider, err := NewProvider(tempPath)
	if err != nil {
		t.Error(err)
		return
	}

	// Save two map abstracts
	chunk1 := basic.CoordinatePair{102, -3400}
	chunk2 := basic.CoordinatePair{102, -3401}

	abstract1 := map_abstract.MapAbstract{ChunkNatureFlags: map_abstract.Dangerous}
	err = provider.StoreMapAbstract(chunk1, &abstract1)
	if err != nil {
		t.Fatal(err)
	}

	abstract2 := map_abstract.MapAbstract{ChunkNatureFlags: map_abstract.Loot}
	err = provider.StoreMapAbstract(chunk2, &abstract2)
	if err != nil {
		t.Fatal(err)
	}

	// Create a mini-provider for chunk 1
	ch1miniProvider := provider.GetMiniProviderForRenderer()

	nonexistentChunk := basic.CoordinatePair{101, -3400}

	// Request both known and unknown
	foundChunks, err := ch1miniProvider.TryGetMapAbstracts([]basic.CoordinatePair{chunk2, nonexistentChunk})
	if err != nil {
		t.Fatal(err)
	}

	if l := len(foundChunks); l != 1 {
		t.Fatalf("expected to find only 1 chunk, found %d", l)
	}

	if !reflect.DeepEqual(&abstract2, foundChunks[chunk2]) {
		t.Fatalf("did not receive expected data from try-get query: %v vs %v", abstract2, foundChunks[chunk2])
	}

	_ = provider.Close()
}

func TestProviderBasicRoundtrip(t *testing.T) {
	tempPath, err := createTempDir()
	if err != nil {
		t.Error(err)
		return
	}

	provider, err := NewProvider(tempPath)
	if err != nil {
		t.Error(err)
		return
	}

	// No chunks should exist
	test_helpers.CheckNChunksExist(t, provider, 0)

	// Check that our test chunk doesn't exist
	chunk := basic.CoordinatePair{102, -3400}
	test_helpers.CheckChunkDoesNotExist(t, provider, chunk)

	// Save map abstract
	abstract := map_abstract.MapAbstract{ChunkNatureFlags: map_abstract.Dangerous}
	err = provider.StoreMapAbstract(chunk, &abstract)
	if err != nil {
		t.Fatal(err)
	}

	// Check that this chunk exists
	test_helpers.CheckChunkExists(t, provider, chunk)
	test_helpers.CheckChunksFoundInListingExact(t, provider, chunk)

	// Retrieve it
	retrievedAbstracts, err := provider.GetMapAbstracts([]basic.CoordinatePair{chunk})
	if err != nil {
		t.Fatal(err)
	}

	// Test that it equals
	if !reflect.DeepEqual(retrievedAbstracts[chunk], &abstract) {
		t.Fatal("retrieved map abstract was not equal!")
	}

	// Test modification time
	test_helpers.VerifyModificationTimesCloseToNow(t, provider, chunk)

	// Test deletion
	err = provider.DeleteMapAbstract(chunk)
	if err != nil {
		t.Fatal(err)
	}

	// No chunks should exist anymore
	test_helpers.CheckNChunksExist(t, provider, 0)
	test_helpers.CheckChunkDoesNotExist(t, provider, chunk)

	err = provider.Close()
	if err != nil {
		t.Error(err)
		return
	}
}
