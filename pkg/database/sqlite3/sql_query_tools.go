package sqlite3

import (
	"bytes"
	"compress/gzip"
	"database/sql"
	"encoding/gob"
	"fmt"
	"orienteermap-go-app/pkg/types/basic"
	"orienteermap-go-app/pkg/types/map_abstract"
	"os"
	"strings"
)

func (P *Provider) initSqlSchema() error {
	_, err := P.connection.Exec(`
		CREATE TABLE IF NOT EXISTS map_abstract(
		    x INTEGER NOT NULL,
		    z INTEGER NOT NULL,
		    map_abstract BLOB NOT NULL,
		    modification_time INTEGER NOT NULL, -- Stored as UNIX time
		    PRIMARY KEY(x, z)
		) WITHOUT ROWID;
	`)

	return err
}

func generateXzWhere(chunks ...basic.CoordinatePair) (string, []interface{}) {
	var sb strings.Builder
	var args []interface{}
	sb.WriteString("WHERE (")
	for i, chunk := range chunks {
		if i > 0 {
			sb.WriteString(" OR ")
		}
		sb.WriteString("(")
		sb.WriteString(fmt.Sprintf("x = ? AND z = ?"))
		args = append(args, chunk[0], chunk[1])
		sb.WriteString(")")
	}
	sb.WriteString(")")

	return sb.String(), args
}

func (P *Provider) getMapAbstractsInner(pairs []basic.CoordinatePair, checkCount bool) (map[basic.CoordinatePair]*map_abstract.MapAbstract, error) {
	resMap := make(map[basic.CoordinatePair]*map_abstract.MapAbstract)

	if len(pairs) == 0 {
		return resMap, nil
	}

	where, args := generateXzWhere(pairs...)

	if checkCount {
		countRow := P.connection.QueryRow(fmt.Sprintf("SELECT COUNT(*) FROM map_abstract %s", where), args...)
		var count int64

		err := countRow.Scan(&count)
		if err != nil {
			return nil, err
		}

		if count != int64(len(pairs)) {
			return nil, fmt.Errorf("not all chunks are available: expected %d, but found %d", count, len(pairs))
		}
	}

	// Retrieve actual map abstracts
	rows, err := P.connection.Query(fmt.Sprintf("SELECT x,z,map_abstract FROM map_abstract %s", where), args...)
	if err != nil {
		return nil, err
	}

	// Defer row closure
	defer func(rows *sql.Rows) {
		err := rows.Close()
		if err != nil {
			_, _ = fmt.Fprintf(os.Stderr, "failed to close DB rows: %v", err)
		}
	}(rows)

	// Scan contents
	for rows.Next() {
		var x int32
		var z int32
		var mapAbstractRaw []byte

		err := rows.Scan(&x, &z, &mapAbstractRaw)
		if err != nil {
			return nil, err
		}

		// Try to decompress
		byteReader := bytes.NewReader(mapAbstractRaw)

		gzReader, err := gzip.NewReader(byteReader)
		if err != nil {
			return nil, err
		}

		gobDecoder := gob.NewDecoder(gzReader)

		var data map_abstract.MapAbstract
		err = gobDecoder.Decode(&data)
		if err != nil {
			return nil, err
		}

		// All good, we now have all we need
		resMap[basic.CoordinatePair{x, z}] = &data

		if err := gzReader.Close(); err != nil {
			return nil, err
		}

	}

	if err := rows.Err(); err != nil {
		return nil, err
	}

	return resMap, nil
}
