// Vendored and adapted from: https://github.com/SAP/go-hdb/blob/v0.104.1/internal/unicode/cesu8/cesu8.go, https://github.com/crazytyper/go-cesu8/blob/270517b5a01c/cesu8.go

/*
Copyright 2014 SAP SE

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

// Package cesu8 implements functions and constants to support text encoded in CESU-8.
// It implements functions comparable to the unicode/utf8 package for UTF-8 de- and encoding.
package cesu8

import (
	"strings"
	"unicode/utf16"
	"unicode/utf8"
)

const (
	// CESUMax is the maximum amount of bytes used by an CESU-8 codepoint encoding.
	CESUMax = 6
)

// DecodeRune unpacks the first CESU-8 encoding in p and returns the rune and its width in bytes. It also handles surrogates where present
func DecodeRune(p []byte) (rune, int) {
	high, n1 := decodeRuneInternal(p)
	if !utf16.IsSurrogate(high) {
		return high, n1
	}
	low, n2 := decodeRuneInternal(p[n1:])
	if low == utf8.RuneError {
		return low, n1 + n2
	}
	return utf16.DecodeRune(high, low), n1 + n2
}

// DecodeString decodes a CESU-8 encoded slice of bytes into a string.
func DecodeString(p []byte) string {
	// Construct a string builder, and incrementally decode runes into a full string
	s := strings.Builder{}
	for len(p) > 0 {
		r, n := DecodeRune(p)
		s.WriteRune(r)
		p = p[n:]
	}
	return s.String()
}

// +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

// Copied from unicode utf8
// - allow utf8 encoding of utf16 surrogate values
// - see (*) for code changes

const (
	tx = 0x80 // 1000 0000
	t2 = 0xC0 // 1100 0000
	t3 = 0xE0 // 1110 0000
	t4 = 0xF0 // 1111 0000
	t5 = 0xF8 // 1111 1000

	maskx = 0x3F // 0011 1111
	mask2 = 0x1F // 0001 1111
	mask3 = 0x0F // 0000 1111
	mask4 = 0x07 // 0000 0111

	rune1Max = 1<<7 - 1
	rune2Max = 1<<11 - 1
	rune3Max = 1<<16 - 1
)

// decodeRuneInternal decodes a given byte sequence into a UTF-32 codepoint
func decodeRuneInternal(p []byte) (r rune, size int) {
	n := len(p)
	if n < 1 {
		return utf8.RuneError, 0
	}
	c0 := p[0]

	// 1-byte, 7-bit sequence?
	if c0 < tx {
		return rune(c0), 1
	}

	// unexpected continuation byte?
	if c0 < t2 {
		return utf8.RuneError, 1
	}

	// need first continuation byte
	if n < 2 {
		return utf8.RuneError, 1
	}
	c1 := p[1]
	if c1 < tx || t2 <= c1 {
		return utf8.RuneError, 1
	}

	// 2-byte, 11-bit sequence?
	if c0 < t3 {
		r = rune(c0&mask2)<<6 | rune(c1&maskx)
		if r <= rune1Max {
			return utf8.RuneError, 1
		}
		return r, 2
	}

	// need second continuation byte
	if n < 3 {
		return utf8.RuneError, 1
	}
	c2 := p[2]
	if c2 < tx || t2 <= c2 {
		return utf8.RuneError, 1
	}

	// 3-byte, 16-bit sequence?
	if c0 < t4 {
		r = rune(c0&mask3)<<12 | rune(c1&maskx)<<6 | rune(c2&maskx)
		if r <= rune2Max {
			return utf8.RuneError, 1
		}
		// do not throw error on surrogates // (*)
		//if surrogateMin <= r && r <= surrogateMax {
		//	return RuneError, 1, false
		//}
		return r, 3
	}

	// need third continuation byte
	if n < 4 {
		return utf8.RuneError, 1
	}
	c3 := p[3]
	if c3 < tx || t2 <= c3 {
		return utf8.RuneError, 1
	}

	// 4-byte, 21-bit sequence?
	if c0 < t5 {
		r = rune(c0&mask4)<<18 | rune(c1&maskx)<<12 | rune(c2&maskx)<<6 | rune(c3&maskx)
		if r <= rune3Max || utf8.MaxRune < r {
			return utf8.RuneError, 1
		}
		return r, 4
	}

	// error
	return utf8.RuneError, 1
}
