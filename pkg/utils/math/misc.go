package math

func Modulo(a, b int64) int64 {
	m := a % b
	if a < 0 && b < 0 {
		m -= b
	}
	if a < 0 && b > 0 {
		m += b
	}
	return m
}

func AbsDiffInt(x, y int32) int32 {
	if x < y {
		return y - x
	}
	return x - y
}

func SaturatingAddUint8(x uint8, y int32) uint8 {
	x32 := int32(x)

	z32 := x32 + y
	if z32 > 255 {
		return 255
	} else if z32 < 0 {
		return 0
	} else {
		return uint8(z32)
	}
}
