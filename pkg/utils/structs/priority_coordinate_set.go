package structs

import (
	"orienteermap-go-app/pkg/types/basic"
	"runtime"
	"sync"
)

// MaxBatchSize limits the maximum amount of coordinates to be processed at once. It is assumed to be 10 chunks per each CPU core
var MaxBatchSize = 10 * runtime.GOMAXPROCS(0)

// PriorityKey maps a combination of a priority and a coordinate. It is not an error to have the same coordinate for multiple priorities,
// as a single priority should only correspond to a single task
type PriorityKey struct {
	priority uint8
	coord    basic.CoordinatePair
}

// PriorityCoordinateSet is a form of a set partitioned by a priority. When retrieving a batch, the highest priority keys are chosen, and only that
// is returned at one time.
type PriorityCoordinateSet struct {
	// coords contains mappings of coordinates per priorities
	coords map[PriorityKey]struct{}
	// readLock for read-only access, indicating length
	readLock sync.Locker
	// accessCondVariable for signaling and controlling write access to map
	accessCondVariable *sync.Cond
	// hasFlushableItems indicates if there are items that should be flushed
	hasFlushableItems bool
	// highestPriority indicates the largest priority that has been added to the set
	highestPriority uint8
	// onceInitialization ensures that the initialization is ran only once
	onceInitialization sync.Once
}

func (PCS *PriorityCoordinateSet) lazyInit() {
	PCS.onceInitialization.Do(func() {
		rwLock := &sync.RWMutex{}
		condVar := sync.NewCond(rwLock)

		PCS.coords = make(map[PriorityKey]struct{})
		PCS.readLock = rwLock.RLocker()
		PCS.accessCondVariable = condVar
	})
}

// Len returns the total count of keys (including all priorities) in the set
func (PCS *PriorityCoordinateSet) Len() int {
	PCS.lazyInit()

	PCS.readLock.Lock()
	defer PCS.readLock.Unlock()

	return len(PCS.coords)
}

// Quiesce removes all data from the set, effectively canceling any and all pending coordinate requests
// Optionally, it may also trigger an empty flush for exactly one waiting call (but not many; call multiple times if multiple calls need to be forced to exit)

func (PCS *PriorityCoordinateSet) Quiesce(triggerFlush bool) {
	PCS.lazyInit()

	PCS.accessCondVariable.L.Lock()
	defer PCS.accessCondVariable.L.Unlock()

	PCS.hasFlushableItems = triggerFlush
	PCS.highestPriority = 0

	for k := range PCS.coords {
		delete(PCS.coords, k)
	}

	if triggerFlush {
		PCS.accessCondVariable.Signal()
	}
}

// FlushBatch waits until a batch of coordinates is available, and returns them along with the priority of the said coordinates
// It is not guaranteed for this to return a non-empty batch in unusual scenarios
func (PCS *PriorityCoordinateSet) FlushBatch() (uint8, []basic.CoordinatePair) {
	PCS.lazyInit()

	PCS.accessCondVariable.L.Lock()

	for !PCS.hasFlushableItems {
		PCS.accessCondVariable.Wait()
	}

	var resLst []basic.CoordinatePair
	for k := range PCS.coords {
		if k.priority != PCS.highestPriority {
			continue
		}

		resLst = append(resLst, k.coord)
		delete(PCS.coords, k)

		if len(resLst) >= MaxBatchSize {
			break
		}
	}

	PCS.hasFlushableItems = len(PCS.coords) > 0

	priority := PCS.highestPriority

	go func() {
		defer PCS.accessCondVariable.L.Unlock()

		var highestPriorityFound uint8 = 0
		for k := range PCS.coords {
			if k.priority > highestPriorityFound {
				highestPriorityFound = k.priority
			}
		}

		PCS.highestPriority = highestPriorityFound
	}()

	return priority, resLst
}

func (PCS *PriorityCoordinateSet) Insert(priority uint8, chunks ...basic.CoordinatePair) {
	PCS.lazyInit()

	if len(chunks) == 0 {
		return
	}

	PCS.accessCondVariable.L.Lock()
	defer PCS.accessCondVariable.L.Unlock()

	for _, v := range chunks {
		PCS.coords[PriorityKey{
			priority: priority,
			coord:    v,
		}] = struct{}{}
	}

	if PCS.highestPriority < priority {
		PCS.highestPriority = priority
	}

	PCS.hasFlushableItems = true
	PCS.accessCondVariable.Signal()
}
