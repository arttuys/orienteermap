package structs

import (
	"orienteermap-go-app/pkg/types/basic"
	"sync"
	"testing"
)

func TestPriorityCoordinateSetRoundtrip(t *testing.T) {
	set := PriorityCoordinateSet{}

	var syncWaitGroup sync.WaitGroup
	for i := 0; i < 121; i++ {
		syncWaitGroup.Add(2)

		i := i

		go func() {
			set.Insert(1, basic.CoordinatePair{-1, int32(i)}, basic.CoordinatePair{1 + int32(i), (1 + int32(i)) * 123}, basic.CoordinatePair{-1, int32(i)})
			syncWaitGroup.Done()
		}()

		go func() {
			set.Insert(2, basic.CoordinatePair{-2, int32(i)}, basic.CoordinatePair{1 + int32(i), (1 + int32(i)) * (123 * 2)}, basic.CoordinatePair{-2, int32(i)})
			syncWaitGroup.Done()
		}()
	}

	syncWaitGroup.Wait()

	var resCount int
	var curPriority uint8 = 2 // Initialize to highest known priority

	// Set maximum batch to be artificially low
	oldMaxBatch := MaxBatchSize
	MaxBatchSize = 5
	for {
		if set.Len() == 0 {
			break
		}

		gotPriority, resultLst := set.FlushBatch()
		resCount += len(resultLst)

		t.Logf("got priority %d : %v\n", gotPriority, resultLst)

		if gotPriority > curPriority {
			t.Fatalf("priority went in reverse: %d > %d", gotPriority, curPriority)
		}

		curPriority = gotPriority

		for _, v := range resultLst {
			if v[0] < 0 {
				if v[0] != int32(gotPriority)*-1 {
					t.Fatalf("unexpected value by priority, expected %d, got %d", int32(gotPriority)*-1, v[0])
				}
				if v[1] < 0 || v[1] > 999 {
					t.Fatal("received an unexpected value: ", v)
				}
			} else {
				if v[0]*(123*int32(curPriority)) != v[1] {
					t.Fatal("mismatching pair: ", v)
				}
			}
		}
	}

	if resCount != 484 {
		t.Fatalf("mismatching count: expected 480, got %d", resCount)
	}

	MaxBatchSize = oldMaxBatch
}

func TestPriorityCoordinateSet_Quiesce(t *testing.T) {
	set := PriorityCoordinateSet{}

	// Insert one...
	set.Insert(1, basic.CoordinatePair{-1, 3})

	// ... but trigger a flush before
	set.Quiesce(true)

	priority, batch := set.FlushBatch()
	if priority != 0 {
		t.Fatalf("unexpected priority, should be zero: %d", priority)
	}

	if len(batch) != 0 {
		t.Fatalf("unexpected data returned, should be none: %v", batch)
	}
}
