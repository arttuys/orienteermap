package test_helpers

import (
	"fmt"
	"math"
	"orienteermap-go-app/pkg/types/basic"
	"orienteermap-go-app/pkg/types/interfaces"
	"testing"
	"time"
)

func CheckChunkExists(t *testing.T, cmq interfaces.ChunkMetadataQueryable, chunk basic.CoordinatePair) {
	if exists, err := cmq.ChunkExists(chunk); err != nil || !exists {
		if err == nil {
			err = fmt.Errorf("expected chunk to exist, but did not find it")
		}

		t.Fatal(err)
	}
}

func VerifyModificationTimesCloseToNow(t *testing.T, cmq interfaces.ChunkMetadataQueryable, chunks ...basic.CoordinatePair) {
	// Expect modification time to be relatively close to the current moment, in the order of 3 minutes at max
	modTimes, err := cmq.GetChunkModificationTimes(chunks, true)
	if err != nil {
		t.Fatal(err)
	}

	if l := len(modTimes); l != len(chunks) {
		t.Fatal(fmt.Errorf("%d modification times should be found, %d found instead", len(chunks), l))
	}

	now := time.Now()

	for _, modTime := range modTimes {
		diff := now.Sub(modTime)

		if math.Abs(diff.Minutes()) > 3 {
			t.Fatal(fmt.Errorf("too large time difference: %v", diff))
		}
	}
}

func CheckNChunksExist(t *testing.T, provider interfaces.ChunkMetadataQueryable, n int) {
	list, err := provider.GetListOfChunks()
	if l := len(list); err != nil || l != n {
		if err == nil {
			err = fmt.Errorf("should have found %d chunks, found %d", n, l)
		}

		t.Fatal(err)
	}
}

func CheckChunkDoesNotExist(t *testing.T, cmq interfaces.ChunkMetadataQueryable, chunk basic.CoordinatePair) {
	// Assert it does not exist
	if exists, err := cmq.ChunkExists(chunk); err != nil || exists {
		if err == nil {
			err = fmt.Errorf("expected chunk not to exist, but found it")
		}

		t.Fatal(err)
	}
}

func CheckChunksFoundInListingExact(t *testing.T, cmq interfaces.ChunkMetadataQueryable, chunks ...basic.CoordinatePair) {
	list, err := cmq.GetListOfChunks()
	if l := len(list); err != nil || l != len(chunks) {
		if err == nil {
			err = fmt.Errorf("should have found %d chunks, found %d", len(chunks), l)
		}

		t.Error(err)
		return
	}

outerLoop:
	for _, v := range chunks {
		for _, v2 := range list {
			if v == v2 {
				continue outerLoop
			}
		}

		t.Fatalf("did not find a match for %v", v)
	}
}
