package tests

import (
	"encoding/json"
	"orienteermap-go-app/pkg/mc_formats/anvil/mca"
	"orienteermap-go-app/pkg/mc_formats/shared"
	"os"
	"path/filepath"
	"sync"
	"testing"
)

// TestParseAsChunk tests that a chunk can be loaded as a chunk struct
func TestParseAsChunk(t *testing.T) {
	singleChunkInner(t, "simple_test.mca", true, 255, 0)
}

// TestParseAsChunk118 tests that a 1.18 chunk (larger world height) can be loaded as a chunk struct
func TestParseAsChunk118(t *testing.T) {
	singleChunkInner(t, "1.18.mca", false, 319, -64)
}

// singleChunkInner implements the actual tests for chunk loading, with varying parameters as per test cases
func singleChunkInner(t *testing.T, file string, printOut bool, expectedMaxHeight int32, expectedMinHeight int32) {
	// Load file
	basePath, err := os.Getwd()
	if err != nil {
		t.Fatal(err)
	}

	t.Logf("Base path: %s", basePath)

	filePath := filepath.Join(basePath, "..", "..", "..", "..", "test", "datasets", "mca", file)

	t.Logf("Final file path: %s", filePath)

	fileSeeker, err := os.Open(filePath)

	if err != nil {
		t.Fatal(err)
	}

	// Do multithreaded testing as with Bigtest
	var startBarrier = sync.NewCond(&sync.Mutex{})
	var canGo = false
	var endWaitGroup sync.WaitGroup
	var locker sync.Mutex
	for i := 0; i < 50; i++ {
		endWaitGroup.Add(1)
		i := i
		go func() {
			defer endWaitGroup.Done()

			// Wait until condition variable changes
			startBarrier.L.Lock()
			for !canGo {
				startBarrier.Wait()
			}
			startBarrier.L.Unlock()

			regionReader := mca.NewReaderAdvanced(fileSeeker, 0, 0, &locker, i%3 == 0)

			var chunk *shared.Chunk
			found, err := regionReader.ReadData(0, 0, &chunk)

			// Assert that no error was found
			if err != nil {
				t.Error(err)
				return
			}

			if chunk == nil || found == false {
				t.Error("did not find expected chunk")
				return
			}

			// Test conversion to JSON
			jsonRes, err := json.Marshal(&chunk)

			if err != nil {
				t.Error(err)
				return
			}

			if printOut && i == 0 {
				t.Logf("Found data: %v", string(jsonRes))
			}

			t.Logf("Chunk data version: %v\n", chunk.DataVersion)

			if chunk.Level.XPos != 0 || chunk.Level.ZPos != 0 {
				t.Errorf("Outside expected chunk: %v,%v", chunk.Level.XPos, chunk.Level.ZPos)
				return
			}

			if h := chunk.Level.BlockSectionData.MinY; h != expectedMinHeight {
				t.Errorf("Did not find expected min height as a level definition: found %d, expected %d", h, expectedMaxHeight)
				return
			}

			if h := chunk.Level.BlockSectionData.MaxY; h != expectedMaxHeight {
				t.Errorf("Did not find expected max height as a level definition: found %d, expected %d", h, expectedMaxHeight)
				return
			}

			if h := chunk.Level.Heightmaps["MOTION_BLOCKING"].Data[[2]int32{0xD, 0x7}]; h != expectedMaxHeight {
				t.Errorf("Did not find expected max height at 0xD,0x7: found %d, expected %d", h, expectedMaxHeight)
				return
			}
			if h := chunk.Level.Heightmaps["MOTION_BLOCKING"].Data[[2]int32{0xD, 0x8}]; h != expectedMinHeight {
				t.Errorf("Did not find expected min height at 0xD,0x8: found %d, expected %d", h, expectedMaxHeight)
				return
			}

			// Test chunk block data
			if chunk.Level.BlockSectionData.Blocks[[3]int32{9, 89, 3}].Name != "minecraft:oak_wall_sign" {
				t.Error("Did not find expected sign")
				return
			}

			// Test that a sign was found as an entity at 9, 89, 30
			for _, arr := range chunk.Level.BlockEntityData.IDToLocations["minecraft:sign"] {
				if arr == [3]int32{9, 89, 3} {
					goto blockEntityTestPassed
				}
			}

			t.Error("Did not find expected sign entity")
			return

		blockEntityTestPassed:

			t.Log("All ok!\n")
		}()
	}

	// Set condition variable and allow all goroutines to proceed
	startBarrier.L.Lock()
	canGo = true
	startBarrier.Broadcast()
	startBarrier.L.Unlock()

	endWaitGroup.Wait()
}
