package tests

import (
	"orienteermap-go-app/pkg/mc_formats/shared"
	"testing"
)

// Test_getBitIndiceSize ensures that bit indice sizes for block section palettes work
func Test_getBitIndiceSize(t *testing.T) {
	type args struct {
		len int64
	}
	tests := []struct {
		name string
		args args
		want uint8
	}{
		{"simple 1", args{len: 1}, 4},
		{"simple 2", args{len: 16}, 4},
		{"simple 3", args{len: 17}, 5},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := shared.GetPaletteBitIndiceSize(tt.args.len); got != tt.want {
				t.Errorf("getBitIndiceSize() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestGlobalToInChunkRelativeCoord(t *testing.T) {
	type args struct {
		c int32
	}
	tests := []struct {
		name string
		args args
		want int32
	}{
		{"positive chunk coordinates are appropriately handled", args{1}, 1},
		{"negative chunk coordinates are appropriately handled", args{-1}, 15},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := shared.GlobalToInChunkRelativeCoord(tt.args.c); int32(got) != tt.want {
				t.Errorf("GlobalToInChunkRelativeCoord() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestChunkCoordinatesToRegionFileOffset(t *testing.T) {
	type args struct {
		x int32
		z int32
	}
	tests := []struct {
		name string
		args args
		want uint64
	}{
		{"simple case", args{0, 0}, 0},
		{"negative case", args{-2, 1}, 62},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := shared.ChunkCoordinatesToRegionFileLocation(tt.args.x, tt.args.z); got != tt.want {
				t.Errorf("ChunkCoordinatesToRegionFileOffset() = %v, want %v", got, tt.want)
			}
		})
	}
}
