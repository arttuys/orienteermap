package shared

import (
	"orienteermap-go-app/pkg/mc_formats/nbt"
	"orienteermap-go-app/pkg/types/basic"
)

type PointsOfInterest struct {
	Data        POIData `nbt:"Sections"`
	DataVersion int32
}

type POIData struct {
	POIs map[basic.CoordinateTriplet]string
}

func (pd *POIData) DeserializeFromData(rawMessage *nbt.RawMessage) error {
	// Try to deserialize it as a list of internal block sections
	var sections map[string]poiRaw
	if err := rawMessage.DeserializeInto(&sections); err != nil {
		return err
	}

	pd.POIs = make(map[basic.CoordinateTriplet]string)

	for _, section := range sections {
		if !section.Valid {
			continue
		}

		for _, record := range section.Records {
			x := GlobalToInChunkRelativeCoord(record.Pos[0])
			z := GlobalToInChunkRelativeCoord(record.Pos[2])

			pd.POIs[[3]int32{x, record.Pos[1], z}] = record.Type
		}
	}

	return nil
}

type poiRaw struct {
	Valid   bool
	Records []poiRawInner
}

type poiRawInner struct {
	Type string   `nbt:"type"`
	Pos  [3]int32 `nbt:"pos"`
}
