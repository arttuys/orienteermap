package shared

import (
	"fmt"
	"orienteermap-go-app/pkg/mc_formats/nbt"
	"orienteermap-go-app/pkg/mc_formats/shared/block_entities"
	"orienteermap-go-app/pkg/types/basic"
)

type BlockEntities struct {
	MappingToStruct map[basic.CoordinateTriplet]interface{}
	IDToLocations   map[string][]basic.CoordinateTriplet
}

type blockEntityCommon struct {
	Id string `nbt:"id"`
	X  int32  `nbt:"x"`
	Y  int32  `nbt:"y"`
	Z  int32  `nbt:"z"`
}

func (b *BlockEntities) generateInterfaceFromRawMessage(t string, msg *nbt.RawMessage) (interface{}, error) {
	switch t {
	case "minecraft:sign":
		var sign block_entities.SignEntity
		if err := msg.DeserializeInto(&sign); err != nil {
			return nil, err
		}
		return sign, nil
	default:
		// If we don't know what it is, return a raw message
		return msg, nil
	}
}

func (b *BlockEntities) DeserializeFromData(rawMessage *nbt.RawMessage) error {
	// Try to deserialize it as a list of raw messages
	var tags []*nbt.RawMessage
	if err := rawMessage.DeserializeInto(&tags); err != nil {
		return err
	}

	b.MappingToStruct = make(map[basic.CoordinateTriplet]interface{})
	b.IDToLocations = make(map[string][]basic.CoordinateTriplet)

	for _, tag := range tags {
		var commonData blockEntityCommon
		if err := tag.DeserializeInto(&commonData); err != nil {
			return err
		}

		if commonData.Id == "INVALID" {
			continue
		}

		// Location triplet
		location := [3]int32{GlobalToInChunkRelativeCoord(commonData.X), commonData.Y, GlobalToInChunkRelativeCoord(commonData.Z)}

		// We could deserialize it, save it to entity map
		b.IDToLocations[commonData.Id] = append(b.IDToLocations[commonData.Id], location)
		data, err := b.generateInterfaceFromRawMessage(commonData.Id, tag)
		if err != nil {
			return fmt.Errorf("could not construct block entity at %v: %w", commonData, err)
		}
		b.MappingToStruct[location] = data
	}

	return nil
}
