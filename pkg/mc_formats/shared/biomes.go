package shared

import (
	"orienteermap-go-app/pkg/mc_formats/nbt"
	"orienteermap-go-app/pkg/types/basic"
)

type Biomes struct {
	BiomeTypes map[basic.CoordinateTriplet]int32
}

func (b *Biomes) ReadjustToWorldLevel(minY int32) {
	newMap := make(map[basic.CoordinateTriplet]int32)

	for k, v := range b.BiomeTypes {
		newMap[basic.CoordinateTriplet{k[0], k[1] + minY, k[2]}] = v
	}

	b.BiomeTypes = newMap
}

func (b *Biomes) DeserializeFromData(rawMessage *nbt.RawMessage) error {
	// Try to deserialize it as a slice of integers
	var integers []int32
	if err := rawMessage.DeserializeInto(&integers); err != nil {
		return err
	}

	b.BiomeTypes = make(map[basic.CoordinateTriplet]int32)

	cx := 0
	cy := 0
	cz := 0
	for _, biomeId := range integers {
		for x := 0; x < 4; x++ {
			for y := 0; y < 4; y++ {
				for z := 0; z < 4; z++ {
					coordinate := basic.CoordinateTriplet{int32(cx*4 + x), int32(cy*4 + y), int32(cz*4 + z)}
					b.BiomeTypes[coordinate] = biomeId
				}
			}
		}

		cx += 1
		if cx == 4 {
			cz += 1
			cx = 0
		}
		if cz == 4 {
			cy += 1
			cz = 0
		}
	}

	return nil
}
