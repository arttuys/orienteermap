package shared

type Level struct {
	Data LevelSettings
}

type LevelSettings struct {
	AllowCommands    bool `nbt:"allowCommands"`
	DataVersion      int32
	Difficulty       int8
	DifficultyLocked bool
	GameRules        map[string]string
	Hardcore         bool `nbt:"hardcore"`
	Initialized      bool `nbt:"initialized"`
	LastPlayed       int64
	WasModded        bool
}
