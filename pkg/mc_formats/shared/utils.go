package shared

import (
	"math"
	"orienteermap-go-app/pkg/types/basic"
	extMath "orienteermap-go-app/pkg/utils/math"
)

func nibble4(arr []int8, index uint32) uint8 {
	if index%2 == 0 {
		return uint8(arr[index/2] & 0x0F)
	} else {
		return uint8((arr[index/2] >> 4) & 0x0F)
	}
}

func readNibble4ToMap(arr []int8, baseY int32, m map[basic.CoordinateTriplet]uint8) {
	// If array is empty, implicitly omit
	if arr == nil {
		return
	}

	var i uint32 = 0
	for y := 0; y < 16; y++ {
		for z := 0; z < 16; z++ {
			for x := 0; x < 16; x++ {
				m[[3]int32{int32(x), int32(y) + baseY, int32(z)}] = nibble4(arr, i)
				i++
			}
		}
	}
}

// getBitFromArray gets a single bit, from start to end of array, right (smallest bit first) to the left
func getBitFromArray(arr []int64, bit uint64) bool {
	rem := bit % 64
	arrI := (bit - rem) / 64

	return ((arr[arrI] >> rem) & 1) == 1
}

// GetPaletteBitIndiceSize indicates how many bits a palette entry requires (block sections)
func GetPaletteBitIndiceSize(len int64) uint8 {
	v := uint8(math.Ceil(math.Log2(float64(len))))
	if v < 4 {
		v = 4
	}

	return v
}

// GlobalToChunkCoord converts a global coordinate (absolute position in the world) to a chunk coordinate (in which chunk this coordinate lies)
func GlobalToChunkCoord(c int32) int32 {
	return c >> 4
}

// ChunkToRegionCoord converts a chunk coordinate (position of chunk in relation to other chunks) to a region coordinate (which region file this coord is found in)
func ChunkToRegionCoord(c int32) int32 {
	return c >> 5
}

// GlobalToRegionCoord converts a global coordinate to a region coordinate.
func GlobalToRegionCoord(c int32) int32 {
	return ChunkToRegionCoord(GlobalToChunkCoord(c))
}

// GlobalToInChunkRelativeCoord converts a global coordinate to an in-chunk relative coordinate (what element in a 16x16 grid this coordinate would be in)
func GlobalToInChunkRelativeCoord(c int32) int32 {
	return int32(extMath.Modulo(int64(c), 16))
}

// ChunkToGlobalCoordinate converts a chunk-specific coordinate to a global coordinate, given appropriate chunk XZ positions
func ChunkToGlobalCoordinate(chunkC int32, c int32) int32 {
	return chunkC*16 + c
}

// ChunkCoordinatesToRegionFileLocation translates a given chunk coordinate to a region file offset
func ChunkCoordinatesToRegionFileLocation(x int32, z int32) uint64 {
	return uint64((x & 31) + ((z & 31) * 32))
}
