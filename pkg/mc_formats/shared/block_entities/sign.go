package block_entities

import "orienteermap-go-app/pkg/mc_formats/nbt"

type SignEntity struct {
	Color   string
	Glowing bool
	Text    [4]string
}

type signEntityAsNBT struct {
	Color       string
	GlowingText bool
	Text1       string
	Text2       string
	Text3       string
	Text4       string
}

func (s *SignEntity) DeserializeFromData(rawMessage *nbt.RawMessage) error {
	var rawSignEntity signEntityAsNBT

	if err := rawMessage.DeserializeInto(&rawSignEntity); err != nil {
		return err
	}

	s.Color = rawSignEntity.Color
	s.Glowing = rawSignEntity.GlowingText
	s.Text = [4]string{
		rawSignEntity.Text1,
		rawSignEntity.Text2,
		rawSignEntity.Text3,
		rawSignEntity.Text4,
	}

	return nil
}
