package shared

import (
	"fmt"
	"orienteermap-go-app/pkg/mc_formats/nbt"
	"orienteermap-go-app/pkg/types/basic"
)

type Heightmap struct {
	Data map[basic.CoordinatePair]int32
}

// readjustForCustomWorldHeight shifts heights down by a given offset - due to data storage format, they start at zero but then are logically considered shifted. We use actual coordinates
func (h *Heightmap) readjustForCustomWorldHeight(minY int32) {
	for key, v := range h.Data {
		h.Data[key] = v + minY
	}
}

// generateDataMap generates the heightmap from given raw data
func generateDataMap(base []int64) (map[basic.CoordinatePair]int32, error) {
	if l := len(base); l != 37 && l != 36 {
		return nil, fmt.Errorf("unrecognized heightmap format: length must be 36 or 37, got %d", l)
	}

	skipTrailingBit := len(base) == 37

	m := make(map[basic.CoordinatePair]int32)

	bitIdx := uint64(0)

	for z := int32(0); z < 16; z++ {
		for x := int32(0); x < 16; x++ {
			if skipTrailingBit && (bitIdx%64 == 63) {
				bitIdx += 1
			}
			pendingVal := int32(0)
			for bd := 0; bd < 9; bd++ {
				// If a positive bit, set
				if getBitFromArray(base, bitIdx) {
					pendingVal |= 1 << bd
				}
				bitIdx++
			}

			// Observation; for several structures, the provided value seems to be one higher than the actual block height,
			// with zero indicating non-existence..?
			// This attempts to adjust for this
			if pendingVal > 0 {
				m[[2]int32{x, z}] = pendingVal - 1
			}
		}
	}

	return m, nil
}

func (h *Heightmap) DeserializeFromData(rawMessage *nbt.RawMessage) error {
	// Try to deserialize it as a slice of longs
	var longs []int64
	if err := rawMessage.DeserializeInto(&longs); err != nil {
		return err
	}

	data, err := generateDataMap(longs)

	if err != nil {
		return err
	}

	h.Data = data

	return nil
}
