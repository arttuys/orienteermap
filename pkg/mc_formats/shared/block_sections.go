package shared

import (
	"fmt"
	"math"
	"orienteermap-go-app/pkg/mc_formats/nbt"
	"orienteermap-go-app/pkg/types/basic"
)

type BlockData struct {
	BlockLightLevels       map[basic.CoordinateTriplet]uint8
	MaxBlockSkyLightLevels map[basic.CoordinateTriplet]uint8
	// Blocks obviously repeat quite often, so it makes sense to store them as pointers instead of duplicating values
	Blocks map[basic.CoordinateTriplet]*Block
	MaxY   int32
	MinY   int32
}

type Block struct {
	Name       string
	Properties map[string]interface{}
}

type InternalBlockSection struct {
	BlockLight  []int8
	SkyLight    []int8
	BlockStates []int64
	Y           int8
	Palette     []Block
}

// getReadParameters constructs the required reading parameters - depending on the version of the world, the format may change slightly
func getReadParameters(paletteBits uint8, blockStateLength int) (padBits bool, padBy uint8, padAfterEach uint8, yStart int32, yEnd int32, err error) {
	// First test for base case
	padBits = false
	padBy = 0
	padAfterEach = 0
	yStart = 0
	yEnd = 16

	// Calculate expected size if it were that bits were cleanly split across longs
	// 4096 indices, times palette bits, split into longs
	expectedJoinedBitSize := int(math.Ceil(float64(paletteBits) * 4096.0 / 64.0))

	if blockStateLength == expectedJoinedBitSize {
		// No padding required, matches as expected for 4096 (aka 16x16x16, classic 255 height)
		return
	} else {
		// Padding required
		padBits = true
	}

	padAfterEach = 64 / paletteBits
	padBy = 64 - (paletteBits * padAfterEach)

	// What the expected length would be if it still were exactly 16x16x16, aka 4096 divided by pad-after-each. Does it match?
	expectedPaddedBitSize := int(math.Ceil(4096.0 / float64(padAfterEach)))

	if expectedPaddedBitSize != blockStateLength {
		err = fmt.Errorf("unknown block state format, cannot be handled yet: palette bits %d, but length %d", paletteBits, blockStateLength)
	}

	return
}

func (bd *BlockData) DeserializeFromData(rawMessage *nbt.RawMessage) error {
	// Try to deserialize it as a list of internal block sections
	var sections []InternalBlockSection
	if err := rawMessage.DeserializeInto(&sections); err != nil {
		return err
	}

	bd.BlockLightLevels = make(map[basic.CoordinateTriplet]uint8)
	bd.MaxBlockSkyLightLevels = make(map[basic.CoordinateTriplet]uint8)
	bd.Blocks = make(map[basic.CoordinateTriplet]*Block)
	for _, section := range sections {
		baseY := int32(section.Y) * 16

		// Read light data, omitting data where missing
		readNibble4ToMap(section.BlockLight, baseY, bd.BlockLightLevels)
		readNibble4ToMap(section.SkyLight, baseY, bd.MaxBlockSkyLightLevels)

		if len(section.BlockStates) == 0 {
			// Empty block state? Ignore for block reading and max/min height calculation purposes
			continue
		}

		// Next, look at palette data
		paletteBits := GetPaletteBitIndiceSize(int64(len(section.Palette)))

		padBits, padBy, padAfterEach, yStart, yEnd, detErr := getReadParameters(paletteBits, len(section.BlockStates))

		if detErr != nil {
			return detErr
		}

		if gy := yStart + baseY; gy < bd.MinY {
			bd.MinY = gy
		}

		if gy := yEnd - 1 + baseY; gy > bd.MaxY {
			bd.MaxY = gy
		}

		bitI := uint64(0)
		numCounter := uint64(0)

		for y := yStart; y < yEnd; y++ {
			for z := 0; z < 16; z++ {
				for x := 0; x < 16; x++ {

					index := 0
					for i := uint8(0); i < paletteBits; i++ {
						if getBitFromArray(section.BlockStates, bitI) {
							index |= 1 << i
						}
						bitI++
					}

					// We have an index, save it
					location := [3]int32{int32(x), y + baseY, int32(z)}
					bd.Blocks[location] = &section.Palette[index]

					numCounter += 1
					if padBits && numCounter%uint64(padAfterEach) == 0 {
						bitI += uint64(padBy)
					}
				}
			}
		}
	}

	return nil
}
