package shared

import (
	"orienteermap-go-app/pkg/mc_formats/nbt"
	"orienteermap-go-app/pkg/types/basic"
)

type Entities struct {
	Entities    map[basic.CoordinateTriplet]string
	DataVersion int32
}

func (e *Entities) DeserializeFromData(message *nbt.RawMessage) error {
	var raw entitiesRaw
	if err := message.DeserializeInto(&raw); err != nil {
		return err
	}

	e.DataVersion = raw.DataVersion
	e.Entities = make(map[basic.CoordinateTriplet]string)
	for _, entity := range raw.Entities {
		x := GlobalToInChunkRelativeCoord(int32(entity.Position[0]))
		z := GlobalToInChunkRelativeCoord(int32(entity.Position[2]))

		e.Entities[[3]int32{x, int32(entity.Position[1]), z}] = entity.Id
	}

	return nil
}

type entitiesRaw struct {
	Entities    []entity
	DataVersion int32
}

type entity struct {
	Id       string     `nbt:"id"`
	Position [3]float64 `nbt:"Pos"`
}
