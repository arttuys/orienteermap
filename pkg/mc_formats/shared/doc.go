// Package shared contains various mixed assets which are shared by various parts of the program.
// These include, for example, datatypes and conversion utilities for chunks, and interface definitions
package shared
