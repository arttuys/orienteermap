package shared

type Chunk struct {
	DataVersion int32
	IsTallWorld bool `nbt:"-"`
	Level       ChunkData
}

type ChunkData struct {
	LastUpdate       int64
	Status           string
	Biomes           *Biomes
	Heightmaps       map[string]Heightmap
	BlockSectionData BlockData     `nbt:"Sections"`
	BlockEntityData  BlockEntities `nbt:"TileEntities"`
	XPos             int32         `nbt:"xPos"`
	ZPos             int32         `nbt:"zPos"`
}

func (c *Chunk) DeserializePostprocess() error {
	if c.DataVersion >= 2730 {
		c.IsTallWorld = true

		for k, v := range c.Level.Heightmaps {
			v.readjustForCustomWorldHeight(c.Level.BlockSectionData.MinY)
			c.Level.Heightmaps[k] = v
		}

		if c.Level.Biomes != nil {
			c.Level.Biomes.ReadjustToWorldLevel(c.Level.BlockSectionData.MinY)
		}
	}

	return nil
}
