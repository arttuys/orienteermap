package shared

type ChunkBundle struct {
	Chunk    *Chunk
	Entities *Entities
	POI      *PointsOfInterest
}
