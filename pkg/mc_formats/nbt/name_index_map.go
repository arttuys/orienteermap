package nbt

import (
	"reflect"
	"sync"
)

// Declare type for name-index maps
type nameIndexMap map[string]int

// Cache known types, as they will not change
var typeCache sync.Map

// constructTypeInfoMap constructs a map of string-index pairs for mutating a struct
func constructNameIndexMap(t reflect.Type) nameIndexMap {
	if ni, ok := typeCache.Load(t); ok {
		return ni.(nameIndexMap)
	}

	m := make(nameIndexMap)

	if t.Kind() == reflect.Struct {
		count := t.NumField()

		for i := 0; i < count; i++ {
			f := t.Field(i)
			tag := f.Tag.Get("nbt")
			if (f.PkgPath != "" && !f.Anonymous) || tag == "-" {
				// Unexported or explicitly omitted field
				continue
			}

			// If a tag has been explicitly set, use it as the canonical name - otherwise use field name
			if tag != "" {
				m[tag] = i
			} else {
				m[f.Name] = i
			}
		}
	}

	ni, _ := typeCache.LoadOrStore(t, m)
	return ni.(nameIndexMap)
}
