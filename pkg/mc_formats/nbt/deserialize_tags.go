package nbt

import (
	"fmt"
	"reflect"
)

func (nbt *Deserializer) deserializeCompound(v reflect.Value) error {
	switch vk := v.Kind(); vk {
	default:
		return fmt.Errorf("cannot parse TagCompound as %s", vk.String())
	case reflect.Struct:
		nameIndexMap := constructNameIndexMap(v.Type())
		for {
			tagType, tagName, err := nbt.readTagHeader()
			if err != nil {
				return err
			}
			if tagType == TagEnd {
				break
			}

			fieldIndex, ok := nameIndexMap[tagName]
			if ok {
				err := nbt.DeserializeIntoRaw(tagType, tagName, v.Field(fieldIndex))
				if err != nil {
					return fmt.Errorf("failed to construct a struct from compound tag (type %d, name %s): %v", tagType, tagName, err)
				}
			} else {
				// Unknown, ignore
				if err := nbt.discardRead(tagType); err != nil {
					return err
				}
			}
		}

		// Check if we can interface to postprocessing
		if v.CanAddr() {
			va := v.Addr()
			if va.Type().NumMethod() > 0 && va.CanInterface() {
				if u, ok := va.Interface().(CustomDeserializePostprocess); ok {
					if err := u.DeserializePostprocess(); err != nil {
						return err
					}
				}
			}
		}

		return nil
	case reflect.Interface:
		// Construct a new map
		buf := make(map[string]interface{})
		for {
			tagType, tagName, err := nbt.readTagHeader()
			if err != nil {
				return err
			}
			if tagType == TagEnd {
				break
			}

			var iv interface{}
			if err := nbt.DeserializeIntoRaw(tagType, tagName, reflect.ValueOf(&iv).Elem()); err != nil {
				return fmt.Errorf("failed to decode a part of a compound tag (type %d, name %s): %v", tagType, tagName, err)
			}

			// Save to map
			buf[tagName] = iv
		}

		// Finally, assign to interface
		v.Set(reflect.ValueOf(buf))
		return nil
	case reflect.Map:
		// Parse as a map; check we can reuse it
		if v.Type().Key().Kind() != reflect.String {
			return fmt.Errorf("keys for tag compound maps are required to be strings, but got %s", v.Type().Key().Kind().String())
		}
		if v.IsNil() {
			v.Set(reflect.MakeMap(v.Type()))
		}
		for {
			tagType, tagName, err := nbt.readTagHeader()
			if err != nil {
				return err
			}
			if tagType == TagEnd {
				break
			}

			iv := reflect.New(v.Type().Elem())
			if err := nbt.DeserializeIntoRaw(tagType, tagName, iv.Elem()); err != nil {
				return fmt.Errorf("failed to decode a part of a compound tag (type %d, name %s): %v", tagType, tagName, err)
			}

			// Save to map
			v.SetMapIndex(reflect.ValueOf(tagName), iv.Elem())
		}
		return nil
	}
}

func (nbt *Deserializer) deserializeList(v reflect.Value) error {
	listInnerType, err := nbt.readUnsignedByte()
	if err != nil {
		return err
	}
	listLen, err := nbt.readInt()
	if err != nil {
		return err
	}
	if listLen < 0 {
		return fmt.Errorf("got negative list length when reading list: %d", listLen)
	}

	// Check for type validity. We want a slice or array of suitable type
	vt := v.Type()

	var buf reflect.Value
	vk := vt.Kind()
	switch vk {
	case reflect.Interface:
		// We cannot determine prior what exact type we want, so we provide a slice of interfaces
		buf = reflect.ValueOf(make([]interface{}, listLen, listLen))
	case reflect.Slice:
		// We provide a slice
		buf = reflect.MakeSlice(v.Type(), int(listLen), int(listLen))
	case reflect.Array:
		if v.Len() < int(listLen) {
			return fmt.Errorf("array is too short: needs %d, got %d", listLen, v.Len())
		}
		buf = v
	default:
		return fmt.Errorf("cannot interpret TagList as %s, needs to be a slice or array", vt.Kind().String())
	}

	for i := 0; i < int(listLen); i++ {
		// Observation: list tag payloads do not have names, empty name is given
		if err := nbt.DeserializeIntoRaw(listInnerType, "", buf.Index(i)); err != nil {
			return err
		}
	}
	if vk != reflect.Array {
		v.Set(buf)
	}
	return nil
}

// checkAcceptableSliceOrArrayType checks if a given slice or array type has one of the acceptable inner types
// Always accepts non-slice or non-array types
func checkAcceptableSliceOrArrayType(v reflect.Value, tagName string, acceptableTypes ...reflect.Kind) error {
	vt := v.Type()
	vk := vt.Kind()

	if vk == reflect.Slice || vk == reflect.Array {
		tk := vt.Elem().Kind()
		for _, t := range acceptableTypes {
			if tk == t {
				return nil
			}
		}

		return fmt.Errorf("cannot interpret %s with slice or array of type %s", tagName, tk.String())
	}

	return nil
}

func (nbt *Deserializer) deserializeLongArray(v reflect.Value) error {
	arrayLen, err := nbt.readInt()
	if err != nil {
		return err
	}
	if arrayLen < 0 {
		return fmt.Errorf("got negative array length when reading long array: %d", arrayLen)
	}

	// Check for type validity. We want a slice or array of appropriate type
	if err := checkAcceptableSliceOrArrayType(v, "TagLongArray", reflect.Int, reflect.Uint, reflect.Int64, reflect.Uint64); err != nil {
		return err
	}

	vt := v.Type()
	vk := vt.Kind()

	var buf reflect.Value
	// Retrieve or generate appropriate value
	switch vk {
	case reflect.Array:
		if v.Len() < int(arrayLen) {
			return fmt.Errorf("array is too short: needs %d, got %d", arrayLen, v.Len())
		}
		buf = v
	case reflect.Interface:
		buf = reflect.ValueOf(make([]int64, arrayLen, arrayLen))
	case reflect.Slice:
		buf = reflect.MakeSlice(v.Type(), int(arrayLen), int(arrayLen))
	default:
		return fmt.Errorf("cannot interpret TagLongArray as %s", vt.Kind().String())
	}

	for i := 0; i < int(arrayLen); i++ {
		arrayVal, err := nbt.readLong()
		if err != nil {
			return err
		}
		buf.Index(i).SetInt(arrayVal)
	}

	if vk != reflect.Array {
		v.Set(buf)
	}

	return nil
}

func (nbt *Deserializer) deserializeIntArray(v reflect.Value) error {
	arrayLen, err := nbt.readInt()
	if err != nil {
		return err
	}
	if arrayLen < 0 {
		return fmt.Errorf("got negative array length when reading int array: %d", arrayLen)
	}

	// Check for type validity. We want a slice or array of appropriate type
	if err := checkAcceptableSliceOrArrayType(v, "TagIntArray", reflect.Int32, reflect.Uint32, reflect.Int64, reflect.Uint64); err != nil {
		return err
	}

	vt := v.Type()
	vk := vt.Kind()

	var buf reflect.Value
	// Retrieve or generate appropriate value
	switch vk {
	case reflect.Array:
		if v.Len() < int(arrayLen) {
			return fmt.Errorf("array is too short: needs %d, got %d", arrayLen, v.Len())
		}
		buf = v
	case reflect.Interface:
		buf = reflect.ValueOf(make([]int32, arrayLen, arrayLen))
	case reflect.Slice:
		buf = reflect.MakeSlice(v.Type(), int(arrayLen), int(arrayLen))
	default:
		return fmt.Errorf("cannot interpret TagIntArray as %s", vt.Kind().String())
	}

	for i := 0; i < int(arrayLen); i++ {
		arrayVal, err := nbt.readInt()
		if err != nil {
			return err
		}
		buf.Index(i).SetInt(int64(arrayVal))
	}

	if vk != reflect.Array {
		v.Set(buf)
	}

	return nil
}

func (nbt *Deserializer) deserializeByteArray(v reflect.Value) error {
	arrayLen, err := nbt.readInt()
	if err != nil {
		return err
	}
	if arrayLen < 0 {
		return fmt.Errorf("got negative array length when reading byte array: %d", arrayLen)
	}

	// Check for type validity. We want a slice or array of appropriate type
	if err := checkAcceptableSliceOrArrayType(v, "TagLongArray", reflect.Int8, reflect.Uint8, reflect.Int16, reflect.Uint16, reflect.Int32, reflect.Uint32, reflect.Int64, reflect.Uint64); err != nil {
		return err
	}

	vt := v.Type()
	vk := vt.Kind()

	var buf reflect.Value
	// Retrieve or generate appropriate value
	switch vk {
	case reflect.Array:
		if v.Len() < int(arrayLen) {
			return fmt.Errorf("array is too short: needs %d, got %d", arrayLen, v.Len())
		}
		buf = v
	case reflect.Interface:
		buf = reflect.ValueOf(make([]int8, arrayLen, arrayLen))
	case reflect.Slice:
		buf = reflect.MakeSlice(v.Type(), int(arrayLen), int(arrayLen))
	default:
		return fmt.Errorf("cannot interpret TagByteArray as %s", vt.Kind().String())
	}

	for i := 0; i < int(arrayLen); i++ {
		arrayVal, err := nbt.readSignedByte()
		if err != nil {
			return err
		}
		buf.Index(i).SetInt(int64(arrayVal))
	}

	if vk != reflect.Array {
		v.Set(buf)
	}

	return nil
}

func (nbt *Deserializer) deserializeRawString(v reflect.Value) error {
	s, err := nbt.readStr()
	if err != nil {
		return err
	}
	switch vk := v.Kind(); vk {
	default:
		return fmt.Errorf("cannot interpret TagString as %s", vk.String())
	case reflect.Bool:
		b, err := strToBool(s)
		if err != nil {
			return err
		}
		v.SetBool(b)
	case reflect.String:
		v.SetString(s)
	case reflect.Interface:
		v.Set(reflect.ValueOf(s))
	}
	return nil
}

func (nbt *Deserializer) deserializeRawDouble(v reflect.Value) error {
	value, err := nbt.readDouble()
	if err != nil {
		return err
	}
	switch vk := v.Kind(); vk {
	default:
		return fmt.Errorf("cannot interpret TagFloat as %s", vk.String())
	case reflect.Float32:
		return fmt.Errorf("refusing to interpret as %s, too narrow datatype", vk.String())
	case reflect.Float64:
		v.SetFloat(value)
	case reflect.Interface:
		v.Set(reflect.ValueOf(value))
	}
	return nil
}

func (nbt *Deserializer) deserializeRawFloat(v reflect.Value) error {
	value, err := nbt.readFloat()
	if err != nil {
		return err
	}
	switch vk := v.Kind(); vk {
	default:
		return fmt.Errorf("cannot interpret TagFloat as %s", vk.String())
	case reflect.Float32, reflect.Float64:
		v.SetFloat(float64(value))
	case reflect.Interface:
		v.Set(reflect.ValueOf(value))
	}
	return nil
}

func (nbt *Deserializer) deserializeRawLong(v reflect.Value) error {
	value, err := nbt.readLong()
	if err != nil {
		return err
	}
	switch vk := v.Kind(); vk {
	default:
		return fmt.Errorf("cannot interpret TagLong as %s", vk.String())
	case reflect.Bool:
		b, err := intToBool(value)
		if err != nil {
			return err
		}
		v.SetBool(b)
	case reflect.Int8, reflect.Uint8, reflect.Int16, reflect.Uint16, reflect.Int32, reflect.Uint32:
		return fmt.Errorf("refusing to interpret as %s, too narrow datatype", vk.String())
	case reflect.Int64:
		v.SetInt(value)
	case reflect.Uint64:
		v.SetUint(uint64(value))
	case reflect.Interface:
		v.Set(reflect.ValueOf(value))
	}
	return nil
}

func (nbt *Deserializer) deserializeRawInt(v reflect.Value) error {
	value, err := nbt.readInt()
	if err != nil {
		return err
	}
	switch vk := v.Kind(); vk {
	default:
		return fmt.Errorf("cannot interpret TagInt as %s", vk.String())
	case reflect.Bool:
		b, err := intToBool(int64(value))
		if err != nil {
			return err
		}
		v.SetBool(b)
	case reflect.Int8, reflect.Uint8, reflect.Int16, reflect.Uint16:
		return fmt.Errorf("refusing to interpret as %s, too narrow datatype", vk.String())
	case reflect.Int32, reflect.Int64:
		v.SetInt(int64(value))
	case reflect.Uint32, reflect.Uint64:
		v.SetUint(uint64(value))
	case reflect.Interface:
		v.Set(reflect.ValueOf(value))
	}
	return nil
}

func (nbt *Deserializer) deserializeRawShort(v reflect.Value) error {
	value, err := nbt.readShort()
	if err != nil {
		return err
	}
	switch vk := v.Kind(); vk {
	default:
		return fmt.Errorf("cannot interpret TagShort as %s", vk.String())
	case reflect.Bool:
		b, err := intToBool(int64(value))
		if err != nil {
			return err
		}
		v.SetBool(b)
	case reflect.Int8, reflect.Uint8:
		return fmt.Errorf("refusing to interpret as %s, too narrow datatype", vk.String())
	case reflect.Int16, reflect.Int32, reflect.Int64:
		v.SetInt(int64(value))
	case reflect.Uint16, reflect.Uint32, reflect.Uint64:
		v.SetUint(uint64(value))
	case reflect.Interface:
		v.Set(reflect.ValueOf(value))
	}
	return nil
}

func (nbt *Deserializer) deserializeRawByte(v reflect.Value) error {
	value, err := nbt.readSignedByte()
	if err != nil {
		return err
	}
	switch vk := v.Kind(); vk {
	default:
		return fmt.Errorf("cannot interpret TagByte as %s", vk.String())
	case reflect.Bool:
		b, err := intToBool(int64(value))
		if err != nil {
			return err
		}
		v.SetBool(b)
	case reflect.Int8, reflect.Int16, reflect.Int32, reflect.Int64:
		v.SetInt(int64(value))
	case reflect.Uint8, reflect.Uint16, reflect.Uint32, reflect.Uint64:
		v.SetUint(uint64(value))
	case reflect.Interface:
		v.Set(reflect.ValueOf(value))
	}
	return nil
}
