package nbt

import (
	"reflect"
)

// indirect walks down v, allocating pointers where needed
// until first non-pointer is encountered, which is then returned
// If decodableAsNull is set, allow returning first pointer which can be set as nil
// If an CustomDeserializer or rawMessageLike is encountered, it is returned instead
// Based on equivalent "encoding/json" functionality
func indirect(v reflect.Value, decodableAsNull bool) (CustomDeserializer, rawMessageLike, reflect.Value) {
	v0 := v
	haveAddr := false

	// If v is a named type and is addressable,
	// start with its address, so that if the type has pointer methods,
	// we find them. This is required for RawMessage, for example
	if v.Kind() != reflect.Ptr && v.Type().Name() != "" && v.CanAddr() {
		haveAddr = true
		v = v.Addr()
	}

	for {
		// Load value from interface, but only if the result will be
		// usefully addressable.
		if v.Kind() == reflect.Interface && !v.IsNil() {
			e := v.Elem()
			// Interface must lead to a pointer and non-nil, and either 1) not decodable as a nil, or leading to something that can be assigned as a nil
			if e.Kind() == reflect.Ptr && !e.IsNil() && (!decodableAsNull || e.Elem().Kind() == reflect.Ptr) {
				haveAddr = false
				v = e
				continue
			}
		}

		// Not a pointer? Break
		if v.Kind() != reflect.Ptr {
			break
		}

		// Can be set as null (value is a pointer) and appropriate to do so?
		if decodableAsNull && v.CanSet() {
			break
		}

		// Prevent infinite loop if v is an interface pointing to its own address:
		if v.Elem().Kind() == reflect.Interface && v.Elem().Elem() == v {
			v = v.Elem()
			break
		}
		// If nil, create a new value
		if v.IsNil() {
			v.Set(reflect.New(v.Type().Elem()))
		}

		// If this is an interface, and a custom deserializer/raw-message-like, return it
		if v.Type().NumMethod() > 0 && v.CanInterface() {
			if u, ok := v.Interface().(rawMessageLike); ok {
				return nil, u, reflect.Value{}
			}
			if u, ok := v.Interface().(CustomDeserializer); ok {
				return u, nil, reflect.Value{}
			}
		}

		if haveAddr {
			v = v0 // restore original value after round-trip Value.Addr().Elem()
			haveAddr = false
		} else {
			v = v.Elem()
		}
	}

	return nil, nil, v
}
