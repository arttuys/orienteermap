package nbt

import (
	"fmt"
	"strings"
)

// strToBool converts a string to a boolean from MC data
func strToBool(s string) (b bool, err error) {
	switch sl := strings.ToLower(s); sl {
	case "true":
		b = true
	case "false":
		b = false
	default:
		err = fmt.Errorf("not a valid boolean: %s", sl)
	}

	return
}

// strToBool converts an integer to a boolean from MC data
func intToBool(i int64) (b bool, err error) {
	switch i {
	case 0:
		b = false
	case 1:
		b = true
	default:
		err = fmt.Errorf("not a valid boolean: %d", i)
	}

	return
}
