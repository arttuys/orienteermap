package nbt

import (
	"bytes"
	"encoding/binary"
	"fmt"
	"io"
)

// readToRawMessage reads given tag data, and saves it to a byte array
func (nbt *Deserializer) readToBytes(tagType byte) (b []byte, err error) {
	var buf bytes.Buffer
	err = nbt.copyRead(&buf, tagType)

	if err == nil {
		b = buf.Bytes()
	}
	return
}

// discardRead reads a given tag, but discards any data. This is much simpler and faster way to walk though - can be used for skipping undesired fields. It is assumed the tag header, if any, has already been read
func (nbt *Deserializer) discardRead(tagType byte) error {
	return nbt.copyRead(io.Discard, tagType)
}

// copyRead reads given tag data, and copies it to the writer. This can be used either as a discarding read, or for saving data for later processing when it is not immediately known what it should be parsed to
func (nbt *Deserializer) copyRead(target io.Writer, tagType byte) error {
	switch tagType {
	default:
		return fmt.Errorf("got unknown tag type %d for discarding read", tagType)
	case TagEnd:
		// No-op, End tags are zero-length
		return nil
	case TagByte:
		_, err := io.CopyN(target, nbt.readerSeeker, 1)
		return err
	case TagShort:
		_, err := io.CopyN(target, nbt.readerSeeker, 2)
		return err
	case TagInt, TagFloat:
		_, err := io.CopyN(target, nbt.readerSeeker, 4)
		return err
	case TagLong, TagDouble:
		_, err := io.CopyN(target, nbt.readerSeeker, 8)
		return err
	case TagByteArray, TagIntArray, TagLongArray:
		readLength32, err := nbt.readInt()
		readLength := int64(readLength32)

		if err != nil {
			return err
		}
		if readLength < 0 {
			err = fmt.Errorf("got negative length %d for discarding read for byte array", readLength)
			return err
		}

		// Emit byte length into the output buffer
		err = binary.Write(target, binary.BigEndian, readLength32)
		if err != nil {
			return err
		}

		// Adjust actual length by type; integers have 4 bytes, longs 8
		if tagType == TagIntArray {
			readLength *= 4
		} else if tagType == TagLongArray {
			readLength *= 8
		}

		_, err = io.CopyN(target, nbt.readerSeeker, readLength)

		return err
	case TagString:
		l, err := nbt.readShort()
		if err != nil {
			return err
		}
		if l < 0 {
			err = fmt.Errorf("got negative length %d for discarding read for string", l)
			return err
		}

		err = binary.Write(target, binary.BigEndian, l)
		if err != nil {
			return err
		}

		_, err = io.CopyN(target, nbt.readerSeeker, int64(l))

		return err
	case TagCompound:
		for {
			tagType, tagName, err := nbt.readTagHeader()
			if err != nil {
				return err
			}
			err = binary.Write(target, binary.BigEndian, tagType)
			if err != nil {
				return err
			}
			if tagType == TagEnd {
				// End found, break
				break
			} else {
				// Write tag name length
				var tagNameLen = int16(len(tagName))
				err = binary.Write(target, binary.BigEndian, tagNameLen)
				if err != nil {
					return err
				}

				// Write actual name. This is not strictly CESU8, but should work as our CESU8 decoder can handle normal UTF-8 strings as well
				err = binary.Write(target, binary.BigEndian, []byte(tagName))
			}
			// Copy-read the contents
			err = nbt.copyRead(target, tagType)
			if err != nil {
				return err
			}
		}
		return nil
	case TagList:
		// Observation: tags in lists have no names, and hence it suffices to only read a single byte
		listTagType, err := nbt.readUnsignedByte()
		if err != nil {
			return err
		}
		err = binary.Write(target, binary.BigEndian, listTagType)
		if err != nil {
			return err
		}

		l, err := nbt.readInt()
		if err != nil {
			return err
		}

		if l < 0 {
			err = fmt.Errorf("got negative length %d for discarding read for list (inner tag type %d)", l, listTagType)
			return err
		}

		err = binary.Write(target, binary.BigEndian, l)
		if err != nil {
			return err
		}

		// Enumerate through subtags, reading through
		for i := int32(0); i < l; i++ {
			if err := nbt.copyRead(target, listTagType); err != nil {
				return err
			}
		}
		return nil
	}
}
