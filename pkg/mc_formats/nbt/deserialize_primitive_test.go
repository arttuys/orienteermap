package nbt

import (
	"reflect"
	"testing"
)

// TestDeserializeRequirePointer tests that deserialization requires a pointer
func TestDeserializeRequirePointer(t *testing.T) {
	data := []byte{TagEnd}

	var b byte

	_, err := Deserialize(data, b)
	if err == nil {
		t.Fatal("expected error when deserializing a non-pointer, but got none")
	}
}

// TestDeserializeByte tests that a single (nameless) signed byte can be appropriately deserialized
func TestDeserializeByte(t *testing.T) {
	data := []byte{TagByte, 0x0, 0x0, 128}
	var b int8
	_, err := Deserialize(data, &b)

	if err != nil {
		t.Fatal(err)
	}

	if b != -128 {
		t.Fatalf("expected %d, got %d", -128, b)
	}
}

// TestDeserializeShort tests that a single (nameless) short can be appropriately deserialized
func TestDeserializeShort(t *testing.T) {
	data := []byte{TagShort, 0x0, 0x0, 1, 2}
	var b int16
	_, err := Deserialize(data, &b)

	if err != nil {
		t.Fatal(err)
	}

	if b != (1<<8)+2 {
		t.Fatalf("expected %d, got %d", (1<<8)+2, b)
	}
}

func TestDeserializeByteArray(t *testing.T) {
	data := []byte{TagByteArray, 0, 0 /*Length prefix*/, 0, 0, 0, 2 /*Integer 1*/, 3 /*Integer 2*/, 9}
	var arr []int8
	var expected = []int8{3, 9}
	_, err := Deserialize(data, &arr)

	if err != nil {
		t.Fatal(err)
	}

	if !reflect.DeepEqual(arr, expected) {
		t.Fatalf("Expected %v, but got %v", expected, arr)
	}
}

func TestDeserializeIntArray(t *testing.T) {
	data := []byte{TagIntArray, 0, 0 /*Length prefix*/, 0, 0, 0, 2 /*Integer 1*/, 0, 0, 0, 1 /*Integer 2*/, 0, 0, 0, 4}
	var arr []int32
	var expected = []int32{1, 4}
	_, err := Deserialize(data, &arr)

	if err != nil {
		t.Fatal(err)
	}

	if !reflect.DeepEqual(arr, expected) {
		t.Fatalf("Expected %v, but got %v", expected, arr)
	}
}

func TestDeserializeLongArray(t *testing.T) {
	data := []byte{TagLongArray, 0, 0 /*Length prefix*/, 0, 0, 0, 2 /*Integer 1*/, 0, 0, 0, 0, 0, 0, 0, 8 /*Integer 2*/, 0, 0, 0, 0, 0, 0, 0, 6}
	var arr []int64
	var expected = []int64{8, 6}
	_, err := Deserialize(data, &arr)

	if err != nil {
		t.Fatal(err)
	}

	if !reflect.DeepEqual(arr, expected) {
		t.Fatalf("Expected %v, but got %v", expected, arr)
	}
}
