package nbt

import (
	"bytes"
	"fmt"
	"io"
	"reflect"
)

func Deserialize(data []byte, v interface{}) (string, error) {
	deserializer, err := New(bytes.NewReader(data))
	if err != nil {
		return "", err
	}

	return deserializer.DeserializeInto(v)
}

// DeserializeInto attempts to deserialize data from the underlying reader into the given interface value. It assumes that the data is a full NBT
// The type of the data must match logically, and in case of ordinary MC files, the appropriate type is most commonly a map or a struct (as the root tag usually is a compound tag)
// This will also seek to the start offset as set during initialization of the deserializer, and will lock/unlock the synchronization mutex
func (nbt *Deserializer) DeserializeInto(v interface{}) (string, error) {
	// Grab reflection data
	val := reflect.ValueOf(v)

	if kind := val.Kind(); kind != reflect.Ptr {
		return "", fmt.Errorf("expected a pointer for NBT deserialization, got %s instead", kind.String())
	}

	// We should now lock the mutex, and defer unlocking until we return
	nbt.ioLock.Lock()
	defer nbt.ioLock.Unlock()

	// Try seeking into the start point
	_, err := nbt.readerSeeker.Seek(nbt.startPosition, io.SeekStart)
	if err != nil {
		return "", err
	}

	tagType, tagName, err := nbt.readTagHeader()
	if err != nil {
		return tagName, err
	}

	// Ready to DeserializeIntoRaw
	err = nbt.DeserializeIntoRaw(tagType, tagName, val)

	if err != nil {
		err = fmt.Errorf("failed to deserialize NBT date due to: %v", err)
	}

	return tagName, err
}

// DeserializeIntoRaw is the low level method for deserialization; it assumes tag type and name where appropriate have already been read or are otherwise known.
// Usually, you will want to use DeserializeInto unless you are sure what you are doing. This will assume that:
//	- Reader is at an appropriate offset to start reading data
//  - IO mutex is locked; this routine may unlock it in certain conditions, but guarantees that it will be relocked on return
func (nbt *Deserializer) DeserializeIntoRaw(tagType byte, tagName string, vR reflect.Value) error {
	curOffset, err := nbt.readerSeeker.Seek(0, io.SeekCurrent)
	if err != nil {
		return err
	}

	custom, rawMessage, v := indirect(vR, tagType == TagEnd)

	if custom != nil || rawMessage != nil {
		var rmIf rawMessageLike
		if rawMessage != nil {
			rmIf = rawMessage
		} else {
			rmIf = &RawMessage{}
		}

		// Initialize raw message
		rmIf.InitRawMessage(nbt.readerSeeker, nbt.ioLock, curOffset, tagName, tagType)

		// If independence is required, indicate as such
		if nbt.rawMessagesAlwaysIndependent {
			nbt.ioLock.Unlock()
			err := rmIf.MakeIndependent()
			if err != nil {
				return err
			}
			nbt.ioLock.Lock()
		}

		// If we have a custom deserializer, call it from us
		if custom != nil {
			nbt.ioLock.Unlock()
			err := custom.DeserializeFromData(rmIf.ToPlainRawMessage())
			nbt.ioLock.Lock()

			if err != nil {
				return err
			}

			// Discard-read - we want to advance to the tag after
			return nbt.discardRead(tagType)
		}

		return nil
	}

	switch tagType {
	case TagEnd:
		if v.Kind() == reflect.Ptr && v.CanSet() {
			v.Set(reflect.Zero(v.Type()))
			return nil
		} else {
			return fmt.Errorf("unexpected TagEnd in a non-pointer object")
		}
	case TagByte:
		return nbt.deserializeRawByte(v)
	case TagShort:
		return nbt.deserializeRawShort(v)
	case TagInt:
		return nbt.deserializeRawInt(v)
	case TagLong:
		return nbt.deserializeRawLong(v)
	case TagFloat:
		return nbt.deserializeRawFloat(v)
	case TagDouble:
		return nbt.deserializeRawDouble(v)
	case TagString:
		return nbt.deserializeRawString(v)
	case TagByteArray:
		return nbt.deserializeByteArray(v)
	case TagIntArray:
		return nbt.deserializeIntArray(v)
	case TagLongArray:
		return nbt.deserializeLongArray(v)
	case TagList:
		return nbt.deserializeList(v)
	case TagCompound:
		return nbt.deserializeCompound(v)
	default:
		return fmt.Errorf("unhandled tag type: %d", tagType)
	}
}
