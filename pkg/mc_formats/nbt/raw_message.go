package nbt

import (
	"bytes"
	"fmt"
	"io"
	"reflect"
	"sync"
)

// MakeIndependent makes a given raw message independent from its backing stream - the input stream can be closed at will
// It is guaranteed that the I/O offset of the underlying reader will be returned to the same position as it were if successful
func (rm *RawMessage) MakeIndependent() error {
	rm.ioLock.Lock()
	defer rm.ioLock.Unlock()

	if rm.savedTagData != nil {
		return nil
	}

	// Save current offset
	curOffset, err := rm.baseIOStream.Seek(0, io.SeekCurrent)
	if err != nil {
		return err
	}

	// Try seeking to the expected start point
	_, err = rm.baseIOStream.Seek(rm.tagPosition, io.SeekStart)
	if err != nil {
		return err
	}

	// Create deserializer
	nbt, err := NewAdvanced(rm.baseIOStream, rm.ioLock, rm.tagPosition, true, false)
	if err != nil {
		return err
	}

	// Read to bytes
	b, err := nbt.readToBytes(rm.tagType)

	if err != nil {
		return err
	}

	rm.savedTagData = b

	// Return to where we were
	_, err = rm.baseIOStream.Seek(curOffset, io.SeekStart)
	if err != nil {
		return err
	}

	return nil
}

// DeserializeInto works like with ordinary NBT deserializers, attempting to parse the given entity into an object
// It is guaranteed that the I/O offset of the underlying reader will be returned to the same position as it were if successful
// Due to the nature of I/O synchronization and current technical limitations, please note that I/O locker will be used even if this is an independent structure
func (rm *RawMessage) DeserializeInto(v interface{}) error {
	val := reflect.ValueOf(v)

	if kind := val.Kind(); kind != reflect.Ptr {
		return fmt.Errorf("expected a pointer for NBT deserialization from raw message, got %s instead", kind.String())
	}

	// Next, check if we have extant data
	var reader deserializerReaderSeeker
	var actualOffset int64
	if rm.savedTagData != nil {
		reader = bytes.NewReader(rm.savedTagData)
	} else {
		reader = rm.baseIOStream
		actualOffset = rm.tagPosition
	}

	rm.ioLock.Lock()
	defer rm.ioLock.Unlock()

	// Save current offset
	curOffset, err := reader.Seek(0, io.SeekCurrent)
	if err != nil {
		return err
	}

	// Try seeking to the expected start point
	_, err = reader.Seek(actualOffset, io.SeekStart)
	if err != nil {
		return err
	}

	// Ready to deserialize. Construct a simulacrum deserializer and proceed with it, as if we had just read data from an actual stream
	nbt, err := NewAdvanced(reader, rm.ioLock, actualOffset, true, rm.savedTagData != nil)
	if err != nil {
		return err
	}

	// Try deserialization
	err = nbt.DeserializeIntoRaw(rm.tagType, rm.tagName, val)
	if err != nil {
		return err
	}

	// Return to where we were
	_, err = reader.Seek(curOffset, io.SeekStart)
	if err != nil {
		return err
	}

	return err
}

func (rm *RawMessage) InitRawMessage(baseIOStream deserializerReaderSeeker, ioLock sync.Locker, tagPosition int64, tagName string, tagType byte) {
	rm.baseIOStream = baseIOStream
	rm.ioLock = ioLock
	rm.tagPosition = tagPosition
	rm.tagName = tagName
	rm.tagType = tagType
}

func (rm *RawMessage) ToPlainRawMessage() *RawMessage {
	// This is essentially an identity function here, but it might not necessarily be in other cases
	return rm
}

func (rm *RawMessage) MarshalText() ([]byte, error) {
	if rm.savedTagData == nil {
		return []byte(fmt.Sprintf("<tag type %d, tag name '%s', data as stream>", rm.tagType, rm.tagName)), nil
	}

	return []byte(fmt.Sprintf("<tag type %d, tag name '%s', data: %v>", rm.tagType, rm.tagName, rm.savedTagData)), nil
}
