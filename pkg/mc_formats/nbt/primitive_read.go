package nbt

import (
	"encoding/binary"
	"fmt"
	"io"
	"orienteermap-go-app/pkg/utils/cesu8"
)

func (nbt *Deserializer) readUnsignedByte() (b uint8, err error) {
	b, err = nbt.readerSeeker.ReadByte()
	return
}

func (nbt *Deserializer) readSignedByte() (b int8, err error) {
	err = binary.Read(nbt.readerSeeker, binary.BigEndian, &b)
	return
}

func (nbt *Deserializer) readShort() (i int16, err error) {
	err = binary.Read(nbt.readerSeeker, binary.BigEndian, &i)
	return
}

func (nbt *Deserializer) readInt() (i int32, err error) {
	err = binary.Read(nbt.readerSeeker, binary.BigEndian, &i)
	return
}

func (nbt *Deserializer) readLong() (l int64, err error) {
	err = binary.Read(nbt.readerSeeker, binary.BigEndian, &l)
	return
}

func (nbt *Deserializer) readFloat() (f float32, err error) {
	err = binary.Read(nbt.readerSeeker, binary.BigEndian, &f)
	return
}

func (nbt *Deserializer) readDouble() (d float64, err error) {
	err = binary.Read(nbt.readerSeeker, binary.BigEndian, &d)
	return
}

// readStr reads a CESU8 string from the input stream
func (nbt *Deserializer) readStr() (str string, err error) {
	l, err := nbt.readShort()
	if err != nil {
		return
	} else if l < 0 {
		err = fmt.Errorf("got negative length (%d) for string", l)
		return
	}

	if l > 0 {
		buf := make([]byte, l)
		_, err = io.ReadFull(nbt.readerSeeker, buf)
		if err != nil {
			return
		}
		str = cesu8.DecodeString(buf)
	}

	return
}

// readTagHeader reads a tag with a type (a byte) and a name (a string)
func (nbt *Deserializer) readTagHeader() (tagType uint8, tagName string, err error) {
	tagType, err = nbt.readUnsignedByte()
	if err != nil {
		return
	}
	if tagType != TagEnd {
		// End tags do not have a name, but others do
		tagName, err = nbt.readStr()
	}

	return
}
