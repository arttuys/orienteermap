package nbt

import (
	"bytes"
	"io"
	"sync"
)

// Tag types valid for NBT
const (
	TagEnd uint8 = iota
	TagByte
	TagShort
	TagInt
	TagLong
	TagFloat
	TagDouble
	TagByteArray
	TagString
	TagList
	TagCompound
	TagIntArray
	TagLongArray
)

// deserializerReaderSeeker indicates what is required from a reader for it to be compatible with NBT tooling
type deserializerReaderSeeker = interface {
	io.ByteReader
	io.ReadSeeker
}

// CustomDeserializer indicates a type that can build an instance of itself when presented with deserializable raw message
type CustomDeserializer interface {
	DeserializeFromData(*RawMessage) error
}

// rawMessageLike indicates a type that can behave like a raw message - it contains necessary position, type, name data and IO interfaces to be able to resume reading from a given point, and can be initialized like/converted to a plain raw message
type rawMessageLike interface {
	// InitRawMessage initializes a raw-message-like object
	InitRawMessage(baseIOStream deserializerReaderSeeker, ioLock sync.Locker, tagPosition int64, tagName string, tagType byte)
	// MakeIndependent ensures that a given raw message doesn't depend on external streams
	MakeIndependent() error
	// ToPlainRawMessage generates a plain simplified raw message from the data stored
	ToPlainRawMessage() *RawMessage
}

// CustomDeserializePostprocess indicates that the type will need additional post-processing after all its elements have been deserialized; this is useful for cases where additional contextual information is needed
type CustomDeserializePostprocess interface {
	DeserializePostprocess() error
}

// RawMessage saves either a position in an existing stream, or independent data for delaying deserialization of data, and allowing slightly more efficient memory use
type RawMessage struct {
	baseIOStream deserializerReaderSeeker
	ioLock       sync.Locker
	tagPosition  int64
	tagName      string
	tagType      byte
	savedTagData []byte
}

// Deserializer is a baseline deserializer, that can deserialize one or more sequential named tags from an input stream
type Deserializer struct {
	readerSeeker  deserializerReaderSeeker
	startPosition int64
	ioLock        sync.Locker
	// rawOnly changes the intrinsic behavior by disallowing ordinary deserialization
	rawOnly                      bool
	rawMessagesAlwaysIndependent bool
}

// New creates a NBT reader with default settings (start at current point, I/O assumed to be exclusive to it, non-raw)
func New(r io.Reader) (*Deserializer, error) {
	return NewAdvanced(r, nil, -1, false, false)
}

// NewAdvanced creates a new NBT reader. Ensure that at the time of the call, the reader is at the position where data starts, and that nothing else is using the stream.
// However, the reader doesn't consider itself the exclusive owner of a stream - it is callers' responsibility to dispose of it when no longer needed
// It is also possible to specify an alternative sync locker (for shared streams), start position, and raw-only (reader is only valid for raw/advanced reads, not plain deserialization)
func NewAdvanced(r io.Reader, sharedLocker sync.Locker, startPosition int64, rawOnly bool, rawMessagesAlwaysIndependent bool) (*Deserializer, error) {
	d := new(Deserializer)

	// Initialize share-locker
	if sharedLocker != nil {
		d.ioLock = sharedLocker
	} else {
		d.ioLock = &sync.Mutex{}
	}

	// If this is compatible already, pass as is - otherwise read till EOF and create a seekable byte buffer
	if br, ok := r.(deserializerReaderSeeker); ok {
		d.readerSeeker = br
	} else {
		b, err := io.ReadAll(r)
		if err != nil {
			return nil, err
		}
		d.readerSeeker = bytes.NewReader(b)
	}

	// Save start offset, if it is under zero - otherwise save what is given
	if startPosition < 0 {
		offset, err := d.readerSeeker.Seek(0, io.SeekCurrent)
		if err != nil {
			return nil, err
		}

		d.startPosition = offset
	} else {
		d.startPosition = startPosition
	}

	d.rawOnly = rawOnly
	d.rawMessagesAlwaysIndependent = rawMessagesAlwaysIndependent
	return d, nil
}
