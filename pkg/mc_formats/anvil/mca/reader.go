package mca

import (
	"compress/gzip"
	"compress/zlib"
	"encoding/binary"
	"fmt"
	"io"
	"orienteermap-go-app/pkg/mc_formats/nbt"
	shared2 "orienteermap-go-app/pkg/mc_formats/shared"
	"orienteermap-go-app/pkg/types/basic"
	"sync"
	"time"
)

type chunkMetadata struct {
	fileOffset  uint64
	sectorCount uint8
	timestamp   time.Time
}

type runnerInternalBookkeeping struct {
	chunkMetadata [1024]*chunkMetadata
}

// Reader implements an MCA reader, reading MCA format files part of a region folder. It can be considered to be a subcomponent of a region folder reader in a way
type Reader struct {
	inner                        io.ReadSeeker
	RegionX                      int32
	RegionZ                      int32
	unfixableError               error
	seekLocker                   sync.Locker
	internalBookkeeping          *runnerInternalBookkeeping
	internalBookkeepingWaitGroup sync.WaitGroup
	rawMessagesIndependent       bool
}

// First-time initialization for a runner
func (r *Reader) doFirstInit() error {
	bookkeeping := runnerInternalBookkeeping{
		chunkMetadata: [1024]*chunkMetadata{},
	}

	// Seek to zero position
	_, err := r.inner.Seek(0, io.SeekStart)
	if err != nil {
		return err
	}

	// Read first data for chunks
	for i := 0; i < 1024; i++ {
		var offsetHigh uint16
		var offsetLow uint8

		err := binary.Read(r.inner, binary.BigEndian, &offsetHigh)
		if err != nil {
			if err == io.EOF && i == 0 {
				// This seems to be an empty file, as first read ended in EOF. Skip to the end without error
				goto endOfInitialization
			}
			return err
		}
		err = binary.Read(r.inner, binary.BigEndian, &offsetLow)
		if err != nil {
			return err
		}
		offset := (uint64(offsetHigh) << 8) + uint64(offsetLow)

		var sectorCount uint8
		err = binary.Read(r.inner, binary.BigEndian, &sectorCount)
		if err != nil {
			return err
		}

		if offset != 0 && sectorCount != 0 {
			bookkeeping.chunkMetadata[i] = &chunkMetadata{
				fileOffset:  offset * 4096,
				sectorCount: sectorCount,
				timestamp:   time.Time{},
			}
		}
	}

	// Read relevant date data too
	for i := 0; i < 1024; i++ {
		chunkDataPointer := bookkeeping.chunkMetadata[i]
		if chunkDataPointer == nil {
			// Skip forward
			_, err := r.inner.Seek(4, io.SeekCurrent)
			if err != nil {
				return err
			}
		} else {
			var epochSeconds uint32
			err := binary.Read(r.inner, binary.BigEndian, &epochSeconds)
			if err != nil {
				return err
			}
			chunkDataPointer.timestamp = time.Unix(int64(epochSeconds), 0)
		}
	}

endOfInitialization:
	r.internalBookkeeping = &bookkeeping

	return nil
}

// NewReader is a shorthand for creating a simple reader that doesn't share streams with anything else
func NewReader(source io.ReadSeeker, regionX int32, regionZ int32) *Reader {
	return NewReaderAdvanced(source, regionX, regionZ, &sync.Mutex{}, false)
}

// NewReaderAdvanced initializes a new reader structure. It is assumed that all given data is valid and the source is readable
// After a reader has been created, assume it will have control of the source stream when sync locker is locked by it.
// It is assumed the MCA data starts at zero offset - use a SectionReader or equivalent if this is not true
func NewReaderAdvanced(source io.ReadSeeker, regionX int32, regionZ int32, syncLocker sync.Locker, rawMessagesIndependent bool) *Reader {
	reader := &Reader{
		inner:                        source,
		RegionX:                      regionX,
		RegionZ:                      regionZ,
		internalBookkeepingWaitGroup: sync.WaitGroup{},
		seekLocker:                   syncLocker,
		rawMessagesIndependent:       rawMessagesIndependent,
	}

	// Mark as pending startup
	reader.internalBookkeepingWaitGroup.Add(1)

	// Start initial setup process
	go func() {
		reader.seekLocker.Lock()

		defer reader.seekLocker.Unlock()
		defer reader.internalBookkeepingWaitGroup.Done()
		reader.unfixableError = reader.doFirstInit()
	}()

	return reader
}

// GetChunkModifiedTime returns when a chunk has been modified
func (r *Reader) GetChunkModifiedTime(chunk [2]int32) (time.Time, bool, error) {
	r.internalBookkeepingWaitGroup.Wait()

	if r.unfixableError != nil {
		return time.Time{}, false, r.unfixableError
	}

	chunkLocation := shared2.ChunkCoordinatesToRegionFileLocation(chunk[0], chunk[1])

	if chunkLocation >= 1024 {
		return time.Time{}, false, fmt.Errorf("chunk location %d (derived from %d,%d) is not a valid location", chunkLocation, chunk[0], chunk[1])
	}

	// Check what we have
	chunkDataPointer := r.internalBookkeeping.chunkMetadata[chunkLocation]
	if chunkDataPointer == nil {
		// No data, return as such
		return time.Time{}, false, nil
	}

	return chunkDataPointer.timestamp, true, nil
}

// readChunkInner reads a chunk from the given location if available
func (r *Reader) readChunkInner(requestChunkX int32, requestChunkZ int32, deserializeInto interface{}) (bool, error, bool) {
	chunkLocation := shared2.ChunkCoordinatesToRegionFileLocation(requestChunkX, requestChunkZ)

	if chunkLocation >= 1024 {
		return false, fmt.Errorf("chunk location %d (derived from %d,%d) is not a valid location (valid range 0-1023)", chunkLocation, requestChunkX, requestChunkZ), false
	}

	// Check what we have
	chunkDataPointer := r.internalBookkeeping.chunkMetadata[chunkLocation]
	if chunkDataPointer == nil {
		// No data, return as such
		return false, nil, false
	}

	// Try seeking to the offset
	_, err := r.inner.Seek(int64(chunkDataPointer.fileOffset), io.SeekStart)
	if err != nil {
		// This is probably unfixable.. this should not fail
		return false, err, true
	}

	// Read length, compression data
	var length uint32
	err = binary.Read(r.inner, binary.BigEndian, &length)
	if err != nil {
		// Also should be always possible
		return false, err, true
	}

	var compressionType byte
	err = binary.Read(r.inner, binary.BigEndian, &compressionType)
	if err != nil {
		// Also should be always possible
		return false, err, true
	}

	// Adjust for data compression indication
	length -= 1

	var baseReader io.Reader = &io.LimitedReader{
		R: r.inner,
		N: int64(length),
	}

	if compressionType == 1 {
		baseReader, err = gzip.NewReader(baseReader)
		if err != nil {
			// This could be localized corruption, do not treat as unfixable
			return false, err, false
		}
	} else if compressionType == 2 {
		baseReader, err = zlib.NewReader(baseReader)
		if err != nil {
			// This could be localized corruption, do not treat as unfixable
			return false, err, false
		}
	}

	nbtReader, err := nbt.NewAdvanced(baseReader, &sync.Mutex{}, -1, false, r.rawMessagesIndependent)

	if err != nil {
		return false, err, false
	}

	// Call the conversion function
	_, err = nbtReader.DeserializeInto(&deserializeInto)

	if err != nil {
		return false, err, false
	}

	return true, err, false
}

func (r *Reader) ReadData(chunkX int32, chunkZ int32, deserializeInto interface{}) (bool, error) {
	r.internalBookkeepingWaitGroup.Wait()

	if r.unfixableError != nil {
		return false, r.unfixableError
	}

	regionX := shared2.ChunkToRegionCoord(chunkX)
	regionZ := shared2.ChunkToRegionCoord(chunkZ)

	if !(regionX == r.RegionX && regionZ == r.RegionZ) {
		// Quietly discard if expected region doesn't match
		return false, nil
	}

	// Lock synchronization for seeking
	r.seekLocker.Lock()
	defer r.seekLocker.Unlock()

	found, err, unfixable := r.readChunkInner(chunkX, chunkZ, deserializeInto)

	if err != nil {
		if unfixable {
			r.unfixableError = err
		}
		return false, err
	}

	return found, nil
}

// GetListOfChunkCoordinatesFound returns a list of chunk coordinates that can be retrieved from this reader
// This may take a longer amount of time on first call, as the necessary data structures are initialized
// on an adjunct goroutine - this function will wait until it is complete
func (r *Reader) GetListOfChunkCoordinatesFound() ([]basic.CoordinatePair, error) {
	r.internalBookkeepingWaitGroup.Wait()

	// Require that we have no unfixable errors
	if r.unfixableError != nil {
		return nil, r.unfixableError
	}

	// Knowing: int regionX = (int)floor(chunkX / 32.0);
	//   		int regionZ = (int)floor(chunkZ / 32.0);
	// We can calculate the base offset by region coordinate * 32

	baseChunkX := r.RegionX * 32
	baseChunkZ := r.RegionZ * 32

	slice := make([]basic.CoordinatePair, 0)

	for dz := 0; dz < 32; dz++ {
		for dx := 0; dx < 32; dx++ {
			// Get relevant offset
			x := baseChunkX + int32(dx)
			z := baseChunkZ + int32(dz)
			offset := shared2.ChunkCoordinatesToRegionFileLocation(x, z)

			if r.internalBookkeeping.chunkMetadata[offset] != nil {
				slice = append(slice, [2]int32{x, z})
			}
		}
	}

	return slice, nil
}
