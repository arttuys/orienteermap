package region

import (
	"strconv"
	"strings"
)

// ParseRegionFileName takes a file name, and checks if it seems to be a valid region file. If yes, then data is extracted indicating what region it corresponds to
func ParseRegionFileName(name string) (regionX int32, regionZ int32, isValid bool) {
	isValid = false

	splitStr := strings.Split(name, ".")

	if len(splitStr) != 4 || splitStr[0] != "r" || splitStr[3] != "mca" {
		// Not acceptable, not a valid region file name
		return
	}

	if i, err := strconv.ParseInt(splitStr[1], 10, 32); err != nil {
		return
	} else {
		regionX = int32(i)
	}

	if i, err := strconv.ParseInt(splitStr[2], 10, 32); err != nil {
		return
	} else {
		regionZ = int32(i)
	}

	isValid = true

	return
}
