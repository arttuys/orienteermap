package region

import (
	"compress/gzip"
	"errors"
	"fmt"
	"io/ioutil"
	"orienteermap-go-app/pkg/mc_formats/anvil/mca"
	"orienteermap-go-app/pkg/mc_formats/nbt"
	"orienteermap-go-app/pkg/mc_formats/shared"
	"orienteermap-go-app/pkg/types/basic"
	"orienteermap-go-app/pkg/types/interfaces"
	"os"
	"path/filepath"
	"runtime"
	"sync"
	"time"
)

// MaxRegionsOpenLimit indicates how many region files can be open at once.
// Considering a typical use case would not necessarily revisit a large set of regions, it is worthwhile to keep this value relatively small per reader
const MaxRegionsOpenLimit = 8

type taskResult struct {
	chunk basic.CoordinatePair
	value *shared.ChunkBundle
	err   error
}

type channelTask struct {
	chunks     []basic.CoordinatePair
	strict     bool
	outChannel chan *taskResult
}

type regionReaderEl struct {
	sourceStream *os.File
	reader       *mca.Reader
}

type regionReaderTripletEl struct {
	region   basic.CoordinatePair
	chunk    *regionReaderEl
	entities *regionReaderEl
	poi      *regionReaderEl
}

// tryOpenRegionReaderFromFile tries to open a reader from file. If file does not exist, nil is returned
func tryOpenRegionReaderFromFile(filename string, regionX int32, regionZ int32, strict bool) (*regionReaderEl, error) {
	var err error

	regionFile, err := os.Open(filename)
	if err != nil {
		if !strict && os.IsNotExist(err) {
			return nil, nil
		}

		return nil, err
	}

	// Open a reader. Since we may close readers at any time, we should ensure raw messages remain independent
	reader := mca.NewReaderAdvanced(regionFile, regionX, regionZ, &sync.Mutex{}, true)

	return &regionReaderEl{
		sourceStream: regionFile,
		reader:       reader,
	}, nil
}

// Reader implements a region folder reader, reading Minecraft data directories
type Reader struct {
	// rootDirectory declares where the level.dat should be found
	rootDirectory string
	// chunkDirectory indicates where relevant chunk MCA files should be found (usually a subdirectory of rootDirectory)
	chunkDirectory string
	// entitiesDirectory indicates where entity MCA files should be found
	entitiesDirectory string
	// poiDirectory indicates where POI MCA files should be found
	poiDirectory string
	// initialInitializationWaitGroup is a wait group for holding up any calls that happen before the reader has wholly initialized itself
	initialInitializationWaitGroup sync.WaitGroup
	unfixableError                 error

	// Has this provider closed already for tasks?
	closed bool

	// Preloaded level data
	levelData *shared.Level

	// List of available chunks and a map for modification time
	chunkList                []basic.CoordinatePair
	chunkModificationTimeMap map[basic.CoordinatePair]time.Time

	// Task ingress channel and group
	taskIngressChannelRWGroup sync.RWMutex
	taskIngressChannel        chan channelTask
}

func (reader *Reader) ChunkExists(pair basic.CoordinatePair) (bool, error) {
	reader.initialInitializationWaitGroup.Wait()

	if reader.unfixableError != nil {
		return false, reader.unfixableError
	}

	// If the chunk can be found from the modification time map, it exists
	_, found := reader.chunkModificationTimeMap[pair]

	return found, nil
}

func (reader *Reader) GetChunkModificationTimes(pairs []basic.CoordinatePair, strict bool) (map[basic.CoordinatePair]time.Time, error) {
	reader.initialInitializationWaitGroup.Wait()

	if reader.unfixableError != nil {
		return nil, reader.unfixableError
	}

	dataMap := make(map[basic.CoordinatePair]time.Time)

	for _, chunk := range pairs {
		t, found := reader.chunkModificationTimeMap[chunk]
		if !found {
			if !strict {
				continue
			} else {
				return nil, fmt.Errorf("did not find chunk %v, when looking up modification time", chunk)
			}
		}
		dataMap[chunk] = t
	}

	return dataMap, nil
}

func (reader *Reader) GetLevel() (*shared.Level, error) {
	reader.initialInitializationWaitGroup.Wait()

	if reader.unfixableError != nil {
		return nil, reader.unfixableError
	}

	return reader.levelData, nil
}

func (reader *Reader) GetChunks(pairs []basic.CoordinatePair, f func(basic.CoordinatePair, *shared.ChunkBundle), strict bool) error {
	reader.initialInitializationWaitGroup.Wait()

	if reader.unfixableError != nil {
		return reader.unfixableError
	}

	// No data, fast-track bypass
	if len(pairs) == 0 {
		return nil
	}

	filteredPairs := pairs
	if strict {
		for _, chunk := range pairs {
			// Verify this chunk exists by checking that the modification time is defined, and fail if not
			if _, ok := reader.chunkModificationTimeMap[chunk]; !ok {
				return fmt.Errorf("chunk %v does not exist - encountered on attempt to retrieve", chunk)
			}
		}
	} else {
		filteredPairs = make([]basic.CoordinatePair, 0)
		for _, chunk := range pairs {
			// Verify this chunk exists by checking that the modification time is defined, and add it defined
			if _, ok := reader.chunkModificationTimeMap[chunk]; ok {
				filteredPairs = append(filteredPairs, chunk)
			}
		}
	}

	reader.taskIngressChannelRWGroup.RLock()

	if reader.closed {
		reader.taskIngressChannelRWGroup.RUnlock()
		return errors.New("this provider has closed already")
	}

	// Generate a task.
	// Allow output channel to buffer all of the expected results - this will lead to more memory expense, but will also
	// ensure that this won't jam the entire reader if the processing function halts or slows down for some reason
	task := channelTask{
		chunks:     filteredPairs,
		outChannel: make(chan *taskResult, len(filteredPairs)),
		strict:     strict,
	}

	reader.taskIngressChannel <- task
	reader.taskIngressChannelRWGroup.RUnlock()

	// Start collecting output data
	collectedErrors := make([]error, 0, 0)

	for {
		output, ok := <-task.outChannel
		if !ok {
			break
		}

		if output.err != nil {
			collectedErrors = append(collectedErrors, output.err)
		} else {
			f(output.chunk, output.value)
		}
	}

	// And return errors if any
	if len(collectedErrors) > 0 {
		return fmt.Errorf("received errors: %v", collectedErrors)
	} else {
		return nil
	}
}

func (reader *Reader) GetBackchannel() chan interfaces.BackchannelMessage {
	// Not supported for this reader
	return nil
}

func (reader *Reader) Close() error {
	reader.initialInitializationWaitGroup.Wait()

	reader.taskIngressChannelRWGroup.Lock()
	// Close ingress channel, and mark provider as closed
	close(reader.taskIngressChannel)
	reader.closed = true
	reader.taskIngressChannelRWGroup.Unlock()

	return nil
}

func (reader *Reader) GetListOfChunks() ([]basic.CoordinatePair, error) {
	reader.initialInitializationWaitGroup.Wait()

	if reader.unfixableError != nil {
		return nil, reader.unfixableError
	}

	var copyRes = make([]basic.CoordinatePair, len(reader.chunkList))
	copy(copyRes, reader.chunkList)

	return copyRes, nil
}

func (reader *Reader) prepareRegionMetadata() error {
	fileLst, err := ioutil.ReadDir(reader.chunkDirectory)

	if err != nil {
		return err
	}

	// Assume 80% fill rate per file, 0.8 * 32 * 32
	reader.chunkList = make([]basic.CoordinatePair, 0, len(fileLst)*820)
	reader.chunkModificationTimeMap = make(map[basic.CoordinatePair]time.Time)

	var errorsEncountered []error
	var processingWaitGroup sync.WaitGroup
	var sliceWritingMutex sync.Mutex
	var errorWritingMutex sync.Mutex

	for _, file := range fileLst {
		if file.IsDir() {
			continue
		}
		regionX, regionZ, valid := ParseRegionFileName(file.Name())
		if !valid {
			continue
		}

		// Start a goroutine for parsing
		processingWaitGroup.Add(1)

		// Try to run parallelized, process. Create an intermediate variable to ensure no side effects result
		file := file
		go func() {
			// Unlock when done
			defer processingWaitGroup.Done()

			// Open file
			fileReader, err := os.Open(filepath.Join(reader.chunkDirectory, file.Name()))
			if err != nil {
				// Consider this a critical error - all files should be readable at the start!
				errorWritingMutex.Lock()
				defer errorWritingMutex.Unlock()
				errorsEncountered = append(errorsEncountered, err)

				return
			}

			regionReader := mca.NewReader(fileReader, regionX, regionZ)
			regionChunks, err := regionReader.GetListOfChunkCoordinatesFound()
			if err != nil {
				errorWritingMutex.Lock()
				defer errorWritingMutex.Unlock()
				errorsEncountered = append(errorsEncountered, err)

				return
			}

			sliceWritingMutex.Lock()
			reader.chunkList = append(reader.chunkList, regionChunks...)

			for _, chunk := range regionChunks {
				modfTime, _, err := regionReader.GetChunkModifiedTime(chunk)
				if err != nil {
					errorWritingMutex.Lock()

					errorsEncountered = append(errorsEncountered, err)
					errorWritingMutex.Unlock()
					sliceWritingMutex.Unlock()
					return
				}

				reader.chunkModificationTimeMap[chunk] = modfTime
			}

			sliceWritingMutex.Unlock()

			if err = fileReader.Close(); err != nil {
				errorWritingMutex.Lock()
				defer errorWritingMutex.Unlock()
				errorsEncountered = append(errorsEncountered, err)

				return
			}
		}()

	}

	// Wait until everything has finished up
	processingWaitGroup.Wait()

	if len(errorsEncountered) > 0 {
		return fmt.Errorf("something went wrong with processing, collected errors: %v", errorsEncountered)
	}

	return nil
}

func (reader *Reader) doFirstTimeInitialization() error {
	err := reader.prepareLevelDat()
	if err != nil {
		return err
	}

	err = reader.prepareRegionMetadata()
	if err != nil {
		return err
	}

	// Initialize task ingress channel, with 3 queued events at max
	reader.taskIngressChannel = make(chan channelTask, runtime.GOMAXPROCS(0)*3)

	return nil
}

func (reader *Reader) prepareLevelDat() error {
	// Try opening the level.dat file
	rawLevelDatReader, err := os.OpenFile(filepath.Join(reader.rootDirectory, "level.dat"), os.O_RDONLY, os.ModePerm)
	if err != nil {
		return err
	}

	levelDatReader, err := gzip.NewReader(rawLevelDatReader)
	if err != nil {
		return err
	}

	// Prepare level data
	var level shared.Level
	deserializer, err := nbt.New(levelDatReader)
	if err != nil {
		return err
	}

	if _, err = deserializer.DeserializeInto(&level); err != nil {
		return err
	}

	reader.levelData = &level
	return nil
}

func (reader *Reader) taskRunner() {
	reader.unfixableError = reader.doFirstTimeInitialization()

	reader.initialInitializationWaitGroup.Done()

	// Initialize list for region readers open
	regionReaderLst := make([]regionReaderTripletEl, 0)

	for {
		// Initially at the start of the cycle, do close any regions that are open above the max limit we allow
		if l := len(regionReaderLst); l > MaxRegionsOpenLimit {
			excess := l - MaxRegionsOpenLimit

			// Select the excess to remove
			removable := regionReaderLst[0:excess]
			regionReaderLst = regionReaderLst[excess:]

			var closeCompleteWaitGroup sync.WaitGroup
			errorHolderChannel := make(chan error, len(removable)*3)

			for _, elTriplet := range removable {
				elTriplet := elTriplet
				closeCompleteWaitGroup.Add(3)
				go func() {
					for _, el := range []*regionReaderEl{elTriplet.chunk, elTriplet.poi, elTriplet.entities} {
						if el != nil {
							err := el.sourceStream.Close()
							if err != nil {
								errorHolderChannel <- err
							}
						}
						closeCompleteWaitGroup.Done()
					}
				}()
			}

			// Wait until all complete
			closeCompleteWaitGroup.Wait()

			// Close error channel, as we know all senders have stopped
			close(errorHolderChannel)

			// Log all errors, but do not stop the main loop
			for {
				err, ok := <-errorHolderChannel
				if !ok {
					break
				}

				if err != nil {
					// We can't do much if an error appears here, just ignore it
					_, _ = fmt.Fprintf(os.Stderr, "warning: failed to close region reader at trimming stage: %v\n", err)
				}
			}
		}

		// Grab next task from the ingress channel
		task, ok := <-reader.taskIngressChannel

		if !ok {
			// End of tasks. Go to shutdown phase
			goto doShutdown
		}

		// Proceed by collecting region coordinates needed for chunks
		regionMap := make(map[basic.CoordinatePair][]basic.CoordinatePair)
		for _, chunk := range task.chunks {
			regionX := shared.ChunkToRegionCoord(chunk[0])
			regionZ := shared.ChunkToRegionCoord(chunk[1])

			regionCoord := basic.CoordinatePair{regionX, regionZ}
			regionMap[regionCoord] = append(regionMap[regionCoord], chunk)
		}

		// Prepare a similar structure for readers against regions
		regionReaderMap := make(map[basic.CoordinatePair]regionReaderTripletEl)

		// Import all existing readers
		for _, el := range regionReaderLst {
			regionReaderMap[el.region] = el
		}

		// Create a wait group for waiting all pending reads to conclude
		var pendingReadWaitGroup sync.WaitGroup

		for region, chunks := range regionMap {
			// Disjoint from loop variables
			region := region
			chunks := chunks

			contextualRegionReaderTriplet, found := regionReaderMap[region]
			if !found {
				var regionFilePath, entityFilePath, poiFilePath string
				var regionReader, entityReader, poiReader *regionReaderEl
				var err error

				// Try opening a mainstay chunk reader
				regionFilePath = filepath.Join(reader.chunkDirectory, fmt.Sprintf("r.%d.%d.mca", region[0], region[1]))
				regionReader, err = tryOpenRegionReaderFromFile(regionFilePath, region[0], region[1], true)
				if err != nil {
					goto prepDoneOrFailed
				}

				// Try also entities, POI
				entityFilePath = filepath.Join(reader.entitiesDirectory, fmt.Sprintf("r.%d.%d.mca", region[0], region[1]))
				entityReader, err = tryOpenRegionReaderFromFile(entityFilePath, region[0], region[1], false)
				if err != nil {
					goto prepDoneOrFailed
				}

				poiFilePath = filepath.Join(reader.poiDirectory, fmt.Sprintf("r.%d.%d.mca", region[0], region[1]))
				poiReader, err = tryOpenRegionReaderFromFile(poiFilePath, region[0], region[1], false)
				if err != nil {
					goto prepDoneOrFailed
				}

				contextualRegionReaderTriplet = regionReaderTripletEl{
					region:   region,
					chunk:    regionReader,
					entities: entityReader,
					poi:      poiReader,
				}

				regionReaderMap[region] = contextualRegionReaderTriplet
				regionReaderLst = append(regionReaderLst, contextualRegionReaderTriplet)

			prepDoneOrFailed:
				if err != nil {
					// Flush an error for all chunks affected
					for _, chunk := range chunks {
						task.outChannel <- &taskResult{
							chunk: chunk,
							value: nil,
							err:   err,
						}
					}

					// Continue on to next region pair
					continue
				}
			}

			// We now have a reader. Proceed to complete chunk requests
			for _, chunk := range chunks {
				chunk := chunk

				pendingReadWaitGroup.Add(1)

				go func() {
					defer pendingReadWaitGroup.Done()

					var chunkStruct *shared.Chunk
					var entityStruct *shared.Entities
					var poiStruct *shared.PointsOfInterest

					found, err := contextualRegionReaderTriplet.chunk.reader.ReadData(chunk[0], chunk[1], &chunkStruct)

					if !found && err == nil {
						err = fmt.Errorf("received no primary chunk where expected: %d,%d", chunk[0], chunk[1])

					}
					// If primary chunk reading failed hop directly to output step
					if err != nil {
						goto sendOutput
					}

					// Try reading auxiliary structures
					if contextualRegionReaderTriplet.entities != nil {
						found, err = contextualRegionReaderTriplet.entities.reader.ReadData(chunk[0], chunk[1], &entityStruct)
						if err != nil {
							goto sendOutput
						}
					}

					if contextualRegionReaderTriplet.poi != nil {
						found, err = contextualRegionReaderTriplet.poi.reader.ReadData(chunk[0], chunk[1], &poiStruct)
						if err != nil {
							goto sendOutput
						}
					}

				sendOutput:
					task.outChannel <- &taskResult{
						chunk: chunk,
						value: &shared.ChunkBundle{
							Chunk:    chunkStruct,
							Entities: entityStruct,
							POI:      poiStruct,
						},
						err: err,
					}

				}()
			}
		}

		// All requests have been transmitted, wait till each and every of them has completed
		pendingReadWaitGroup.Wait()

		// Close the output channel to indicate we have no more data to send
		close(task.outChannel)
	}

doShutdown:
	// Close remaining readers, and quit the task runner
	reader.doShutdown(regionReaderLst)
}

func (reader *Reader) doShutdown(regionReaderLst []regionReaderTripletEl) {
	var closeCompleteWaitGroup sync.WaitGroup
	errorHolderChannel := make(chan error, len(regionReaderLst)*3)

	for _, elTriplet := range regionReaderLst {
		elTriplet := elTriplet
		closeCompleteWaitGroup.Add(3)
		go func() {
			for _, el := range []*regionReaderEl{elTriplet.chunk, elTriplet.poi, elTriplet.entities} {
				if el != nil {
					err := el.sourceStream.Close()
					if err != nil {
						errorHolderChannel <- err
					}
				}
				closeCompleteWaitGroup.Done()
			}
		}()
	}

	// Wait until all complete
	closeCompleteWaitGroup.Wait()

	// Close error channel, as we know all senders have stopped
	close(errorHolderChannel)

	// Log all errors
	for {
		err, ok := <-errorHolderChannel
		if !ok {
			break
		}

		if err != nil {
			// We can't do much if an error appears here, just ignore it
			_, _ = fmt.Fprintf(os.Stderr, "warning: failed to close region reader at closing of world provider: %v\n", err)
		}
	}
}

func NewReader(rootDirectory string, dimensionSubfolder string) (interfaces.WorldSource, error) {
	// Construct root directory
	absRootDir, err := filepath.Abs(rootDirectory)
	if err != nil {
		return nil, err
	}

	stat, err := os.Stat(absRootDir)
	if err != nil {
		return nil, err
	}

	if mode := stat.Mode(); !mode.IsDir() {
		return nil, fmt.Errorf("given path \"%s\" is not a directory", absRootDir)
	}

	// Chunk directory
	absChunkDir := filepath.Join(absRootDir, dimensionSubfolder, "region")

	stat, err = os.Stat(absChunkDir)
	if err != nil {
		return nil, err
	}

	if mode := stat.Mode(); !mode.IsDir() {
		return nil, fmt.Errorf("given path \"%s\" is not a directory", absRootDir)
	}

	// Entity directory and POI directory (where available)
	absEntityDir := filepath.Join(absRootDir, dimensionSubfolder, "entities")
	absPoiDir := filepath.Join(absRootDir, dimensionSubfolder, "poi")

	// Start initialization
	provider := &Reader{
		rootDirectory:     absRootDir,
		chunkDirectory:    absChunkDir,
		entitiesDirectory: absEntityDir,
		poiDirectory:      absPoiDir,
	}
	provider.initialInitializationWaitGroup.Add(1)

	go provider.taskRunner()

	return provider, nil
}
