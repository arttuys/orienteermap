package region

import (
	"math/rand"
	"orienteermap-go-app/pkg/mc_formats/shared"
	"orienteermap-go-app/pkg/types/basic"
	"os"
	"path/filepath"
	"testing"
)

// TestSimpleProviderOps does some basic testing, checking if chunks can be read, modification dates inspected, and returned chunks seem to be accurate by their information
func TestSimpleProviderOps(t *testing.T) {
	basePath, err := os.Getwd()
	if err != nil {
		t.Fatal(err)
	}

	t.Logf("Base path: %s", basePath)

	path := filepath.Join(basePath, "..", "..", "..", "..", "test", "datasets", "world")

	// Try to open a world
	world, err := NewReader(path, "")
	if err != nil {
		t.Fatal(err)
	}

	// Baseline test, test existence of a known-non-existent chunk and acceptability of level data
	exists, err := world.ChunkExists(basic.CoordinatePair{0, 0})
	if exists || err != nil {
		t.Errorf("expected chunk to not exist, but it did or received an error: %v", err)
		return
	}

	level, err := world.GetLevel()
	if err != nil || level.Data.DataVersion != 2730 {
		t.Errorf("received error or level data version was not 2730: %v", err)
		return
	}

	// Get list of chunks

	chunksOrig, err := world.GetListOfChunks()
	if err != nil {
		t.Fatal(err)
	}

	if l := len(chunksOrig); l != 5487 {
		t.Fatalf("incorrect length, expected 4981 but got %d", l)
	}

	t.Logf("Total count of chunks: %d\n", len(chunksOrig))

	// Shuffle the chunk list to make inquiries a bit more interesting, forcing multiple-reader scenarios
	var chunks []basic.CoordinatePair
	chunks = append(chunks, chunksOrig...)

	rand.Shuffle(len(chunks), func(i, j int) {
		chunks[i], chunks[j] = chunks[j], chunks[i]
	})

	// Count the max/min of chunk locations
	var xLarge, zLarge int32 = 0, 0
	var xSmall, zSmall int32 = 0, 0

	// Establish queue for doing batch requests
	var chunkQueue = make([]basic.CoordinatePair, 0)
	var foundEntities, foundPOIs bool

	// Test that strict/non-strict queries work properly
	// Non-strict
	err = world.GetChunks([]basic.CoordinatePair{[2]int32{999999, 999999}}, func(pair basic.CoordinatePair, bundle *shared.ChunkBundle) {

	}, false)
	if err != nil {
		t.Fatal("did receive an error where not expected: ", err)
	}
	// Strict
	err = world.GetChunks([]basic.CoordinatePair{[2]int32{999999, 999999}}, func(pair basic.CoordinatePair, bundle *shared.ChunkBundle) {

	}, true)
	if err == nil {
		t.Fatal("did not receive an error where expected: ")
	}

	for _, chunk := range chunks {
		x := chunk[0]
		z := chunk[1]

		if x > xLarge {
			xLarge = x
		}
		if x < xSmall {
			xSmall = x
		}

		if z > zLarge {
			zLarge = z
		}
		if z < zSmall {
			zSmall = z
		}

		// Test existence
		exists, err := world.ChunkExists(chunk)
		if !exists || err != nil {
			t.Errorf("expected chunk to exist, but did not or received an error: %v", err)
			return
		}

		chunkQueue = append(chunkQueue, chunk)

		// Skip if we're at less than 3
		if len(chunkQueue) < 3 {
			continue
		}

		// OK, we have sufficient data
		chunkModTime, err := world.GetChunkModificationTimes(chunkQueue, true)
		if err != nil {
			t.Fatal(err)
		}

		// Check that length was correct
		if len(chunkModTime) != len(chunkQueue) {
			t.Fatalf("mismatching return lengths for modification times: expected %d, got %d", len(chunkQueue), len(chunkModTime))
		}

		t.Logf("Chunk %v modified at: %v \n", chunkQueue, chunkModTime)

		// Next, get requested chunks and check their data matches as expected
		err = world.GetChunks(chunkQueue, func(pair basic.CoordinatePair, chunk *shared.ChunkBundle) {
			if chunk.Chunk.Level.XPos != pair[0] || chunk.Chunk.Level.ZPos != pair[1] {
				t.Errorf("unexpected chunk position: %v vs %v", chunk.Chunk.Level, pair)
			}

			// There should at least be one chunk with sheep, and one with a bee nest
			if chunk.Entities != nil {
				for _, v := range chunk.Entities.Entities {
					if v == "minecraft:sheep" {
						foundEntities = true
					}
				}
			}

			if chunk.POI != nil {
				for _, v := range chunk.POI.Data.POIs {
					if v == "minecraft:bee_nest" {
						foundPOIs = true
					}
				}
			}
		}, true)
		if err != nil {
			t.Fatal(err)
		}

		chunkQueue = nil

	}

	if !foundEntities {
		t.Fatal("did not find a sheep entity from chunks!")
	}

	if !foundPOIs {
		t.Fatal("did not find a bee nest from chunks!")
	}

	t.Logf("X range: [%d,%d], Z range: [%d, %d]", xSmall, xLarge, zSmall, zLarge)

	if err = world.Close(); err != nil {
		t.Fatal(err)
	}
}
