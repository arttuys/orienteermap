package abstract2svg

type RenderOpts struct {
	OmitDangers bool `mapstructure:"omit_dangers"`
}
