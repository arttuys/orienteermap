package abstract2svg

import (
	"github.com/rs/zerolog/log"
	"orienteermap-go-app/pkg/core/abstract2svg/svg"
	"orienteermap-go-app/pkg/types/basic"
	"orienteermap-go-app/pkg/types/map_abstract"
	"orienteermap-go-app/pkg/types/map_global_meta"
)

var contextualLogger = log.With().Str("module", "abstract2svg").Logger()

func RenderSVGFromAbstract(chunk basic.CoordinatePair, abstract *map_abstract.MapAbstract, globalMetadata *map_global_meta.MapGlobalMetadata) ([]byte, error) {
	contextualLogger.Info().Interface("chunk", chunk).Msg("starting rendering process for SVG")
	contextualLogger.Trace().Interface("chunk", chunk).Interface("abstract", abstract).Interface("meta", globalMetadata).Msg("tracing data being rendered")

	// First, render terrain background.
	bg, err := abstractTerrainToPNGBackground(&abstract.BaseTerrain)
	if err != nil {
		contextualLogger.Error().Err(err).Msg("failed to render PNG background")
		return nil, err
	}

	// Construct a map representation
	mapRenderStruct := svg.MapSVG{BackgroundImage: bg}

	svgBytes, err := mapRenderStruct.ProduceSVG()
	if err != nil {
		contextualLogger.Error().Err(err).Msg("failed to render SVG")
		return nil, err
	}

	return svgBytes, nil
}
