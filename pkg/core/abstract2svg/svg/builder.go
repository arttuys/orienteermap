package svg

import (
	"bytes"
	"encoding/base64"
	"encoding/xml"
	"fmt"
)

type MapSVG struct {
	BackgroundImage []byte
}

func (M *MapSVG) EmitBackgroundImageTags(encoder *xml.Encoder) error {
	b64image := base64.StdEncoding.EncodeToString(M.BackgroundImage)

	dataUrl := fmt.Sprintf("data:image/png;base64,%s", b64image)

	imgStartEl := xml.StartElement{
		Name: xml.Name{Local: "image"},
		Attr: []xml.Attr{
			{Name: xml.Name{Local: "width"}, Value: "100%"},
			{Name: xml.Name{Local: "height"}, Value: "100%"},
			{Name: xml.Name{Local: "href"}, Value: dataUrl},
		},
	}

	if err := encoder.EncodeToken(imgStartEl); err != nil {
		return err
	}

	if err := encoder.EncodeToken(imgStartEl.End()); err != nil {
		return err
	}

	return nil
}

func (M *MapSVG) ProduceSVG() ([]byte, error) {
	var buf bytes.Buffer

	encoder := xml.NewEncoder(&buf)

	svgStartEl := xml.StartElement{
		Name: xml.Name{Local: "svg"},
		Attr: []xml.Attr{
			{Name: xml.Name{Local: "viewBox"}, Value: "0 0 16 16"},
			{
				Name:  xml.Name{Local: "xmlns"},
				Value: "http://www.w3.org/2000/svg",
			},
		},
	}

	// Encode start of SVG
	if err := encoder.EncodeToken(svgStartEl); err != nil {
		return nil, err
	}

	if err := M.EmitBackgroundImageTags(encoder); err != nil {
		return nil, fmt.Errorf("error while emitting background image tags: %w", err)
	}

	// Encode end of SVG
	if err := encoder.EncodeToken(svgStartEl.End()); err != nil {
		return nil, err
	}

	// Finally, try flushing to ensure everything is written out
	if err := encoder.Flush(); err != nil {
		return nil, err
	}
	return buf.Bytes(), nil
}
