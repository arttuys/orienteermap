package abstract2svg

import (
	"bytes"
	"image"
	"image/color"
	"image/png"
	"orienteermap-go-app/pkg/types/map_abstract"
	"orienteermap-go-app/pkg/utils/math"
)

var terrainToColor map[map_abstract.TerrainType]color.NRGBA = map[map_abstract.TerrainType]color.NRGBA{
	map_abstract.Unknown: {
		R: 255,
		G: 255,
		B: 255,
		A: 255,
	},
	map_abstract.Snow: {
		R: 250,
		G: 250,
		B: 250,
		A: 255,
	},
	map_abstract.InlandWater: {
		R: 16,
		G: 92,
		B: 166,
		A: 255,
	},
	map_abstract.Ocean: {
		R: 16,
		G: 52,
		B: 166,
		A: 255,
	},
	map_abstract.Grassland: {
		R: 30,
		G: 73,
		B: 9,
		A: 255,
	},
	map_abstract.DirtLike: {
		R: 128,
		G: 70,
		B: 27,
		A: 255,
	},
	map_abstract.Farmed: {
		R: 176,
		G: 168,
		B: 86,
		A: 255,
	},
	map_abstract.Pathway: {
		R: 240,
		G: 223,
		B: 41,
		A: 255,
	},
	map_abstract.Stone: {
		R: 190,
		G: 190,
		B: 190,
		A: 255,
	},
	map_abstract.Obsidian: {
		R: 64,
		G: 0,
		B: 112,
		A: 255,
	},
	map_abstract.Sand: {
		R: 255,
		G: 246,
		B: 74,
		A: 255,
	},
	map_abstract.Lava: {
		R: 240,
		G: 76,
		B: 0,
		A: 255,
	},
}

func abstractTerrainToPNGBackground(terrain *map_abstract.ChunkTerrainGrid) ([]byte, error) {
	// As the terrain is 16x16 and we want 4x4, we want a 64x64 image
	rect := image.Rect(0, 0, 63, 63)
	img := image.NewRGBA(rect)

	// Pattern counter, ensuring it applies neatly and evenly all across the entire chunk
	c := uint64(0)

	for x := 0; x < 16; x++ {
		for z := 0; z < 16; z++ {
			terrainStruct := terrain[x][z]

			// Fetch color for image
			mainColor := terrainToColor[terrainStruct.T]

			// Add color offset per step
			step := map_abstract.HeightToHeightStep(terrainStruct.H)

			mainColor.R = math.SaturatingAddUint8(mainColor.R, step*5)
			mainColor.G = math.SaturatingAddUint8(mainColor.G, step*5)
			mainColor.B = math.SaturatingAddUint8(mainColor.B, step*5)

			// Do initial fill
			for dx := 0; dx < 4; dx++ {
				for dy := 0; dy < 4; dy++ {
					ix := (x * 4) + dx
					iy := (z * 4) + dy

					if dy == 1 && (terrainStruct.C&map_abstract.Swamp > 0) {
						// Draw an ocean-colored line to indicate this is a swamped biome
						img.Set(ix, iy, terrainToColor[map_abstract.Ocean])
					} else {
						img.Set(ix, iy, mainColor)
					}

					c += 1
				}
			}

			// If there is forest coverage, make grassland color a bit brighter and overlay
			if terrainStruct.C&map_abstract.ForestCoverage > 0 {
				forestColor := terrainToColor[map_abstract.Grassland]
				forestColor.R = math.SaturatingAddUint8(forestColor.R, 15)
				forestColor.G = math.SaturatingAddUint8(forestColor.G, 15)
				forestColor.B = math.SaturatingAddUint8(forestColor.B, 20)

				for dy := 0; dy < 4; dy++ {
					for dx := 0; dx < 4; dx++ {
						ix := (x * 4) + dx
						iy := (z * 4) + dy

						if c%3 >= 1 {
							img.Set(ix, iy, forestColor)
						}

						c += 1
					}
				}
			}
		}
	}

	var byteBuf bytes.Buffer

	err := png.Encode(&byteBuf, img)

	return byteBuf.Bytes(), err
}
