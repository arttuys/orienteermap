package chunk2abstract

import (
	"orienteermap-go-app/pkg/core/constants"
	"orienteermap-go-app/pkg/mc_formats/shared"
	"orienteermap-go-app/pkg/types/map_abstract"
	"sync"
)

func doEntityScan(entities *shared.Entities, abstract *map_abstract.MapAbstract, sharedMutex *sync.Mutex, waitGroup *sync.WaitGroup) {
	waitGroup.Add(1)

	go func() {
		defer waitGroup.Done()

		var chunkNatureFlags map_abstract.ChunkFlags

		for _, entityType := range entities.Entities {
			if constants.IsDangerousEntity(entityType) {
				// This is a dangerous entity! Mark the chunk dangerous
				chunkNatureFlags |= map_abstract.Dangerous
			}
			if constants.IsResourceEntity(entityType) {
				// Useful resource mobs here
				chunkNatureFlags |= map_abstract.ResourceMobs
			}
			if entityType == "minecraft:villager" {
				// Villagers!
				chunkNatureFlags |= map_abstract.Villagers
			}
		}

		// If we found something, declare it in the chunk nature flags
		if chunkNatureFlags > 0 {
			sharedMutex.Lock()
			defer sharedMutex.Unlock()

			abstract.ChunkNatureFlags |= chunkNatureFlags
		}
	}()
}
