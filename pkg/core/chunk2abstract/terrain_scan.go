package chunk2abstract

import (
	"orienteermap-go-app/pkg/core/constants"
	"orienteermap-go-app/pkg/mc_formats/shared"
	"orienteermap-go-app/pkg/types/map_abstract"
	"sync"
)

type BlockBiomePair struct {
	Block *shared.Block
	Biome int32
}

// isAcceptableTerrainTypeForSwamp checks if the terrain is suitable to be a swamp; certain types might not be
func isAcceptableTerrainTypeForSwamp(t map_abstract.TerrainType) bool {
	switch t {
	case map_abstract.Unknown, map_abstract.Ocean, map_abstract.Farmed, map_abstract.Pathway, map_abstract.Snow, map_abstract.Obsidian, map_abstract.Lava:
		return false
	default:
		return true
	}
}

// getSingleStack examines a single stack of blocks from top to bottom, and determines what terrain characterization would be most descriptive
func getSingleStack(minY int32, maxY int32, blocks []BlockBiomePair) (def map_abstract.TerrainDef) {
	def = map_abstract.TerrainDef{BH: minY - 1}

	contextualLogger.Trace().Interface("blocks", blocks).Msg("tracing block-biome pair given")

	// Start from the highest block, and gradually work down
	var y, biome int32
	var foundBuiltup bool

	for y = maxY; y >= minY; y-- {
		i := y - minY

		block := blocks[i].Block
		biome = blocks[i].Biome

		if block == nil {
			contextualLogger.Debug().Int32("y", y).Msg("null block encountered, returning unknown")
			return
		}

		if constants.IsAirLike(block.Name) {
			// Air has no significance, continue
			continue
		}

		if constants.IsWaterLike(block.Name) {
			// Either inland water or ocean depending on biome
			if constants.IsOceanicBiome(biome) {
				def.T = map_abstract.Ocean
			} else {
				def.T = map_abstract.InlandWater
			}

			break
		}

		if constants.IsLavaLike(block.Name) {
			def.T = map_abstract.Lava

			break
		}

		if constants.IsGrassLike(block.Name) {
			def.T = map_abstract.Grassland

			break
		}

		if block.Name == "minecraft:farmland" {
			def.T = map_abstract.Farmed

			break
		}

		if block.Name == "minecraft:dirt_path" {
			def.T = map_abstract.Pathway

			break
		}

		if constants.IsDirtLike(block.Name) {
			def.T = map_abstract.DirtLike

			// Second-guess - if we're slightly below MaxY
			if y < maxY {
				blockAbove := blocks[i+1]
				if constants.IsLogOrMushroomStem(blockAbove.Block.Name) {
					// This could be grass instead
					def.T = map_abstract.Peer
				}
			}

			break
		}

		if constants.IsStoneLike(block.Name) {
			def.T = map_abstract.Stone

			break
		}

		if constants.IsSandLike(block.Name) {
			def.T = map_abstract.Sand

			break
		}

		if block.Name == "minecraft:obsidian" {
			def.T = map_abstract.Obsidian

			break
		}

		if constants.IsSnowLike(block.Name) {
			def.T = map_abstract.Snow

			break
		}

		if constants.IsWaterloggableBlock(block.Name) {
			def.T = map_abstract.Peer

			break
		}

		// If none of the above match, check if this qualifies as a built-up block, in which case mark that as the top height
		if !foundBuiltup && constants.IsBuiltUp(block.Name) {
			def.BH = y
			def.C |= map_abstract.BuiltUp

			foundBuiltup = true
		}

		if constants.IsLeafLike(block.Name) {
			def.C |= map_abstract.ForestCoverage
		}
	}

	def.H = y

	if isAcceptableTerrainTypeForSwamp(def.T) && constants.IsSwampLikeBiome(biome) {
		def.C |= map_abstract.Swamp
	}

	return
}

// doBaseTerrainGeneration generates the base terrain data,
func doBaseTerrainGeneration(chunk *shared.Chunk, abstract *map_abstract.MapAbstract, sharedMutex *sync.Mutex, waitGroup *sync.WaitGroup) *sync.WaitGroup {
	var terrainGenWait sync.WaitGroup

	waitGroup.Add(1)
	terrainGenWait.Add(1)

	go func() {
		defer terrainGenWait.Done()
		defer waitGroup.Done()

		var stackWaitGroup sync.WaitGroup

		var peerTerrain [][2]int

		for x := 0; x < 16; x++ {
			for z := 0; z < 16; z++ {
				stackWaitGroup.Add(1)

				x := x
				z := z

				go func() {
					defer stackWaitGroup.Done()

					minY := chunk.Level.BlockSectionData.MinY
					maxY := chunk.Level.BlockSectionData.MaxY
					arr := make([]BlockBiomePair, 0, maxY-minY)

					for y := minY; y <= maxY; y++ {
						var biome = int32(-1)
						if chunk.Level.Biomes != nil {
							biome = chunk.Level.Biomes.BiomeTypes[[3]int32{int32(x), y, int32(z)}]
						}
						arr = append(arr, BlockBiomePair{Block: chunk.Level.BlockSectionData.Blocks[[3]int32{int32(x), y, int32(z)}], Biome: biome})
					}

					resultDef := getSingleStack(minY, maxY, arr)

					abstract.BaseTerrain[x][z] = resultDef

					if resultDef.T == map_abstract.Peer {
						peerTerrain = append(peerTerrain, [2]int{x, z})
					}
				}()
			}
		}

		// Wait until all terrain processing is done and finished
		stackWaitGroup.Wait()

		// Next, do peer filtering - average terrain with the candidates, first allowing peers as a majority choice, and then excluding them.
		var foundChangeables bool

		foundChangeables = true
		allowPeersAsMajorityChoice := true

		for foundChangeables {

			// Start state: no non-peer changes found, nor changeables
			hasChangedToNonPeer := false
			foundChangeables = false

			for _, peerPos := range peerTerrain {
				peerChoices := make(map[map_abstract.TerrainType]uint8)

				// If this already has assigned a concrete terrain, ignore
				if abstract.BaseTerrain[peerPos[0]][peerPos[1]].T != map_abstract.Peer {
					continue
				}

				foundChangeables = true

				for dx := -1; dx <= 1; dx++ {
					for dz := -1; dz <= 1; dz++ {
						if dx == 0 && dz == 0 {
							continue
						}

						px := peerPos[0] + dx
						if px < 0 || px >= 16 {
							continue
						}
						pz := peerPos[1] + dz
						if pz < 0 || pz >= 16 {
							continue
						}

						peerTerrain := abstract.BaseTerrain[px][pz].T

						if !allowPeersAsMajorityChoice && peerTerrain == map_abstract.Peer {
							continue
						}

						peerChoices[peerTerrain] += 1
					}
				}

				count := uint8(0)
				var bestTerrainChoice map_abstract.TerrainType

				for choice, c := range peerChoices {
					if c > count {
						count = c
						bestTerrainChoice = choice
					}
				}

				if bestTerrainChoice != map_abstract.Peer {
					hasChangedToNonPeer = true
				}

				abstract.BaseTerrain[peerPos[0]][peerPos[1]].T = bestTerrainChoice
			}

			if !hasChangedToNonPeer && allowPeersAsMajorityChoice {
				allowPeersAsMajorityChoice = false
				foundChangeables = true
			}
		}

	}()

	return &terrainGenWait
}
