package chunk2abstract

import (
	"fmt"
	"github.com/rs/zerolog/log"
	"orienteermap-go-app/pkg/mc_formats/shared"
	"orienteermap-go-app/pkg/types/map_abstract"
	"sync"
)

var contextualLogger = log.With().Str("module", "chunk2abstract").Logger()

func GenerateAbstractForChunk(chunk *shared.ChunkBundle, level *shared.Level) (*map_abstract.MapAbstract, error) {
	var abstract map_abstract.MapAbstract

	// Declare a sync mutex for shared elements that multiple goroutines could modify, and a wait group
	var sharedSync sync.Mutex
	var waitGroup sync.WaitGroup
	var errors []error

	// Start base terrain generation
	doBaseTerrainGeneration(chunk.Chunk, &abstract, &sharedSync, &waitGroup)

	// If entities exist, scan them
	if chunk.Entities != nil {
		doEntityScan(chunk.Entities, &abstract, &sharedSync, &waitGroup)
	}

	// Wait for completion
	waitGroup.Wait()

	var combinedErr error = nil
	if len(errors) != 0 {
		combinedErr = fmt.Errorf("errors while building abstract: %v", errors)
		contextualLogger.Error().Err(combinedErr).Msg("failed to build abstract")
	}

	return &abstract, combinedErr
}
