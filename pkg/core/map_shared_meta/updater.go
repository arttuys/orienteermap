package map_shared_meta

import (
	"github.com/rs/zerolog/log"
	"orienteermap-go-app/pkg/types/basic"
	"orienteermap-go-app/pkg/types/interfaces"
	"orienteermap-go-app/pkg/types/map_global_meta"
)

var contextualLogger = log.With().Str("module", "mapglobalmeta").Logger()

func newEmptyMeta() *map_global_meta.MapGlobalMetadata {
	return &map_global_meta.MapGlobalMetadata{ChunkRngSeeds: make(map[basic.CoordinatePair]int64)}
}

func UpdateMapGlobalMeta(prev *map_global_meta.MapGlobalMetadata, affectedChunks []basic.CoordinatePair, provider interfaces.MiniDatabaseProvider) (*map_global_meta.MapGlobalMetadata, error) {
	meta := prev
	if meta == nil {
		meta = newEmptyMeta()
	}

	return meta, nil
}
