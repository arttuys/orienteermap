package constants

import "fmt"

func stringLstToMapWithPrefix(lst []string) map[string]bool {
	m := make(map[string]bool, len(lst))

	for _, v := range lst {
		m[fmt.Sprintf("minecraft:%s", v)] = true
	}

	return m
}
