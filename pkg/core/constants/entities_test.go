package constants

import "testing"

func TestIsDangerousEntity(t *testing.T) {
	tests := []struct {
		name      string
		entity    string
		dangerous bool
	}{
		{name: "Dangerous elder guardian", entity: "minecraft:elder_guardian", dangerous: true},
		{name: "Harmless cow", entity: "minecraft:cow", dangerous: false},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := IsDangerousEntity(tt.entity); got != tt.dangerous {
				t.Errorf("IsDangerousEntity() = %v, want %v", got, tt.dangerous)
			}
		})
	}
}
