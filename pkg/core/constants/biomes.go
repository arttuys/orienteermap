package constants

import "sync"

var oceanicBiomesLst = []int32{
	0,  // ocean
	24, // deep_ocean
	10, // frozen_ocean
	50, // deep_frozen_ocean
	46, // cold_ocean
	49, // deep_cold_ocean
	45, // lukewarm_ocean
	48, // deep_lukewarm_ocean
	44, // warm_ocean
	47, // deep_warm_ocean
}

var oceanicBiomesMap map[int32]bool

var biomeOnceInit sync.Once

func int32LstToMap(lst []int32) map[int32]bool {
	m := make(map[int32]bool)

	for _, v := range lst {
		m[v] = true
	}

	return m
}

func initBiomes() {
	biomeOnceInit.Do(func() {
		oceanicBiomesMap = int32LstToMap(oceanicBiomesLst)
	})
}

func IsOceanicBiome(id int32) bool {
	initBiomes()

	return oceanicBiomesMap[id]
}

func IsSwampLikeBiome(id int32) bool {
	switch id {
	case 6, 134: // swamp, swamp_hills
		return true
	default:
		return false
	}
}
