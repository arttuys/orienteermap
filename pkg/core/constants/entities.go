package constants

import "sync"

var dangerousEntitiesLst = []string{
	"blaze",
	"elder_guardian",
	"evoker",
	"ghast",
	"guardian",
	"hoglin",
	"magma_cube",
	"piglin_brute",
	"ravager",
	"vindicator",
	"witch",
	"wither",
	"wither_skeleton",
}

var dangerousEntitiesMap map[string]bool

func IsDangerousEntity(entity string) bool {
	initEntities()

	return dangerousEntitiesMap[entity]
}

var resourceEntitiesList = []string{
	"sheep",
	"cow",
	"chicken",
	"horse",
	"mooshroom",
}

var resourceEntitiesMap map[string]bool

func IsResourceEntity(entity string) bool {
	initEntities()

	return resourceEntitiesMap[entity]
}

//////////////////////////

var entityOnceInit sync.Once

func initEntities() {
	entityOnceInit.Do(func() {
		dangerousEntitiesMap = stringLstToMapWithPrefix(dangerousEntitiesLst)
		resourceEntitiesMap = stringLstToMapWithPrefix(resourceEntitiesList)
	})
}
