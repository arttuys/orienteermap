package constants

import (
	"fmt"
	"regexp"
	"strings"
)

// IsAirLike checks if a given block is air-like
func IsAirLike(block string) bool {
	switch block {
	case "minecraft:air", "minecraft:cave_air", "minecraft:void_air":
		return true
	default:
		return false
	}
}

// IsWaterLike checks if a given block is water-like (either still or flowing)
func IsWaterLike(block string) bool {
	switch block {
	case "minecraft:water", "minecraft:flowing_water", "minecraft:bubble_column":
		return true
	default:
		return false
	}
}

// IsLavaLike checks if a given block is lava-like
func IsLavaLike(block string) bool {
	switch block {
	case "minecraft:lava", "minecraft:flowing_lava":
		return true
	default:
		return false
	}
}

// IsStoneLike checks if a given block is a natural stone of some kind
func IsStoneLike(block string) bool {
	// Natural terracotta is considered to be a subtype of stone
	if IsNaturalTerracotta(block) {
		return true
	}

	// All ores are considered to be stone-like
	if IsOre(block) {
		return true
	}

	switch block {
	case "minecraft:stone", "minecraft:deepslate", "minecraft:bedrock":
		return true
	case "minecraft:andesite", "minecraft:diorite", "minecraft:tuff", "minecraft:granite":
		return true
	case "minecraft:smooth_basalt", "minecraft:blackstone", "minecraft:netherrack":
		return true
	case "minecraft:gravel":
		return true
	default:
		return false
	}
}

func IsGrassLike(block string) bool {
	switch block {
	case "minecraft:grass_block", "minecraft:crimson_nylium", "minecraft:warped_nylium":
		return true
	default:
		return false
	}
}

func IsDirtLike(block string) bool {
	switch block {
	case "minecraft:dirt", "minecraft:podzol", "minecraft:mycelium":
		return true
	default:
		return false
	}
}

// IsNaturalTerracotta checks if a block is probably natural terracotta, either non-prefixed or with some prefix
func IsNaturalTerracotta(block string) bool {
	res, err := regexp.Match("\\Aminecraft:([a-z_]+_)?terracotta\\z", []byte(block))
	if err != nil {
		panic(fmt.Errorf("could not use provided regex: %w", err))
	}

	return res
}

// IsOre checks if a given block is probably any kind of ore
func IsOre(block string) bool {
	res, err := regexp.Match("\\Aminecraft:[a-z_]+_ore\\z", []byte(block))
	if err != nil {
		panic(fmt.Errorf("could not use provided regex: %w", err))
	}

	return res
}

// IsLogOrMushroomStem checks if a given block is either a log, or mushroom stem block (both found in forests, and obscure grass)
func IsLogOrMushroomStem(block string) bool {
	res, err := regexp.Match("\\Aminecraft:([a-z_]+_log|mushroom_stem)\\z", []byte(block))
	if err != nil {
		panic(fmt.Errorf("could not use provided regex: %w", err))
	}

	return res
}

func IsSandLike(block string) bool {
	res, err := regexp.Match("\\Aminecraft:(red_)?sand(stone)?\\z", []byte(block))
	if err != nil {
		panic(fmt.Errorf("could not use provided regex: %w", err))
	}

	return res
}

func IsSnowLike(block string) bool {
	switch block {
	case "minecraft:snow", "minecraft:snow_block", "minecraft:powder_snow":
		return true
	default:
		return false
	}
}

// IsLeafLike examines if the block is leaf-like, and indicates forestation
func IsLeafLike(block string) bool {
	switch block {
	case "minecraft:vine":
		return true
	default:
		return strings.HasSuffix(block, "_leaves")
	}
}

// IsWaterloggableBlock tests if a block is something that can exist in water. Currently, we do not have support for examining block states, so we make an educated guess using peers
func IsWaterloggableBlock(block string) bool {
	switch block {
	case "minecraft:seagrass", "minecraft:tall_seagrass", "minecraft:kelp", "minecraft:kelp_plant":
		return true
	default:
		return false
	}

}
