package constants

import "strings"

var builtupPrefixes = []string{
	"cobblestone",
	"mossy_cobblestone",
	"polished_",
	"chiseled_",
	"stripped_",
}

var builtupSuffixes = []string{
	"_door",
	"_trapdoor",
	"_fence",
	"_gate",
	"_sign",
	"_concrete",
	"_slab",
	"_stairs",
	"_planks",
	"_bricks",
	"_wall",
	"_bars",
	"_pressure_plate",
}

func isPrefixedBuiltUp(block_prt string) bool {
	for _, v := range builtupPrefixes {
		if strings.HasPrefix(block_prt, v) {
			return true
		}
	}

	return false
}

func isSuffixedBuiltUp(block_prt string) bool {
	for _, v := range builtupSuffixes {
		if strings.HasSuffix(block_prt, v) {
			return true
		}
	}

	return false
}

func IsBuiltUp(block string) bool {
	if !strings.HasPrefix(block, "minecraft:") {
		return false
	}

	// Strip header part
	block_prt := strings.TrimPrefix(block, "minecraft:")

	return isPrefixedBuiltUp(block_prt) || isSuffixedBuiltUp(block_prt)
}
