package constants

import "testing"

func TestIsNaturalTerracotta(t *testing.T) {
	tests := []struct {
		name  string
		block string
		want  bool
	}{
		{name: "baseline natural", block: "minecraft:terracotta", want: true},
		{name: "baseline colored", block: "minecraft:white_terracotta", want: true},
		{name: "not terracotta", block: "minecraft:stone", want: false},
		{name: "not natural terracotta", block: "minecraft:terracotta_slab", want: false},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := IsNaturalTerracotta(tt.block); got != tt.want {
				t.Errorf("IsNaturalTerracotta() = %v, want %v", got, tt.want)
			}
		})
	}
}
