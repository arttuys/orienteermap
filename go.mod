module orienteermap-go-app

go 1.16

require github.com/spf13/cobra v1.2.1

require github.com/mattn/go-sqlite3 v1.14.8

require github.com/rs/zerolog v1.23.0

require github.com/spf13/viper v1.8.1
