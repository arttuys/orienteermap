package config_gen

import (
	"orienteermap-go-app/internal/runner/governor"
	"orienteermap-go-app/pkg/core/abstract2svg"
)

type config struct {
	GoMaxProcs *int                    `mapstructure:"gomaxprocs"`
	RunnerOpts governor.ConfigOpts     `mapstructure:"runner_opts"`
	RenderOpts abstract2svg.RenderOpts `mapstructure:"render_opts"`
	SetupType  string                  `mapstructure:"setup_type"`
}
