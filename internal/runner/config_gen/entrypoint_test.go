package config_gen

import (
	"github.com/rs/zerolog"
	"os"
	"path/filepath"
	"testing"
)

func TestBaselineRender(t *testing.T) {
	basePath, err := os.Getwd()
	if err != nil {
		t.Fatal(err)
	}

	path := filepath.Join(basePath, "..", "..", "..", "test")

	err = os.Chdir(path)
	if err != nil {
		t.Fatal(err)
	}

	// Try resetting temporary directory
	removalPath := filepath.Join("temp", "render")
	if err := os.RemoveAll(removalPath); err != nil && !os.IsNotExist(err) {
		t.Fatalf("failed initial removal of test temp dir: %v", err)
	}

	// Set logging to trace mode
	zerolog.SetGlobalLevel(zerolog.DebugLevel)

	// Try to simply run a config
	RunByConfigFile("./entrypoint_test.yaml")
}
