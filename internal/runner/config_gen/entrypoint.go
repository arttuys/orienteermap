package config_gen

import (
	"github.com/rs/zerolog/log"
	"github.com/spf13/viper"
	"orienteermap-go-app/internal/runner/governor"
	"runtime"
)

func bindEnvs() {
	viper.SetEnvPrefix("orienteermap")

	err := viper.BindEnv("gomaxprocs")
	if err != nil {
		log.Fatal().Err(err).Msg("error binding environment variable for maxprocs")
	}
}

func setPreRunOpts(cfg *config) {
	if cfg.GoMaxProcs != nil {
		log.Info().Int("gomaxprocs", *cfg.GoMaxProcs).Msg("setting gomaxprocs to given value")
		runtime.GOMAXPROCS(*cfg.GoMaxProcs)
	} else {
		log.Debug().Int("gomaxprocs", runtime.GOMAXPROCS(0)).Msg("no preference given, staying with default GOMAXPROCS")
	}
}

func RunByConfigFile(fname string) {
	// Set environment vars
	bindEnvs()

	viper.SetConfigFile(fname)
	if err := viper.ReadInConfig(); err != nil {
		log.Fatal().Err(err).Msg("error reading configuration")
	} else {
		log.Info().Msg("read configuration successfully, parsing and validating")
	}

	var config config
	if err := viper.Unmarshal(&config); err != nil {
		log.Fatal().Err(err).Msg("error parsing configuration")
	}

	// Set pre-running opts
	setPreRunOpts(&config)

	// Procure runner elements if possible
	world, db, svg, err := ProcureRunnerParts()
	if err != nil {
		log.Fatal().Err(err).Msg("error generating runner from options")
	}

	log.Info().Msg("preflight checks complete, ready to start with given options")
	runner := governor.New(world, db, svg, config.RunnerOpts)

	if err := runner.Run(); err != nil {
		log.Error().Err(err).Msg("render ended with errors")
	}
}
