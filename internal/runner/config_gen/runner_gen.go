package config_gen

import (
	"errors"
	"fmt"
	"github.com/rs/zerolog/log"
	"github.com/spf13/viper"
	"orienteermap-go-app/pkg/database/sqlite3"
	"orienteermap-go-app/pkg/mc_formats/anvil/region"
	"orienteermap-go-app/pkg/svg/folder_collector"
	"orienteermap-go-app/pkg/types/interfaces"
)

type offlineRegion struct {
	RegionDir          string `mapstructure:"region"`
	DimensionSubfolder string `mapstructure:"dim_folder"`
	OutputDir          string `mapstructure:"output"`
}

func ProcureRunnerParts() (world interfaces.WorldSource, db interfaces.DatabaseProvider, output interfaces.SVGOutputCollector, err error) {
	// Get setup type
	if !viper.IsSet("setup_type") {
		err = errors.New("you must provide a setup type")
		return
	}

	setupType := viper.GetString("setup_type")

	switch setupType {
	case "offline_region":
		var opts offlineRegion
		err = viper.UnmarshalKey("setup", &opts)
		if err != nil {
			return
		}

		if opts.RegionDir == "" || opts.OutputDir == "" {
			err = errors.New("region or output directories cannot be empty")
			return
		}

		log.Info().Str("region", opts.RegionDir).Str("output", opts.OutputDir).Str("dim", opts.DimensionSubfolder).Msg("creating an offline region setup")

		// Initialize various interfaces
		world, err = region.NewReader(opts.RegionDir, opts.DimensionSubfolder)
		if err != nil {
			return
		}

		db, err = sqlite3.NewProvider(opts.OutputDir)
		if err != nil {
			return
		}

		output, err = folder_collector.NewCollector(opts.OutputDir)
		return
	default:
		err = fmt.Errorf("unknown setup type: %s", setupType)
		return
	}
}
