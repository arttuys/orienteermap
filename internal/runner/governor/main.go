package governor

import (
	"errors"
	"github.com/rs/zerolog/log"
	"orienteermap-go-app/pkg/types/basic"
	"orienteermap-go-app/pkg/types/interfaces"
	"orienteermap-go-app/pkg/utils/structs"
	"sync"
	"sync/atomic"
)

var contextualLogger = log.With().Str("module", "governor").Logger()

type Governor struct {
	world  interfaces.WorldSource
	db     interfaces.DatabaseProvider
	output interfaces.SVGOutputCollector
	queue  *structs.PriorityCoordinateSet
	// queueProcessorSignalChannel acts as an implicit rate limiter for queue processing; each item will allow exactly one
	// flush to be done, and closure of the channel terminates the goroutine
	queueProcessorSignalChannel chan struct{}
	// queueProcessorOutputChannel emits a single flushed set of items, with a priority, in tandem
	queueProcessorOutputChannel chan struct {
		priority uint8
		coords   []basic.CoordinatePair
	}
	// configOpts contains config options for the runner
	configOpts ConfigOpts
	// hadErrors indicates something went wrong which should be reported on exit
	hadErrors bool
	// mapSharedMetadataUpdated indicates if map shared metadata has been appropriately updated. This must be ran after any map abstracts have changed or generated
	// and at the start of each session
	mapSharedMetadataUpdated bool
}

func New(world interfaces.WorldSource, db interfaces.DatabaseProvider, output interfaces.SVGOutputCollector, opts ConfigOpts) *Governor {
	return &Governor{
		world:  world,
		db:     db,
		output: output,
		queue:  &structs.PriorityCoordinateSet{},
		queueProcessorOutputChannel: make(chan struct {
			priority uint8
			coords   []basic.CoordinatePair
		}, 1),
		queueProcessorSignalChannel: make(chan struct{}, 1),
		configOpts:                  opts,
	}
}

func (G *Governor) requestRenderAll() error {
	contextualLogger.Debug().Msg("requested render of all chunks, queueing all chunks")

	chunks, err := G.world.GetListOfChunks()

	if err != nil {
		contextualLogger.Error().Err(err).Msg("something went wrong with requesting all chunks, returning without queueing")
		return err
	}

	// Pass it to queue
	G.queue.Insert(PriorityChunkConsideration, chunks...)

	return nil
}

func (G *Governor) Close() error {
	var errCounter int32
	var waitGroup sync.WaitGroup

	// Terminate signal channel
	close(G.queueProcessorSignalChannel)
	G.queue.Quiesce(true)

	waitGroup.Add(3)

	go func() {
		err := G.world.Close()
		if err != nil {
			contextualLogger.Error().Err(err).Msg("failed to close world")
			atomic.AddInt32(&errCounter, 1)
		}
		waitGroup.Done()
	}()

	go func() {
		err := G.db.Close()
		if err != nil {
			contextualLogger.Error().Err(err).Msg("failed to close DB")
			atomic.AddInt32(&errCounter, 1)
		}
		waitGroup.Done()
	}()

	go func() {
		err := G.output.Close()
		if err != nil {
			contextualLogger.Error().Err(err).Msg("failed to close output collector")
			atomic.AddInt32(&errCounter, 1)
		}
		waitGroup.Done()
	}()

	waitGroup.Wait()
	if errCounter > 0 {
		return errors.New("something went wrong while closing, check log")
	} else if G.hadErrors {
		return errors.New("run might not have completed entirely successfully, check log")
	} else {
		return nil
	}
}

// DoQueuePumping transforms queue flush operations into channel inputs to make using select structure possible
func (G *Governor) DoQueuePumping() {
	for {
		_, ok := <-G.queueProcessorSignalChannel
		if !ok {
			return
		}

		// Attempt a flush
		priority, els := G.queue.FlushBatch()
		if len(els) == 0 {
			// This was an empty flush. This could be a prelude to a termination, or some kind of a hiccup. Retry loop
			continue
		} else {
			G.queueProcessorOutputChannel <- struct {
				priority uint8
				coords   []basic.CoordinatePair
			}{priority: priority, coords: els}
		}
	}
}

func (G *Governor) Run() error {
	if !G.configOpts.PassiveRun {
		err := G.requestRenderAll()
		if err != nil {
			contextualLogger.Error().Err(err).Msg("failed to initialize single-run, exiting runner prematurely")
			return err
		}
	}

	// Give initial start token to queue processor, and start pumping
	G.queueProcessorSignalChannel <- struct{}{}
	go G.DoQueuePumping()

	for {
		select {
		case queueEl := <-G.queueProcessorOutputChannel:
			switch queueEl.priority {
			case PriorityChunkConsideration:
				G.doChunkConsideration(queueEl.coords)
			case PriorityMapAbstractGen:
				G.doMapAbstractGen(queueEl.coords)
			case PriorityMapSharedUpdate:
				G.doMapGlobalMetaUpdate(queueEl.coords)
			case PriorityRender:
				G.doRendering(queueEl.coords)
			default:
				contextualLogger.Error().Uint8("type", queueEl.priority).Interface("elements", queueEl.coords).Msg("unknown priority, skipping")
				G.hadErrors = true
			}
			// If we are on a single run, and no tasks remain, exit
			if (!G.configOpts.PassiveRun) && G.queue.Len() == 0 {
				goto exit
			} else {
				// Otherwise, provide a new token for the queue processor
				G.queueProcessorSignalChannel <- struct{}{}
			}
		}
	}

exit:
	return G.Close()
}
