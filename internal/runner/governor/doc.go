package governor

// The governor is intended to be what its name purports it to be, something governing how execution of renders goes
// It will maintain 3 distinct queues:
//  - Chunks that need consideration (requested by user or some other entity to be rendered)
//	- Chunks that need map abstract generation
//  - Chunks that need re-rendering (they or their dependencies have changed)
//
// The governor in its current design plan will pick tasks from the highest possible queue (consideration > abstracts > rendering)
// in a batch, and at each step wait for the batch's completion. In addition, before proceeding to render queue, a map global metadata regeneration task
// shall be run, as we assume at that point we should have all the map abstracts that we can have.
