package governor

const (
	// PriorityRender is for rendering tasks, the lowest priority as almost any change will potentially render them void
	PriorityRender uint8 = iota
	// PriorityMapSharedUpdate indicates that map shared metadata will need an update, as abstracts have changed
	PriorityMapSharedUpdate
	// PriorityMapAbstractGen is for map abstract generation; it is not affected by rendering changes (as renders originate from map abstracts)
	PriorityMapAbstractGen
	// PriorityChunkConsideration is the highest priority tier; these are looked over and placed on a more appropriate tier, if valid
	// As it is a cheap operation and effectively a precursor/dependency to anything else, it naturally must be the highest priority
	PriorityChunkConsideration
)
