package governor

import (
	"fmt"
	"github.com/rs/zerolog/log"
	"orienteermap-go-app/pkg/core/abstract2svg"
	"orienteermap-go-app/pkg/core/chunk2abstract"
	"orienteermap-go-app/pkg/core/map_shared_meta"
	"orienteermap-go-app/pkg/mc_formats/shared"
	"orienteermap-go-app/pkg/types/basic"
	"sync"
	"time"
)

func (G *Governor) doChunkConsideration(rawCoords []basic.CoordinatePair) {
	coords := make([]basic.CoordinatePair, 0)
	var chunksOmitted bool
	for _, v := range rawCoords {
		if !G.configOpts.ChunkInRange(v) {
			contextualLogger.Debug().Interface("chunk", v).Msg("chunk out of configured range, skipping")
			chunksOmitted = true
		} else {
			coords = append(coords, v)
		}
	}

	if chunksOmitted {
		contextualLogger.Warn().Msg("some chunks were omitted due to them being out of range per limits")
	}

	if G.configOpts.BypassModChecks {
		for _, v := range coords {
			contextualLogger.Debug().Interface("chunk", v).Msg("bypass mod checks active, adding given chunk to map-abstract queue directly")
			G.queue.Insert(PriorityMapAbstractGen, v)
		}

		return
	}

	var waitGroup sync.WaitGroup

	// First step, collect all modification times
	var modTimesForChunk, modTimesForDb, modTimesForOutput map[basic.CoordinatePair]time.Time
	errored := false

	waitGroup.Add(3)
	go func() {
		var err error
		modTimesForChunk, err = G.world.GetChunkModificationTimes(coords, false)

		if err != nil {
			errored = true
			contextualLogger.Error().Err(err).Msg("something went wrong retrieving chunk modification times for set")
		}

		waitGroup.Done()
	}()

	go func() {
		var err error
		modTimesForDb, err = G.db.GetChunkModificationTimes(coords, false)

		if err != nil {
			errored = true
			contextualLogger.Error().Err(err).Msg("something went wrong retrieving map abstract modification times for set")
		}

		waitGroup.Done()
	}()

	go func() {
		var err error
		modTimesForOutput, err = G.output.GetChunkModificationTimes(coords, false)

		if err != nil {
			errored = true
			contextualLogger.Error().Err(err).Msg("something went wrong retrieving output modification times for set")
		}

		waitGroup.Done()
	}()

	waitGroup.Wait()

	if errored {
		G.hadErrors = true
		contextualLogger.Error().Msg("unable to process chunk consideration queue set, skipping")
		return
	}

	if len(coords) != len(modTimesForChunk) {
		contextualLogger.Warn().Msg("some chunks have no modification time from world provider, skipping them (this could be simply a missing chunk in most common case)")
	}

	for chunk, modTime := range modTimesForChunk {
		mapAbstractModTime, found := modTimesForDb[chunk]
		if !found || modTime.After(mapAbstractModTime) {
			// We need a new map abstract, add to map abstract queue
			contextualLogger.Info().Interface("chunk", chunk).Msg("new map abstract required, adding to map abstract queue")

			G.queue.Insert(PriorityMapAbstractGen, chunk)
			continue
		} else {
			contextualLogger.Debug().Interface("chunk", chunk).Msg("up to date map abstract, checking render")
		}

		renderModTime, found := modTimesForOutput[chunk]
		if !found || modTime.After(renderModTime) {
			contextualLogger.Info().Interface("chunk", chunk).Msg("new rendering required, adding to render queue")

			// Render is out of date, new render required
			G.queue.Insert(PriorityRender, chunk)
			continue
		} else {
			contextualLogger.Debug().Interface("chunk", chunk).Msg("up to date render")
		}

		contextualLogger.Info().Interface("chunk", chunk).Msg("chunk is fully up to date, no action required")
	}
}

func (G *Governor) doMapGlobalMetaUpdate(coords []basic.CoordinatePair) {
	prevMeta, err := G.db.GetMapGlobalMetadata()
	if err != nil {
		G.hadErrors = true
		contextualLogger.Error().Err(err).Msg("failed to retrieve previous global meta, cannot process")
		return
	}

	miniProvider := G.db.GetMiniProviderForRenderer()

	nextMeta, err := map_shared_meta.UpdateMapGlobalMeta(prevMeta, coords, miniProvider)
	if err != nil {
		G.hadErrors = true
		contextualLogger.Error().Err(err).Msg("failed to update map metadata - renders may not be wholly accurate")
		return
	}

	err = G.db.UpdateMapGlobalMetadata(nextMeta)
	if err != nil {
		G.hadErrors = true
		contextualLogger.Error().Err(err).Msg("failed to save metadata to DB - renders may not be wholly accurate")
		return
	}

	contextualLogger.Info().Interface("chunks", coords).Msg("updated map metadata for chunks")
	contextualLogger.Trace().Interface("meta", nextMeta).Msg("tracing current metadata")
}

func (G *Governor) doMapAbstractGen(coords []basic.CoordinatePair) {
	level, err := G.world.GetLevel()

	if err != nil {
		G.hadErrors = true
		contextualLogger.Error().Err(err).Msg("failed to retrieve shared level information, cannot process")
		return
	}

	var waitGroup sync.WaitGroup
	err = G.world.GetChunks(coords, func(pair basic.CoordinatePair, chunk *shared.ChunkBundle) {
		waitGroup.Add(1)
		go func() {
			defer waitGroup.Done()

			// Do an initial preflight check for chunk validity
			if chunk.Chunk.Level.Status != "full" {
				contextualLogger.Warn().Interface("chunk", pair).Msg("this chunk has not yet been fully generated and cannot be processed - skipping implicitly without error")
				return
			}

			abstract, err := chunk2abstract.GenerateAbstractForChunk(chunk, level)
			if err != nil {
				G.hadErrors = true
				contextualLogger.Error().Err(err).Interface("chunk", pair).Msg("could not generate an abstract from chunk")
				return
			}

			log.Info().Interface("chunk", pair).Msg("generated chunk, saving to abstract DB")
			log.Trace().Interface("chunk", pair).Interface("abstract", abstract).Msg("emitting chunk contents for trace")

			err = G.db.StoreMapAbstract(pair, abstract)
			if err != nil {
				G.hadErrors = true
				contextualLogger.Error().Err(err).Interface("chunk", pair).Msg("could not save abstract to DB")
				return
			}

			// Enqueue meta update, rendering
			G.queue.Insert(PriorityMapSharedUpdate, pair)
			G.queue.Insert(PriorityRender, pair)
		}()
	}, true)

	if err != nil {
		G.hadErrors = true
		contextualLogger.Error().Err(err).Msg("something went wrong generating map abstracts")
	}

	// Wait for all to complete
	waitGroup.Wait()
}

func (G *Governor) doRendering(coords []basic.CoordinatePair) {
	// First, try fetching all map abstracts
	abstracts, err := G.db.GetMapAbstracts(coords)
	if err != nil {
		contextualLogger.Error().Err(err).Msg("something went wrong fetching abstracts, cannot process this batch")
		G.hadErrors = true
		return
	}

	mapMeta, err := G.db.GetMapGlobalMetadata()
	if err != nil {
		contextualLogger.Error().Err(err).Msg("something went wrong fetching map meta, cannot process this batch")
		G.hadErrors = true
		return
	}

	var completionWaitGroup sync.WaitGroup
	errChan := make(chan error, len(coords))

	for _, v := range coords {
		completionWaitGroup.Add(1)

		chunk := v

		go func() {
			defer completionWaitGroup.Done()

			chunkData := abstracts[chunk]
			if chunkData == nil {
				errChan <- fmt.Errorf("received nil chunk for %v", chunk)
				return
			}

			b, err := abstract2svg.RenderSVGFromAbstract(chunk, chunkData, mapMeta)
			if err != nil {
				errChan <- fmt.Errorf("rendering error for %v: %w", chunk, err)
				return
			}

			err = G.output.SaveChunkRender(chunk, b)
			if err != nil {
				errChan <- fmt.Errorf("saving error for %v: %w", chunk, err)
				return
			}
		}()
	}

	completionWaitGroup.Wait()
	close(errChan)

	var errLst []error

	for {
		err, found := <-errChan
		if !found {
			break
		} else {
			errLst = append(errLst, err)
			contextualLogger.Error().Err(err).Msg("encountered error in rendering step")
		}
	}

	if len(errLst) > 0 {
		G.hadErrors = true
		contextualLogger.Error().Msg("found errors, all chunks may not have rendered")
	}
}
