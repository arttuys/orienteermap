package governor

import (
	"orienteermap-go-app/pkg/types/basic"
	"orienteermap-go-app/pkg/utils/math"
)

type ConfigOpts struct {
	// PassiveRun indicates if this should start as a passive run, namely not enqueueing anything unless asked to via a backchannel
	PassiveRun bool `mapstructure:"passive_run"`
	// BypassModChecks omits certain data modification checks, instead forcing rebuild/recalculation to take place. Use only when necessary
	BypassModChecks bool  `mapstructure:"bypass_mod_checks"`
	ChunkX          int32 `mapstructure:"chunk_x"`
	ChunkZ          int32 `mapstructure:"chunk_z"`
	ChunkMaxSteps   int32 `mapstructure:"chunk_max_steps"`
}

func (CO ConfigOpts) ChunkInRange(chunk basic.CoordinatePair) bool {
	if CO.ChunkMaxSteps == 0 {
		// Always in range
		return true
	}

	dx := math.AbsDiffInt(chunk[0], CO.ChunkX)
	dz := math.AbsDiffInt(chunk[1], CO.ChunkZ)

	return (dx + dz) <= CO.ChunkMaxSteps
}
